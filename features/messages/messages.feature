Feature: messages
  In order to see important messages
  As a user
  I want to see them on the home page

  Background:
    Given there are messages
    When I am on the homepage

  Scenario: viewing message
    Then I should see last message

  Scenario: viewing all messages
    When I click on message
    Then I should see all messsages
