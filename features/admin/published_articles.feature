Feature: Working with published articles
  In order to unit or edit/delete published articles
  As an administrator
  I want to do it on admin page instead of main

  Background:
    Given there are some published articles
    And there are two articles for grouping
    And there are some categories
    And there are some news sources
    And I have successfully signed in as administrator
    And I am on the admin published articles path

  @javascript
  Scenario: viewing published articles
    Then I should see them on published article administration list

  @javascript
  Scenario: join articles into one group
    When I check some articles for grouping
    And press group button
    Then I should see that articles are grouped

  @javascript
  Scenario: ungrouping articles
    When I check some articles for grouping
    And press group button
    Then take a snapshot
    When I ungroup articles
    Then articles should be ungrouped

  @javascript
  Scenario: deleting articles
    When I delete article
    Then I should not see it on published article administration list
