Feature: Article administration
  In order to manage articles
  As an administrator
  I want to have special admin tools

  Background:
    Given I have successfully signed in as administrator

  Scenario: Viewing unsorted articles
    Given there are some unpublished articles
    When I visit article administration page
    Then I should see unpublished articles

  Scenario: Clear all unpublished articles
    Given there are some unpublished articles
    And I visit article administration page
    When I delete unupublished articles
    Then I should not see that unpublished articles

  Scenario: Deleting one article
    Given there is unpublished article
    And I visit article administration page
    When I delete single article
    Then I should not see that article

  Scenario: training articles
    And I visit article administration page
    When I train classificator
    Then there should be zero not publilhsed articles

  Scenario: adding articles by hand
    When I create new article manually
    Then I should see manually created article

  Scenario: editing article
    When I edit article
    Then I should see changes of edited article
