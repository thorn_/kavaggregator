# -*- encoding: utf-8 -*-
Feature: Creating trends
  In order to have some trends
  As an administrator
  I want to create them through site

  Background:
    Given there are some trends
    And I have successfully signed in as administrator
    And I am on the trends administration path

  Scenario: Viewing trends
    Then I should see all trends

  Scenario: Adding a valid trend
    When I create valid trend
    Then I should see all trends

  Scenario: Failing creating trend
    When I create invalid trend
    Then I should see missing trend name warning

  Scenario: Destroying trends
    When I delete trend
    Then I should not see trend in trends list

  Scenario: Editing trend
    When I edit trend with valid attributes
    Then I should see new name of trend

  Scenario: Fail edit
    When I edit trend with invalid attributes
    Then I should see missing trend name warning
