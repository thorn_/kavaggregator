Feature: Users
  In order to admin user
  As an administrator
  I want to manage them through site

  Background:
    And there are some users
    Given I have successfully signed in as administrator
    And I am on the users administration page

  Scenario: Viewing user
    Then I should see them on users list

  Scenario: Deleting users
    When I delete user
    Then I should not see him on users list

  Scenario: Viewing online user
    Given there is user online
    Then I should see online mark
    And I should see number of online users

  Scenario: Editing users role
    When I edit user's role
    Then he should have another role
