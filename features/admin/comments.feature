Feature: Admin comments action
  In order to see what's someone saying
  As an administrator
  I want to see all comments

  Background:
    Given I have successfully signed in as administrator
    And there are some comments
    Given I am on the comments administration path

  Scenario: Viewing comments
    Then I should see all comments
    And I should see comments number

  Scenario: Deleting first comment
    When I delete comment
    Then I should see comment deleted marker
