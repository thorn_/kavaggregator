# -*- encoding: utf-8 -*-
Feature: News Sources
  In order to provide parser with news sources
  As an administrator
  I want to manage them through admin page

  Background:
    Given there are some news sources
    And I have successfully signed in as administrator
    And I am on the news sources administration path

  Scenario: Viewing news sources
    Then I should see all news sources

  Scenario: Adding a valid news source
    When I create valid news source
    Then I should see all news sources

  Scenario: Failing creating news source
    When I create invalid news source
    Then I should see missing news source name warning

  Scenario: Destroying news sources
    When I delete news source
    Then I should not see news source in news sources list

  Scenario: Editing news source
    When I edit news source with valid attributes
    Then I should see new name of news source

  Scenario: Fail edit
    When I edit news source with invalid attributes
    Then I should see invalid news source warning
