# -*- encoding: utf-8 -*-
Feature: Creating categories
  In order to have some categories
  As an administrator
  I want to create them through site

  Background:
    Given there are some categories
    And I have successfully signed in as administrator
    And I am on the categories administration path

  Scenario: Viewing categories
    Then I should see all categories

  Scenario: Adding a valid category
    When I create valid category
    Then I should see all categories

  Scenario: Failing creating category
    When I create invalid category
    Then I should see missing category name warning

  Scenario: Destroying categories
    When I delete category
    Then I should not see category in categories list

  Scenario: Editing category
    When I edit category with valid attributes
    Then I should see new name of category

  Scenario: Fail edit
    When I edit category with invalid attributes
    Then I should see missing category name warning
