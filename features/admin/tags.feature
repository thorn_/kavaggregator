# -*- encoding: utf-8 -*-
Feature: Creating tags
  In order to have some tags
  As an administrator
  I want to create them through site

  Background:
    Given there are some tags
    And I have successfully signed in as administrator
    And I am on the tags administration path

  Scenario: Viewing tags
    Then I should see all tags

  Scenario: Adding a valid tag
    When I create valid tag
    Then I should see all tags

  Scenario: Failing creating tag
    When I create invalid tag
    Then I should see missing tag name warning

  Scenario: Destroying tags
    When I delete tag
    Then I should not see tag in tags list

  Scenario: Editing tag
    When I edit tag with valid attributes
    Then I should see new name of tag

  Scenario: Fail edit
    When I edit tag with invalid attributes
    Then I should see missing tag name warning
