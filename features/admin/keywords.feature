# -*- encoding: utf-8 -*-
Feature: Keywords
  In order to provide parser with search keywords
  As an administrator
  I want to manage them through admin page

  Background:
    Given there are some keywords
    And I have successfully signed in as administrator
    And I am on the keywords administration path

  Scenario: Viewing keywords
    Then I should see all keywords

  Scenario: Adding a valid keyword
    When I create valid keyword
    Then I should see all keywords

  Scenario: Failing creating keyword
    When I create invalid keyword
    Then I should see missing keyword name warning

  Scenario: Destroying keywords
    When I delete keyword
    Then I should not see keyword in keywords list

  Scenario: Editing keyword
    When I edit keyword with valid attributes
    Then I should see new name of keyword

  Scenario: Fail edit
    When I edit keyword with invalid attributes
    Then I should see invalid keyword warning
