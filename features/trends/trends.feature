Feature: Using trends
  In order to look for hot news
  As a user
  I want to click on trends for the actual news

  Scenario: Viewing trends
    Given there are some active trends
    And I am on the homepage
    Then I should see trends

  Scenario: Viewing articles from trends
    Given there are some active trends with articles
    And I am on the homepage
    When I click on trend
    Then I should see news of that trend
    And meta attributes should have trend description
