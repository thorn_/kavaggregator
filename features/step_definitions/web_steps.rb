When /^I (?:click|press) "(.*?)"$/ do |name|
  click_on name
end

Then /^I should see "(.*?)"$/ do |text|
  page.should have_content(text)
end

When /^I fill in "(.*?)" with "(.*?)"$/ do |field, value|
  fill_in field, with: value
end

When /^I want to see current page$/ do
  save_and_open_page
end

Then /^(?:|I )should see "([^\"]*)" within "([^\"]*)"$/ do |text, selector|
  within selector do
    page.should have_xpath("//*[text()='#{text}']", visible: true)
  end
end

Then /^I should really see "(.*?)"$/ do |text|
  within "body" do
    page.should have_xpath("//*[text()='#{text}']", visible: true)
  end
end

Then /^I should not really see "(.*?)"$/ do |text|
  within "body" do
    page.should_not have_xpath("//*[text()='#{text}']", visible: true)
  end
end
