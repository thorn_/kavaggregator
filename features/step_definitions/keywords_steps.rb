#-*- encoding: utf-8 -*-

def keyword_template
  @key = {name: "test_keyword"}
end

def create_keyword
  visit new_admin_keyword_path
  fill_in "Имя", with: @key[:name]
  click_on "Создать ключевое слово"
end

Given /^there are some keywords$/ do
  1.times do |i|
    FactoryGirl.create(:keyword, name: "test_keyword#{i}")
  end
end

Then /^I should see all keywords$/ do
  Keyword.scoped.each do |cat|
    page.should have_content(cat.name)
  end
end

When /^I create valid keyword$/ do
  keyword_template
  create_keyword
end

When /^I create invalid keyword$/ do
  keyword_template
  @key[:name] = ''
  create_keyword
end

Then /^I should not see keyword in keywords list$/ do
  visit admin_keywords_path
  Keyword.scoped.each do |cat|
    page.should_not have_content(cat.name)
  end
end

Then /^I should see missing keyword name warning$/ do
  page.should have_content("Ключевые слова не созданы")
end

When /^I delete keyword$/ do
  click_on "Удалить"
end

When /^I edit keyword with valid attributes$/ do
  click_on "Изменить"
  fill_in "Имя", with: "New keyword name"
  click_on "Сохранить"
end

Then /^I should see new name of keyword$/ do
  page.should have_content("New keyword name")
end

When /^I edit keyword with invalid attributes$/ do
  click_on "Изменить"
  fill_in "Имя", with: ""
  click_on "Сохранить"
end

Then /^I should see invalid keyword warning$/ do
  page.should have_content("Ключевое слово не изменено")
end
