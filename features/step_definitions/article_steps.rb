#-*- encoding: utf-8 -*-
Given /^there are some unpublished articles$/ do
  3.times do |i|
    FactoryGirl.create(:article, title: "unpublished_#{i}")
  end
end

Then /^I should see unpublished articles$/ do
  Article.not_published.each do |a|
    page.should have_content(a.title)
  end
end

When /^I delete unupublished articles$/ do
  click_on 'Очистить все'
end

Then /^I should not see that unpublished articles$/ do
  Article.not_published.scoped.each do |a|
    page.should_not have_content(a.title)
  end
end

Given /^there is unpublished article$/ do
  FactoryGirl.create(:article, title: "unpublished")
end

When /^I delete single article$/ do
  click_on "Удалить"
end

Then /^I should not see that article$/ do
  page.should_not have_content("unpublished")
end

Given /^there are some published articles$/ do
  3.times do |i|
    @article = FactoryGirl.create(:article, title: "published_#{i}", published: true)
  end
end

Then /^I should see published articles$/ do
  Article.published.each do |article|
    page.should have_content(article.title)
  end
end

Given /^there are some articles in different categories$/ do
  cat1 = FactoryGirl.create(:category, name: "test_category1")
  cat2 = FactoryGirl.create(:category, name: "test_category2")
  art1 = FactoryGirl.create(:article, title: "test_article1", category: cat1, published: true)
  art2 = FactoryGirl.create(:article, title: "test_article2", category: cat2, published: true)
end

When /^I choose category$/ do
  within('.categories_list') do
    click_on "test_category1"
  end
end

When /^I choose category within article$/ do
  within("#article_#{Article.not_published.last.id}") do
    click_on "test_category1"
  end
end

Then /^I should see articles only from that category$/ do
  Category.find_by_name("test_category1").articles.each do |art|
    page.should have_content(art.title)
  end
  Category.find_by_name("test_category2").articles.each do |art|
    page.should_not have_content(art.title)
  end
end

When /^I click on article header$/ do
  click_on Article.first.title
end

Then /^I should see article text$/ do
  page.should have_content(Article.first.text)
end

Given /^there are articles from different news sources$/ do
  ns1 = FactoryGirl.create(:news_source, name: "test_news_source1")
  ns2 = FactoryGirl.create(:news_source, name: "test_news_source2")
  art1 = FactoryGirl.create(:article, title: "test_article1", news_source: ns1, published: true)
  art2 = FactoryGirl.create(:article, title: "test_article2", news_source: ns2, published: true)
end

When /^I choose news source$/ do
  within ".sources_list" do
    click_link "test_news_source1"
  end
end

Then /^I should see articles only from that source$/ do
  NewsSource.find_by_name("test_news_source1").articles.each do |art|
    page.should have_content(art.title)
  end
  NewsSource.find_by_name("test_news_source2").articles.each do |art|
    page.should_not have_content(art.title)
  end
end

Given /^there are some similar articles$/ do
  art_group = FactoryGirl.create(:article_group)
  2.times do |i|
    FactoryGirl.create(:article, article_group: art_group, title: "test_article#{i}", published: true)
  end
end

Then /^I should see similar articles$/ do
  page.should have_content('Похожие статьи')
end

When /^I train classificator$/ do
  Article.not_published.each do |a|
    within("#article_#{a.id}") do
      click_on "test_category_test"
    end
  end
end

When(/^I create new article manually$/) do
  FactoryGirl.create(:news_source, name: "Недельные обзоры")
  visit new_admin_article_path
  fill_in "Заголовок", with: "Статья, созданная вручную"
  fill_in "Текст", with: "<p>It is very hard thing</p>"
  fill_in "Сводка", with: "Обзор"
  select "Недельные обзоры", from: "Источник новости"
  check "Опубликовано"
  click_on "Создать статью"
end

Then(/^I should see manually created article$/) do
  page.should have_content "Статья, созданная вручную"
  page.should have_content "Обзор"
end

When(/^I edit article$/) do
  @article = FactoryGirl.create(:article, title: "old title", text: "old text")
  visit article_path(@article)
  click_on "Редактировать"
  fill_in "Заголовок", with: "new title"
  fill_in "Текст", with: "new text"
end

Then(/^I should see changes of edited article$/) do
  page.should have_content("new title")
  page.should have_content("new text")
end
