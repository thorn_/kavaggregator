Given /^I am on the homepage$/ do
  visit articles_path
end

Given /^I am on the categories administration path$/ do
  visit admin_categories_path
end

Given /^I am on the comments administration path$/ do
  visit admin_comments_path
end

Given /^I am on the keywords administration path$/ do
  visit admin_keywords_path
end

Given /^I am on the news sources administration path$/ do
  visit admin_news_sources_path
end

Given /^I am on the users administration page$/ do
  visit admin_users_path
end

Given /^I visit article administration page$/ do
  visit admin_articles_path
end

Given /^I am on the admin published articles path$/ do
  visit admin_published_articles_path
end

Given /^I am on the trends administration path$/ do
  visit admin_trends_path
end

Given /^I am on the tags administration path$/ do
  visit admin_tags_path
end
