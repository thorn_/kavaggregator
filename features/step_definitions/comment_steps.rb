#-*- encoding: utf-8 -*-
Given /^there are some comments$/ do
  3.times do |i|
    FactoryGirl.create(:comment, content: "test_comment_#{i}")
  end
end

Then /^I should see all comments$/ do
  Comment.scoped.each do |com|
    page.should have_content(com.content)
  end
end

Then /^I should see comments number$/ do
  page.should have_content("Всего: 3")
end

When /^I delete comment$/ do
  within("#comment_#{Comment.last.id}") do
    click_on "Удалить"
  end
end

Then /^I should see comment deleted marker$/ do
  page.should have_content("Удален")
end

When /^I comment article$/ do
  click_on Article.published.first.title
  click_on "Комментарии"
  fill_in "comment_content", with: "test_comment"

  click_on "Ответить"
end

Then /^I should see my comment$/ do
  page.should have_content("test_comment")
end

When /^I write a reply$/ do
  page.should have_content("sandwich")
  within ".comment" do
    click_link "Ответить"
    fill_in "comment_content", with: "test_comment_reply"
    click_button "Ответить"
  end
end

Then /^I should see that it is a reply$/ do
  page.should have_content("test_comment_reply")
end

When /^I delete a comment$/ do
  click_link("Удалить")
end

Then /^I should see that comment is deleted$/ do
  page.should have_content("Комментарий был удален")
end
