Given /^there are messages$/ do
  3.times do |i|
    FactoryGirl.create(:message, title: "Title #{i}")
  end
end

Then /^I should see last message$/ do
  page.should have_content("Title 2")
end

When /^I click on message$/ do
  click_on "Title 2"
end

Then /^I should see all messsages$/ do
  3.times do |i|
    page.should have_content("Title #{i}")
  end
end
