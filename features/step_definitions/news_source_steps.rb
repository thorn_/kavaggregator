#-*- encoding: utf-8 -*-

def news_source_template
  @key = {name: "test_news_source"}
end

def create_news_source
  visit new_admin_news_source_path
  fill_in "Имя", with: @key[:name]
  click_on "Создать источник новостей"
end

Given /^there are some news sources$/ do
  1.times do |i|
    FactoryGirl.create(:news_source, name: "test_news_source#{i}")
  end
end

Then /^I should see all news sources$/ do
  Keyword.scoped.each do |cat|
    page.should have_content(cat.name)
  end
end

When /^I create valid news source$/ do
  news_source_template
  create_news_source
end

When /^I create invalid news source$/ do
  news_source_template
  @key[:name] = ''
  create_news_source
end

Then /^I should not see news source in news sources list$/ do
  visit admin_news_sources_path
  Keyword.scoped.each do |cat|
    page.should_not have_content(cat.name)
  end
end

Then /^I should see missing news source name warning$/ do
  page.should have_content("Источник новостей не добавлен")
end

When /^I delete news source$/ do
  click_on "Удалить"
end

When /^I edit news source with valid attributes$/ do
  click_on "Изменить"
  fill_in "Имя", with: "New news_source name"
  click_on "Сохранить"
end

Then /^I should see new name of news source$/ do
  page.should have_content("New news_source name")
end

When /^I edit news source with invalid attributes$/ do
  click_on "Изменить"
  fill_in "Имя", with: ""
  click_on "Сохранить"
end

Then /^I should see invalid news source warning$/ do
  page.should have_content("не может быть пустым")
end
