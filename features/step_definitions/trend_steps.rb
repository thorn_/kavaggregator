#-*- encoding: utf-8 -*-
Given /^there are some active trends$/ do
  3.times do |i|
    @trend = FactoryGirl.create(:trend, name: "trend_#{i}", active: true)
  end
end

Then /^I should see trends$/ do
  3.times do |i|
    page.should have_content("trend_#{i}")
  end
end

Given /^there are some active trends with articles$/ do
  3.times do |i|
    trend = FactoryGirl.create(:trend, name: "trend_#{i}", description: "trend_description", active: true)
    FactoryGirl.create(:article, trend_id: trend.id, title: "article_1_from_trend_#{i}", published: true)
    FactoryGirl.create(:article, trend_id: trend.id, title: "article_2_from_trend_#{i}", published: true)
  end
end

When /^I click on trend$/ do
  within(".trends") do
    click_on "trend_1"
  end
end

Then(/^meta attributes should have trend description$/) do
  trend = Trend.find_by_name('trend_1')
  page.should have_selector('meta[name=description][content="'+ trend.description+ '"]', visible: false)
end

Then /^I should see news of that trend$/ do
  page.should have_content("article_1_from_trend_1")
  page.should have_content("article_2_from_trend_1")
end


def trend_template
  @trend = {name: "test_trend", description: "test_trend description"}
end

def create_trend
  visit new_admin_trend_path
  fill_in "Имя", with: @trend[:name]
  fill_in "Описание", with: @trend[:description]
  click_on "Создать тренд"
end

Given /^there are some trends$/ do
  1.times do |i|
    FactoryGirl.create(:trend, name: "test_trend#{i}")
  end
end

Then /^I should see all trends$/ do
  Trend.scoped.each do |trend|
    page.should have_content(trend.name)
  end
end

When /^I create valid trend$/ do
  trend_template
  create_trend
end

When /^I create invalid trend$/ do
  trend_template
  @trend[:name] = ''
  create_trend
end

Then /^I should not see trend in trends list$/ do
  visit admin_trends_path
  Trend.scoped.each do |trend|
    page.should_not have_content(trend.name)
  end
end

Then /^I should see missing trend name warning$/ do
  page.should have_content("не может быть пустым")
end

When /^I delete trend$/ do
  click_on "Удалить"
end

When /^I edit trend with valid attributes$/ do
  click_on "Изменить"
  fill_in "Имя", with: "New trend name"
  click_on "Сохранить"
end

Then /^I should see new name of trend$/ do
  page.should have_content("New trend name")
end

When /^I edit trend with invalid attributes$/ do
  click_on "Изменить"
  fill_in "Имя", with: ""
  click_on "Сохранить"
end

Given /^there is test trend$/ do
  FactoryGirl.create(:trend, name: "test_trend1")
end
