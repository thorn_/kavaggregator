#-*- encoding: utf-8 -*-

def category_template
  @cat = {name: "test_category"}
end

def create_category
  visit new_admin_category_path
  fill_in "Имя", with: @cat[:name]
  click_on "Создать категорию"
end

Given /^there are some categories$/ do
  1.times do |i|
    FactoryGirl.create(:category, name: "test_category#{i}")
  end
end

Then /^I should see all categories$/ do
  Category.scoped.each do |cat|
    page.should have_content(cat.name)
  end
end

When /^I create valid category$/ do
  category_template
  create_category
end

When /^I create invalid category$/ do
  category_template
  @cat[:name] = ''
  create_category
end

Then /^I should not see category in categories list$/ do
  visit admin_categories_path
  page.should_not have_content("test_category")
end

Then /^I should see missing category name warning$/ do
  page.should have_content("не может быть пустым")
end

When /^I delete category$/ do
  click_on "Удалить"
end

When /^I edit category with valid attributes$/ do
  click_on "Изменить"
  fill_in "Имя", with: "New category name"
  click_on "Сохранить"
end

Then /^I should see new name of category$/ do
  page.should have_content("New category name")
end

When /^I edit category with invalid attributes$/ do
  click_on "Изменить"
  fill_in "Имя", with: ""
  click_on "Сохранить"
end

Given /^there is test category$/ do
  FactoryGirl.create(:category, name: "test_category_test")
end
