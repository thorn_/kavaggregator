#-*- encoding: utf-8 -*-

def tag_template
  @tag = {name: "test_tag"}
end

def create_tag
  visit new_admin_tag_path
  fill_in "Имя", with: @tag[:name]
  click_on "Создать тег."
end

Given /^there are some tags$/ do
  1.times do |i|
    FactoryGirl.create(:tag, name: "test_tag#{i}")
  end
end

Then /^I should see all tags$/ do
  Tag.scoped.each do |tag|
    page.should have_content(tag.name)
  end
end

When /^I create valid tag$/ do
  tag_template
  create_tag
end

When /^I create invalid tag$/ do
  tag_template
  @tag[:name] = ''
  create_tag
end

Then /^I should not see tag in tags list$/ do
  visit admin_tags_path
  page.should_not have_content("test_tag")
end

Then /^I should see missing tag name warning$/ do
  page.should have_content("не может быть пустым")
end

When /^I delete tag$/ do
  click_on "Удалить"
end

When /^I edit tag with valid attributes$/ do
  click_on "Изменить"
  fill_in "Имя", with: "New tag name"
  click_on "Сохранить"
end

Then /^I should see new name of tag$/ do
  page.should have_content("New tag name")
end

When /^I edit tag with invalid attributes$/ do
  click_on "Изменить"
  fill_in "Имя", with: ""
  click_on "Сохранить"
end

Given /^there is test tag$/ do
  FactoryGirl.create(:tag, name: "test_tag_test")
end
