# -*- encoding: utf-8 -*-
Then /^I should see them on published article administration list$/ do
  visit admin_published_articles_path
  Article.published.each do |art|
    page.should have_content(art.title)
  end
end

When /^press group button$/ do
  click_on "Группировать"
end

Then /^I should see that articles are grouped$/ do
  page.should have_content("Открепить")
end

When /^I delete article$/ do
  @article = Article.published.first
  within("#article_#{@article.id}") do
    click_on "Удалить"
  end
end

Then /^I should not see it on published article administration list$/ do
  page.should_not have_content(@article.title)
end

When /^I ungroup articles$/ do
  click_on "Открепить"
end

When /^there are two articles for grouping$/ do
  @article_for_grouping_1 = FactoryGirl.create(:article, published: true, title: "article for grouping 1")
  @article_for_grouping_2 = FactoryGirl.create(:article, published: true, title: "article for grouping 2")
end

When /^I check some articles for grouping$/ do
  [@article_for_grouping_1, @article_for_grouping_2].each do |a|
    within("#article_#{a.id}") do
      all('input#model_ids_group_[type=checkbox]').each {|el| el.set(true)}
    end
  end
end

Then /^articles should be ungrouped$/ do
  page.should_not have_content("Открепить")
end

Then /^there should be zero not publilhsed articles$/ do
  Article.not_published.each do |a|
    page.should_not have_content(a.title)
  end
end

When /^article has a trend$/ do
  @trend = FactoryGirl.create(:trend, active: true, name: "active_trend")
  @article = FactoryGirl.create(:article, published: true, trend_id: @trend.id, title: "Trend Article")
end

Then /^I should see it on the select box$/ do
  page.should have_select("article_trend_id", selected: @trend.name, visible: false)
end
