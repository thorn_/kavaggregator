# -*- encoding: utf-8 -*-
Feature: Loggin in as a user
  In order to read news with much more comfort
  As a user
  I want to log in to site

  Scenario: sign up
    When I sign up with valid credentials
    Then I should see welcome message

  Scenario: sign up without name
    When I sign up without name
    Then I should see missing name message

  Scenario: Sign up without password
    When I sign up without password
    Then I should see invalid password message

  Scenario: Sign up without email
    When I sign up without email
    Then I should see invalid email message
