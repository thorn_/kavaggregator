# -*- encoding: utf-8 -*-
Feature: authorization
  In order to use system on its' full
  As user
  I want to log in my account

  Scenario: login from main page
    Given I am registered user
    When I enter valid sign in credentials
    Then I should be in my account
    And I should see welcome message

  Scenario: fail login
    Given I am registered user
    When I enter invalid sign in credentials
    Then I should see invalid login password warning

  Scenario: sign out
    Given I am successfully signed in
    When I try to sign out
    Then I should be on main page
