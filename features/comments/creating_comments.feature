# -*- encoding: utf-8 -*-
Feature: Commenting
  In order to express my opinion
  As user or administrator
  I want to write comment like a keyboard hero

  Background:
    Given there are some published articles
    And I am successfully signed in

  Scenario: Writing comment as user
    When I comment article
    Then I should see my comment

  # bootstrap tabs don't work without javascript, so
  # don't know how to test it carefully
  # Scenario: Writing reply
  #   When I comment article
  #   And I write a reply
  #   Then I should see that it is a reply

  Scenario: Deleting comment
    When I comment article
    And I delete a comment
    Then I should see that comment is deleted
