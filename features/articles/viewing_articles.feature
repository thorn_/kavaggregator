# -*- encoding: utf-8 -*-
Feature: Viewing articles
  In order to see what's happened in the world
  As a user
  I want to visit main page and read some news

  Scenario: reading all articles
    Given there are some published articles
    And I am on the homepage
    Then I should see published articles

  Scenario: reading articles using category filter
    Given there are some articles in different categories
    And I am on the homepage
    When I choose category
    Then I should see articles only from that category

  Scenario: reading articles from particular news source
    Given there are articles from different news sources
    And I am on the homepage
    When I choose news source
    Then I should see articles only from that source

  Scenario: reading one article
    Given there are some published articles
    And I am on the homepage
    When I click on article header
    Then I should see article text

  Scenario: viewing similar articles
    Given there are some similar articles
    And I am on the homepage
    When I click on article header
    Then I should see similar articles
