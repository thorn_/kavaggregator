# -*- encoding: utf-8 -*-
FactoryGirl.define do

  sequence :url do |n|
    "http://www.example#{n}.com/feed.xml"
  end

  factory :article do
    title "LOREM IPSUM"
    text "Lorem ipsum dolor sit amet"
    summary "summary"
    category
    news_source
    published false
    article_group
    img_url Article::DEFAULT_IMAGE
    created_at Date.today
    trend
    rating 0
    tags {[FactoryGirl.create(:tag)]}
  end

  factory :tag do
    name "tag_name"
  end

  factory :category do
    name "category_name"
  end

  factory :message do
    title "Title"
    text "Text"
  end

  factory :feature do
    count 1
    name "feature"
    association :category
  end

  factory :keyword do
    name "Name"
  end

  factory :news_source do
    name "Source1"
    url
    period "15"
    active true
  end

  factory :user do
    name 'simpleton'
    email
    password 'please'
    password_confirmation 'please'
    confirmed_at Time.now
    confirmation_token nil
    role "user"
  end

  factory :comment do
    user
    content "Comment content"
    article
  end

  factory :article_group do
  end

  factory :trend do
    name "trend"
    description "trend description"
    active false
  end

  sequence :email do |n|
    "example_#{n}@example.com"
  end
end
