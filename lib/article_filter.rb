# -*- encoding: utf-8 -*-
class ArticleFilter

  def self.filter(params = {})
    category = Category.find_by_id(params[:cat_id].to_i)
    category_id = category ? category.id : nil

    trend = Trend.find_by_id(params[:trend_id].to_i)
    trend_id = trend ? trend.id : nil

    tag = Tag.find_by_id(params[:tag_id].to_i)
    tag_id = tag ? tag.id : nil

    source   = NewsSource.order("created_at DESC").find_all_by_id(params[:source_id]).map(&:id)

    page = params[:page].nil? ? 1 : params[:page].to_i
    per_page = params[:per_page].nil? ? 15 : params[:per_page].to_i
    per_page = 30 if per_page > 30
    query = params[:query]

    ordering = params[:order] || "rating DESC, created_at DESC"

    if query
      articles = Article.text_search(query).page(page).per_page(per_page)
      total_count = articles.count
    else
      common_text = "
          COUNT(*) OVER() as total_count,
          max(articles.created_at) as created_at,
          max(articles.rating) as rating,
          max(articles.id) as id,
          articles.article_group_id,
          published"
      if tag_id
        articles = tag.articles.published.select("
          #{common_text}
          ").group("article_group_id, published, trend_id").order(ordering).page(page).per_page(per_page)
      elsif trend_id
        articles = Article.published.select("
          #{common_text},
          trend_id as trend_id"
        ).group("article_group_id, published, trend_id").order(ordering).where(trend_id: trend_id).page(page).per_page(per_page)
      elsif category and source.length > 0
        articles = Article.published.select("
          #{common_text},
          category_id as category_id,
          news_source_id as news_source_id"
        ).group("article_group_id, published, category_id, news_source_id").order(ordering).where(category_id: category_id, news_source_id: source).page(page).per_page(per_page)
      elsif category and source.length == 0
        articles = Article.published.select("
          #{common_text},
          category_id as category_id"
        ).group("article_group_id, category_id, published").order(ordering).where("category_id = ?", category_id).page(page).per_page(per_page)
      elsif source.length > 0 and category.nil?
        articles = Article.published.select("
          #{common_text},
          news_source_id as news_source_id"
        ).group("article_group_id, news_source_id, published").order(ordering).where(news_source_id: source).page(page).per_page(per_page)
      else
        articles = Article.published.select("
          #{common_text}"
        ).group("article_group_id, published").order(ordering).page(page).per_page(per_page)
      end
      total_count = (articles.length.zero?) ? 0 : articles[0].total_count.to_i
      articles = Article.includes(:tags).where(id: articles.map(&:id)).order(ordering)
    end
    {articles: articles, page: page.to_i, per_page: per_page, cat_id: category_id, source_id: source, total_count: total_count, trend_id: trend_id, tag_id: tag_id}
  end

end
