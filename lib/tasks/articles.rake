namespace :articles do
  desc 'Calculate hotness of tasks for period of time'
  task calculate_hotness: :environment do
    articles = Article.where("created_at > ?", Date.today - 2)
    # articles = Article.order("created_at DESC").limit(50)
    articles.map(&:calculate_hotness!)
  end
end
