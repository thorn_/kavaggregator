# -*- encoding: utf-8 -*-
require "lingua/stemmer"

class Checker
  def initialize(text)
    @text = Nokogiri::HTML(text).text
  end

  def check(params = {})
    raise RuntimeError, "Abstract method called"
  end

  def word_on_text(word)
    @text.scan(/#{word}/i).length
  end

  def word_count
    stemmer= Lingua::Stemmer.new(:language => "ru")
    @text.split(/[^A-Za-zА-Яа-я]+/i).select{|w| w.length > 3 and w.length < 20 }
      .uniq.collect { |w| stemmer.stem(w.strip.chomp.mb_chars.downcase.to_s) }.uniq.length
  end
end

class ArticleLengthCheck < Checker
  def initialize(options)
    super options
  end

  def check(params = {})
    length = params.fetch(:length, 30)
    word_count > length
  end
end

class RequiredWordsCheck < Checker
  def initialize(params)
    super params
  end

  def check(params = {})
    required_words = Keyword.required
    false if required_words.empty?
    for word in required_words do
      return true if word_on_text(word.name) > 0
    end
    return false
  end
end

class ExcludedWordCheck < Checker
  def initialize(params)
    super params
  end

  def check(params = {})
    required_words = Keyword.not_included
    true if required_words.empty?
    for word in required_words do
      return false if word_on_text(word.name) > 0
    end
    return true
  end
end
