#-*- encoding: utf-8 -*-
require "nokogiri"

class Page
  attr_reader :text
  def initialize(url)
    @text = get_page(url)
  end

  def image
    html.at_css('img') && html.at_css('img')["src"]
  end

  def summary(length = 300)
    if real_text.length > length
      real_text[0...length] + '...'
    else
      real_text
    end
  end

  def process
    @text = yield(@text)
  end

  private

  def html
    Nokogiri::HTML(@text)
  end

  def real_text
    Nokogiri::HTML(@text).text
  end

  def get_page(url)
    request = Curl::Easy.perform(url) do |curl|
      curl.follow_location = true
      curl.max_redirects = 3
      curl.headers["User-Agent"] = 'Mozilla/5.0 (X11; Linux i686) AppleWebKit/535.7 (KHTML, like Gecko) Chrome/16.0.912.77 Safari/535.7'
    end
    Nokogiri::HTML(request.body_str).to_s.encode("UTF-8")
  rescue Exception => m
    return ""
  end
end
