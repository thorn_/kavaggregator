# -*- encoding: utf-8 -*-
class Grouper

  def self.group(list)
    list.each { |g| g.sort! }
    result = list.inject([]) do |res, gr1|
      current_group = recursive_finder(list, gr1)
      res << current_group.sort
    end
    result | list
  end

  def self.recursive_finder(list, group)
    current_group = group
    list.each do |gr2|
      if similar? current_group, gr2
        list.delete group
        list.delete gr2
        current_group |= gr2 | recursive_finder(list, gr2)
      end
    end
    current_group
  end

  def self.similar?(list1, list2)
    for el in list1
      return true if list2.include?(el)
    end
    false
  end

end
