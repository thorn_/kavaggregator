# -*- encoding: utf-8 -*-
require "nokogiri"

class PageProcessor

  def self.process(html, processors = [], options = {})
    soup = get_main_content(html, options[:find])
    processors.each do |p|
      processor = p.new(options.merge(soup: soup))
      processor.process
    end
    return soup.to_s.encode("UTF-8")
  end

  def self.get_main_content(html, selector = nil)
    selector.nil? ? Nokogiri::HTML(html).css("*").first : Nokogiri::HTML(html).css(selector)
  end

end

class Processor
  def initialize(options)
    @soup = options[:soup]
  end

  def process
    raise "Abstract method was called"
  end
end

class ImageProcessor < Processor
  def initialize(options = {host: "http://kavigator.ru"})
    super options
    @host = options[:host] || "http://example.com"
    @injections = [
      "новости дагестана",
      "новости дагестана сегодня",
      "новости дагестана последние",
      "новости дагестан #{Date.today.year}",
      "новости дагестана свежие",
      "риа новости дагестан сегодня",
      "криминальные новости дагестана",
      "новости дагестана криминал",
      "дагестан буйнакск новости",
      "дагестан новости махачкале",
      "новости дагестана хасавюрт",
      "новости республики дагестан",
      "новости дня в дагестане"]
  end

  def process

    @soup.css('img').each do |img|
      if img["src"]
        img["src"] = "http://" + @host + img["src"] if img["src"][0] == '/'
        img["alt"] = (img["alt"] || "") + ". " + @injections.sample
        img["rel"] = "nofollow"
      else
        img.remove
      end
    end
    @soup
  end
end

class IgnoredProcessor < Processor
  def initialize(options )
    super options
    @ignored = options[:ignored] || "ignored"
    @blacklist = "#{@ignored}, script, style, link, iframe, textarea, input"
  end

  def process
    @soup.css(@blacklist).remove
  end
end

class AttributeProcessor < Processor
  def initialize(options)
    super options
    @blacklist = ["style", "width", "height", "class"] + (options[:ignored_attributes] || [])
  end

  def process
    @blacklist.each do |attribute|
      @soup.css("[#{attribute}]").each { |el| el.remove_attribute(attribute) }
    end
  end
end

class LinkProcessor < Processor
  def initialize(options)
    super options
    @host = options[:host] || "http://kavigator.ru"
  end

  def process
    @soup.css('a').each do |link|
      if link["href"]
        next if link["data-href"] && link["href"] = link["data-href"]
        next if link["href"][0] == "#"
        link["target"] = "_blank"
        link["href"] = "http://" + @host + link["href"] if link["href"][0] == '/'
      else
        link.remove unless link["name"]
      end
    end
    @soup
  end
end
