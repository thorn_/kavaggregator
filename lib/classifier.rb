# -*- encoding: utf-8 -*-
require "lingua/stemmer"
require 'zlib'

def get_words(text)
  stemmer= Lingua::Stemmer.new(:language => "ru")
  text.split(/[^A-Za-zА-Яа-я]+/i).select{|w| w.length > 3 and w.length < 20 }
    .uniq.collect { |w| stemmer.stem(w.strip.chomp.mb_chars.downcase.to_s) }.uniq
end

def words_count(source)
  source.split(/[^A-Za-zА-Яа-я]+/i).select{|w| w.length > 3 and w.length < 20 }.count
end

class Classifier

  def initialize(get_features)
    @get_features = get_features
    @categories ||= Category.all
  end

  def inc_f(f, cat)
    feature = Feature.find_or_initialize_by_name_and_category_id(f,cat.id)
    if feature.new_record?
      feature.save
    else
      feature.update_attributes(count: feature.count + 1)
    end
  end

  def inc_c(cat)
    cat.update_attributes(count: cat.count + 1)
  end

  def f_count(f, cat)
    res = Feature.where(name: f, category_id: cat.id).first
    return 0 if res.nil?
    res.count
  end

  def total_count
    Category.sum :count
  end

  def train(item, cat)
    features = send(@get_features,item)
    features.each {|f| inc_f(f, cat) }

    inc_c(cat)
  end

  def f_prob(f, cat)
    cat.count == 0 ? 0 : f_count(f, cat).to_f / cat.count
  end

  def weighted_prob(f, cat, prf, weight = 1, ap = 0.5)
    basic_prob = send(prf, f, cat)
    totals = @categories.inject(0){|sum, c| sum += f_count(f, c)}

    ((weight * ap) + (totals * basic_prob)) / (weight + totals)
  end


  def clear
    Feature.destroy_all
    @categories.each { |c| c.count = 0; c.save }
    Article.all.each { |a| a.update_attributes(category_id: nil, published: false) }
  end
end

class FisherClassifier < Classifier

  def initialize(get_features = :get_words)
    super
    @minimums = {}
  end

  def c_prob(f, cat)
    cl_f = f_prob(f, cat)
    return 0 if cl_f.zero?

    freq_sum = @categories.inject(0){ |sum, c| sum += f_prob(f, c)}

    cl_f / freq_sum
  end

  def classify(item, default = nil)
    best = default
    max = 0.0
    @categories ||= Category.all
    @categories.each do|c|
      begin
        p = fisher_prob(item, c)
        if p > c.minimum and p > max
          best = c
          max = p
        end
      rescue
        next
      end
    end
    [best, max]
  end

  def fisher_prob(item, cat)
    features = send(@get_features, item)
    p = features.inject(1) { |p, f| p *= weighted_prob(f, cat, :c_prob) }

    f_score = -2 * Math::log(p)

    inv_chi2(f_score, features.length)
  end

  def inv_chi2(chi, df)
    m = chi / 2.0
    sum = term = Math.exp(-m)

    (1...df).each do |i|
      term *= m / i
      sum += term
    end
    [sum, 1.0].min
  end
end
