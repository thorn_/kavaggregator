# -*- encoding: utf-8 -*-
require "nokogiri"
require "date"
require "grouper"

class ArticleGrouper

  def initialize(articles, percent = 25)
    @articles = articles
    material = articles.inject({}){|res, a| res.merge(a.id => a.text) }
    @shingles = Shingulizer.shingulize(material)
    @percent = percent / 100.0
  end

  def group
    group_article_ids.each { |gr| modify_articles(gr) }
  end

  def modify_articles(id_group)
    if id_group.length > 0
      new_group = ArticleGroup.create
      @articles.where(id: id_group).order("created_at DESC").each do |a|
        a.article_group.articles.each do |article|
          article.update_attributes(article_group_id: new_group.id)
        end
      end
    end
  end

  def group_article_ids
    Grouper.group(self.compare_all_to_all)
  end

  def compare_all_to_all
    results = []
    for shinglei in @shingles do
      current_group = [shinglei[0]]
      lengthi = shinglei[1].length
      for shinglej in @shingles do
        lengthj = shinglej[1].length
        if (shinglei[0] != shinglej[0]) and ((lengthi - lengthj).abs < lengthi) and ((lengthi - lengthj).abs < lengthj)
          current_group << shinglej[0] if compare(shinglei[1], shinglej[1]) > @percent
        end
      end
      results << current_group
    end
    results
  end

  def compare (source1,source2)
    same = 0
    source1.each{|val| same+=1 if source2.include?(val)}
    same * 2 / ((source1.length + source2.length).to_f || 1)
  end
end

class Shingulizer

  def self.shingulize(material)
    @material = material
    return shingles
  end

  def self.shingles
    result = @material.inject({}) do |res, article|
      res.merge(article[0] => genshingle(canonize(Nokogiri::HTML(article[1]).text)))
    end
    result
  end

  def self.genshingle(source)
    shingleLen = 2
    out = []
    for i in (0..source.length -  shingleLen - 1) do
      str = source[i...i + shingleLen].inject([]) do |sum, val|
        sum << val.encode('UTF-8')
      end
      out << Zlib::crc32(str.join(''))
    end
    out
  end

  def self.canonize(source)
    stemmer= Lingua::Stemmer.new(language: "ru")
    source.split(/[^A-Za-zА-Яа-я]+/i).select{|w| w.length > 3 and w.length < 20 }\
      .collect { |w| stemmer.stem(w.strip.chomp.mb_chars.downcase.to_s) }
  end
end
