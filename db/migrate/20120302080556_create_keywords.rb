class CreateKeywords < ActiveRecord::Migration
  def change
    create_table :keywords do |t|
      t.string  :name
      t.string  :public_name
      t.integer :word_type, default: 1

      t.timestamps
    end
  end
end
