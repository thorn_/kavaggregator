class CreateVisitedPages < ActiveRecord::Migration
  def change
    create_table :visited_pages do |t|
      t.string :url, null: false

      t.timestamps
    end
  end
end
