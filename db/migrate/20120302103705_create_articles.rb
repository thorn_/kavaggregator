class CreateArticles < ActiveRecord::Migration
  def change
    create_table :articles do |t|
      t.text    :text
      t.text    :title
      t.text    :url
      t.text    :img_url
      t.boolean :published, default: false
      t.integer :news_source_id
      t.integer :category_id
      t.text    :summary
      t.integer :article_group_id
      t.integer :trend_id

      t.timestamps
    end
  end
end
