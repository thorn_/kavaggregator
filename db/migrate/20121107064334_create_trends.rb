class CreateTrends < ActiveRecord::Migration
  def change
    create_table :trends do |t|
      t.string :name, not_null: true, default: ""
      t.text :description, :text
      t.boolean :active, not_null: true, default: false
      t.timestamps
    end
  end
end
