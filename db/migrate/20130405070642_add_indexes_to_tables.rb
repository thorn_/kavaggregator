class AddIndexesToTables < ActiveRecord::Migration
  def change
    add_index :articles, :news_source_id
    add_index :articles, :category_id
    add_index :articles, :article_group_id
    add_index :articles, :trend_id

    add_index :comments, :article_id
    add_index :comments, :user_id
  end
end
