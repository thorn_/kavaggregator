class AddAttributesToUsers < ActiveRecord::Migration
  def change
    change_table :users do |t|
      t.string :provider
      t.string :uid
      t.string :name
      t.string :role, default: "user"
    end
  end
end
