class CreateComments < ActiveRecord::Migration
  def change
    create_table :comments do |t|
      t.string  :content, null: false
      t.integer :article_id, null: false
      t.integer :user_id, null: false
      t.string  :ancestry
      t.boolean :deleted, null: false, default: false

      t.timestamps
    end
  end
end
