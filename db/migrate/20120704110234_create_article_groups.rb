class CreateArticleGroups < ActiveRecord::Migration
  def change
    create_table :article_groups do |t|
      t.integer :articles_count, default: 0, null: false
      t.timestamps
    end
  end
end
