class CreateNewsSources < ActiveRecord::Migration
  def change
    create_table :news_sources do |t|
      t.string  :name, null: false
      t.string  :url, null: false
      t.text    :find_helper, null: false, default: "body"
      t.text    :ignore_helper, null: false, default: "noignore"
      t.integer :period, default: 20
      t.boolean :active, null: false, default: true

      t.timestamps
    end
  end
end
