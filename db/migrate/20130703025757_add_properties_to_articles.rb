class AddPropertiesToArticles < ActiveRecord::Migration
  def up
    add_column :articles, :properties, :hstore
    execute "CREATE INDEX articles_properties ON articles USING GIN(properties)"
  end

  def down
    execute "DROP INDEX articles_properties"
    remove_column :articles, :properties
  end
end
