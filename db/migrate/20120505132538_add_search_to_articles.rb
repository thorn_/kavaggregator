class AddSearchToArticles < ActiveRecord::Migration
  def up
    execute "create index articles_title on articles using gin(to_tsvector('russian', title))"
    execute "create index articles_text on articles using gin(to_tsvector('russian', text))"
  end

  def down
    execute "drop index articles_name"
    execute "drop index articles_content"
  end
end
