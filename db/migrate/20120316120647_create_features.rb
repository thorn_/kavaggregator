class CreateFeatures < ActiveRecord::Migration
  def change
    create_table :features do |t|
      t.string  :name
      t.integer :category_id
      t.integer :count, default: 1


      t.timestamps
    end
    add_index :features, :category_id
    add_index :features, :name
  end
end
