# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20120505132538) do

  create_table "articles", :force => true do |t|
    t.text     "text"
    t.text     "title"
    t.text     "url"
    t.text     "img_url"
    t.boolean  "published",      :default => false
    t.integer  "news_source_id"
    t.integer  "category_id"
    t.datetime "created_at",                        :null => false
    t.datetime "updated_at",                        :null => false
    t.text     "summary"
  end

  create_table "categories", :force => true do |t|
    t.string   "name"
    t.integer  "count",                                    :default => 0
    t.decimal  "minimum",    :precision => 3, :scale => 2, :default => 0.0
    t.datetime "created_at",                                                :null => false
    t.datetime "updated_at",                                                :null => false
  end

  create_table "comments", :force => true do |t|
    t.string   "content"
    t.integer  "article_id"
    t.integer  "user_id"
    t.string   "ancestry"
    t.boolean  "deleted",    :default => false
    t.datetime "created_at",                    :null => false
    t.datetime "updated_at",                    :null => false
  end

  create_table "events", :force => true do |t|
    t.string   "title"
    t.string   "address"
    t.text     "description"
    t.datetime "start_at"
    t.datetime "end_at"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  create_table "features", :force => true do |t|
    t.string   "name"
    t.integer  "category_id"
    t.integer  "count",       :default => 1
    t.datetime "created_at",                 :null => false
    t.datetime "updated_at",                 :null => false
  end

  add_index "features", ["category_id"], :name => "index_features_on_category_id"
  add_index "features", ["name"], :name => "index_features_on_name"

  create_table "identities", :force => true do |t|
    t.string   "name"
    t.string   "email"
    t.string   "password_digest"
    t.datetime "created_at",      :null => false
    t.datetime "updated_at",      :null => false
  end

  create_table "keywords", :force => true do |t|
    t.string   "name"
    t.string   "public_name"
    t.integer  "word_type",   :default => 1
    t.datetime "created_at",                 :null => false
    t.datetime "updated_at",                 :null => false
  end

  create_table "news_sources", :force => true do |t|
    t.string   "name"
    t.string   "url"
    t.text     "find_helper",   :default => "body"
    t.text     "ignore_helper", :default => "noignore"
    t.integer  "period",        :default => 20
    t.boolean  "active",        :default => true
    t.datetime "created_at",                            :null => false
    t.datetime "updated_at",                            :null => false
  end

  create_table "users", :force => true do |t|
    t.string   "provider"
    t.string   "uid"
    t.string   "name"
    t.string   "salt"
    t.string   "role"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "visited_pages", :force => true do |t|
    t.string   "url"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

end
