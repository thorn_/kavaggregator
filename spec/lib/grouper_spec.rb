describe Grouper do

  before(:each) do
    @list = [[1,2],[2,6],[3],[4,5],[5,1]]
  end

  it "should return true if two arrays have similar element" do
    list1 = [1,2,3]
    list2 = [3,4,5]
    Grouper.similar?(list1, list2).should be true
  end

  it "should return false if there are no common element" do
    list1 = [1,2,3]
    list2 = [4,5,5]
    Grouper.similar?(list1, list2).should be false
  end

  it "should return the list of groups" do
    Grouper.group(@list).should == [[1,2,4,5,6],[3]]
  end

  it "should return identical array when there are no common elements" do
    list2 = [[1,2],[3,4],[5,6]]
    Grouper.group(list2).should == [[1,2],[5,6],[3,4]]
  end

  it "should return exact 1 array in array when all elements are common" do
    list2 = [[1,2],[2,1]]
    Grouper.group(list2).should == [[1,2]]
  end

end
