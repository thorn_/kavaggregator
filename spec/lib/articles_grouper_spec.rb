# -*- encoding: utf-8 -*-
require "spec_helper"
require "date"

describe ArticleGrouper do
  before(:each) do
    @article1 = FactoryGirl.create(:article, text: '
      <div class="newstext normal">
        <p>Национальный антитеррористический комитет (НАК) назвал имена всех шести боевиков, уничтоженных в ходе спецоперации со 2 по 4 апреля в Сергокалинском районе Дагестана, сообщает РИА Новости ria.ru.
          <br><br>
          По сообщению ведомства, данная бандгруппа, возглавляемая турецким наемником Муханнедом, известным также под кличкой Шейх Абдусалам, причастна к целому ряду преступлений террористической направленности, покушениям на жизнь и убийствам гражданских лиц, священнослужителей, военнослужащих и сотрудников полиции, а также вымогательствам.
          <br><br>
          <a href="#me">ME</a>
          <a name="me">ME</a>
          <a>EMPTY LINK</a>
          <a href="/news/1333">Relative Link</a>
          Помимо <a href="/news/258385/">опознанных</a> ранее активного члена бандгруппы Рашида Газалиева и пособника боевиков Магомеда Газалиева, среди боевиков находился Салман Будайчиев 1984 года рождения, уроженец села Бурдеки Сергокалинского района (кличка Хузейфа). Был "завербован" боевиками в июне 2010 года, когда он приехал из Челябинской области на свадьбу своей родственницы, после чего "ушел в лес" и примкнул к бандподполью.
          <br><br> Также <a href="/news/258367/">уничтожен</a> Руслан Капиев 1984 года рождения, уроженец села Сергокала Сергокалинского района (кличка Адам), активный участник бандгруппы с апреля 2011 года. По оперативным данным, он причастен к убийству 2 мая 2011 года начальника сергокалинского РОВД Насрулы Магомедова.
          <br><br> Среди боевиков был Ильяс Закарьяев 1984 года рождения, уроженец села Сергокала Сергокалинского района, кличка Абдулазиз. Активный участник бандгруппы, 4 мая 2011 года он принимал участие в обстреле полицейских-кинологов, в результате чего погиб один сотрудник. Вместе с Капиевым принимал участие в убийстве начальника сергокалинского РОВД.
          <br><br> <em>"От своих родственников &ndash; сотрудников полиции </em><em>&ndash;</em><em> под угрозой физической расправы требовл уволтьсяиз правоохранительных органов"</em>, &ndash; отмечает НАК. <br><br> Также уничтожен Юсуп Халимбеков 1995 года рождения. Под воздествиембандитской пропаганды пять дней назад ушел из дома в селе Бурдеки Сергокалинского района и "взял в руки оружие", информирует НАК.
          <br><br> Блокированы и уничтожены боевики были в лесу около села Кадиркент. Как сообщает НАК, на месте боестолкновений и при нейтралзованныхбандитах обнаружены три противотанковых гранатомета, три пулемета Калашникова, четыре автомата, охотничий карабин с прицелом, приспообленныйпод снайперскую винтовку, подствольный гранатомет, большое количество патронов, гранаты, компоненты самодельных взрывных устройств иметодические пособия по их изготовлению, обмундирование.
          <br><br> <em>"Контртеррористическая операция в Сергокалинском районе продолжается, ведутся дальнейшие активные оперативно-боевые мероприятияи следственные действия", </em>&ndash; отмечает НАК.
          <img src="/assets/image.png">
          <img>
        </p>
        <textarea>
          текст
        </textarea>
        <script type="text/javascript">alert("that is to add comments");</script>
        <div class="advertisement"> Some shitty adverts or comments and so on</div>
        <input type="text" id="s" name="s" value="">
      </div>
      <div class="hidden">That should not be on text
        <p class="cont2" style="holy_crap">
          New Content
        </p>
      </div>', created_at: DateTime.now - 1)
    @article2 = FactoryGirl.create(:article, text: '
      <div class="newstext normal">
        <p>Национальный антитеррористический комитет (НАК) назвал имена всех шести боевиков, уничтоженных в ходе спецоперации со 2 по 4 апреля в Сергокалинском районе Дагестана, сообщает РИА Новости ria.ru.
          <br><br>
          По сообщению ведомства, данная бандгруппа, возглавляемая турецким наемником Муханнедом, известным также под кличкой Шейх Абдусалам, причастна к целому ряду преступлений террористической направленности, покушениям на жизнь и убийствам гражданских лиц, священнослужителей, военнослужащих и сотрудников полиции, а также вымогательствам.
          <br><br>
          <a href="#me">ME</a>
          <a name="me">ME</a>
          <a>EMPTY LINK</a>
          <a href="/news/1333">Relative Link</a>
          Помимо <a href="/news/258385/">опознанных</a> ранее активного члена бандгруппы Рашида Газалиева и пособника боевиков Магомеда Газалиева, среди боевиков находился Салман Будайчиев 1984 года рождения, уроженец села Бурдеки Сергокалинского района (кличка Хузейфа). Был "завербован" боевиками в июне 2010 года, когда он приехал из Челябинской области на свадьбу своей родственницы, после чего "ушел в лес" и примкнул к бандподполью.
          <br><br> Также <a href="/news/258367/">уничтожен</a> Руслан Капиев 1984 года рождения, уроженец села Сергокала Сергокалинского района (кличка Адам), активный участник бандгруппы с апреля 2011 года. По оперативным данным, он причастен к убийству 2 мая 2011 года начальника сергокалинского РОВД Насрулы Магомедова.
          <br><br> Среди боевиков был Ильяс Закарьяев 1984 года рождения, уроженец села Сергокала Сергокалинского района, кличка Абдулазиз. Активный участник бандгруппы, 4 мая 2011 года он принимал участие в обстреле полицейских-кинологов, в результате чего погиб один сотрудник. Вместе с Капиевым принимал участие в убийстве начальника сергокалинского РОВД.
          <br><br> <em>"От своих родственников &ndash; сотрудников полиции </em><em>&ndash;</em><em> под угрозой физической расправы требовл уволтьсяиз правоохранительных органов"</em>, &ndash; отмечает НАК. <br><br> Также уничтожен Юсуп Халимбеков 1995 года рождения. Под воздествиембандитской пропаганды пять дней назад ушел из дома в селе Бурдеки Сергокалинского района и "взял в руки оружие", информирует НАК.
          <br><br> Блокированы и уничтожены боевики были в лесу около села Кадиркент. Как сообщает НАК, на месте боестолкновений и при нейтралзованныхбандитах обнаружены три противотанковых гранатомета, три пулемета Калашникова, четыре автомата, охотничий карабин с прицелом, приспообленныйпод снайперскую винтовку, подствольный гранатомет, большое количество патронов, гранаты, компоненты самодельных взрывных устройств иметодические пособия по их изготовлению, обмундирование.
          <br><br> <em>"Контртеррористическая операция в Сергокалинском районе продолжается, ведутся дальнейшие активные оперативно-боевые мероприятияи следственные действия", </em>&ndash; отмечает НАК.
          <img src="/assets/image.png">
          <img>
        </p>
        <textarea>
          текст
        </textarea>
        <script type="text/javascript">alert("that is to add comments");</script>
        <div class="advertisement"> Some shitty adverts or comments and so on</div>
        <input type="text" id="s" name="s" value="">
      </div>
      <div class="hidden">That should not be on text
        <p class="cont2" style="holy_crap">
          New Content
        </p>
      </div>', created_at: DateTime.now - 2)
    @article3 = FactoryGirl.create(:article, text: "<div>lorem ipsum dolor sit amet 3</div>", created_at: DateTime.now - 3)
    @article4 = FactoryGirl.create(:article, text: "<div>lorem ipsum dolor sit amet 4</div>", created_at: DateTime.now - 4)
    @article5 = FactoryGirl.create(:article, text: "<div>holy crap, I'm a cake!</div>", created_at: DateTime.now - 5)
    @article6 = FactoryGirl.create(:article, text: "<div>holy crap, I'm a cake!</div>", created_at: DateTime.now - 6)
    @articles = Article.scoped
    @article_grouper = ArticleGrouper.new(@articles)
  end

  it "should convert compare all given articles to all" do
    @res = @article_grouper.compare_all_to_all()
    @res.should == [
      [@article1.id, @article2.id],
      [@article2.id, @article1.id],
      [@article3.id, @article4.id],
      [@article4.id, @article3.id],
      [@article5.id, @article6.id],
      [@article6.id, @article5.id]
    ]
  end

  it "should create a grouper and collapse shit above" do
    @article_grouper.group_article_ids().should == [
      [@article1.id, @article2.id],
      [@article3.id, @article4.id],
      [@article5.id, @article6.id]
    ]
  end

  it "should create a new group and assign it's id to similar articles" do
    lambda do
      @article_grouper.modify_articles([@article3.id, @article2.id])
    end.should change(ArticleGroup, :count).by(1)
    @article3.reload
    @article2.reload
    @article3.article_group.should == @article2.article_group
  end

  it "should group articles with group using all described above" do
    @article_grouper.group()
    @article1.reload
    @article2.reload
    @article3.reload
    @article4.reload
    @article5.reload
    @article6.reload
    @article1.article_group.articles.should include(@article1, @article2)
    @article3.article_group.articles.should include(@article3, @article4)
    @article5.article_group.articles.should include(@article5, @article6)
  end

  it "should not delete old bonds" do
    ag = ArticleGroup.create
    @article1 = FactoryGirl.create(:article, article_group_id: ag.id, text: "<div>holy crap, I'm a cake!</div>", created_at: DateTime.now - 1)
    @article6 = FactoryGirl.create(:article, article_group_id: ag.id, text: "sandwich", created_at: DateTime.now - 2)
    @article7 = FactoryGirl.create(:article, article_group_id: ag.id, text: "<div>holy crap, I'm a cake!</div>", created_at: DateTime.now - 1)
    articles = Article.where(id: [@article1.id,@article6.id, @article7.id])
    grouper = ArticleGrouper.new(articles)
    grouper.group()
    @article1.reload
    @article6.reload
    @article7.reload
    @article1.article_group.should == @article7.article_group
    @article1.article_group.should == @article6.article_group
  end


  describe Shingulizer do
    before(:each) do
      material = @articles.inject({}){|res,a| res.merge(a.id => a.text)}
      @s = Shingulizer.shingulize(material)
    end

    it "should make shingles from text and ids" do
      @s.class.should == Hash
      @s.first[0].class.should == Fixnum
      @s.first[1].class.should == Array
      @s.length.should == Article.count
    end
  end
end
