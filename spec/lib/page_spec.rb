# -*- encoding: utf-8 -*-
require 'spec_helper'
require 'nokogiri'

describe Page do

  describe "without processing" do
    before(:each) do
      VCR.use_cassette 'page_requests' do
        @page = Page.new("http://www.riadagestan.ru/news/2012/12/6/147520/")
      end
    end

    it "should get almost raw text" do
      @page.summary.should match(/Республиканское информационное агентство/)
    end

    it "should get first image" do
      @page.image.should == "http://static.riadagestan.ru/img/section_ico_in_world.gif"
    end

    it "summary length should be as much symbols as given with tree dots" do
      @page.summary(1).length.should eq(4) # + '...'
    end

    it "should get all page if there is large summary" do
      @page.summary(100000000).should eq(Nokogiri::HTML(@page.text).text)
    end
  end

  describe "with processing" do
    before(:each) do
      url = "http://www.riadagestan.ru/news/2012/12/6/147520/"
      host = URI.parse(url).host
      find = ".b-post"
      ignored = ".b-post__title, .b-post__date, .b-post__orfus, .b-post__like-it"

      VCR.use_cassette 'page_requests' do
        @page = Page.new(url)
      end

      @page.process do |html|
        PageProcessor.process(
          html,
          [ImageProcessor, IgnoredProcessor, AttributeProcessor, LinkProcessor],
          {find: find, ignored: ignored, host: host}
        )
      end
    end

    it "should get right summary" do
      @page.summary.should_not match(/Республиканское информационное агентство/)
    end

    it "should get right image" do
      @page.image.should == "http://s1.riadagestan.ru/riaimg/e5/45de/b1bf978253f782a2a5e0e688b9c4fbe3.c50c0cec2cd5095.91967970.jpeg"
    end

    it "should return blank text if page is not accessible" do
      Page.new("invalid url").text.should == ""
    end
  end

end
