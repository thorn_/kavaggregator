# -*- encoding: utf-8 -*-
require "spec_helper"

describe Checker do

  before(:each) do
    @ch = Checker.new("the sandwich on the wall")
  end

  it "should raise description when abstract method was called" do
    expect{@ch.check}.to raise_error(RuntimeError)
  end

  it "should return count of words in text" do
    @ch.word_on_text("sandwich").should eq(1)
    @ch.word_on_text("the").should eq(2)
  end

  it "should return right words count" do
    @ch.word_count.should eq(2)
  end
end

describe RequiredWordsCheck do
  it "should return true if there is even 1 required word on page" do
    FactoryGirl.create(:keyword, word_type: 1, name: "sandwich")
    checker = RequiredWordsCheck.new("what was that sandwich")
    checker.check.should be_true
  end

  it "should return false if there is no required words on text" do
    FactoryGirl.create(:keyword, word_type: 1, name: "sandwich")
    checker = RequiredWordsCheck.new("what was that")
    checker.check.should be_false
  end
end

describe ArticleLengthCheck do
  before(:each) do
    @checker = ArticleLengthCheck.new("some text about sandwich")
  end

  it "should return false if there is less words on article" do
    @checker.check(length: 40).should be_false
  end

  it "should return true if there is enough words on text" do
    @checker.check(length: 1).should be_true
  end
end

describe ExcludedWordCheck do
  it "should return true if there is even 1 required word on page" do
    FactoryGirl.create(:keyword, word_type: 3, name: "sandwich")
    checker = ExcludedWordCheck.new("what was that sandwich")
    checker.check.should be_false
  end

  it "should return false if there is no required words on text" do
    FactoryGirl.create(:keyword, word_type: 3, name: "sandwich")
    checker = ExcludedWordCheck.new("what was that")
    checker.check.should be_true
  end
end
