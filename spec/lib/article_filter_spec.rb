# -*- encoding: utf-8 -*-
require "spec_helper"
require "date"
describe ArticleFilter do
  before(:each) do
    @news_source1 = FactoryGirl.create(:news_source, name: "news", created_at: Date.today)
    @news_source2 = FactoryGirl.create(:news_source, name: "news", created_at: Date.today - 1)
    @news_source3 = FactoryGirl.create(:news_source, name: "news", created_at: Date.today - 1)
    @category1 = FactoryGirl.create(:category)
    @category2 = FactoryGirl.create(:category)
    @category3 = FactoryGirl.create(:category)
    @trend1 = FactoryGirl.create(:trend, name: "trend_1", active: true)
    @trend2 = FactoryGirl.create(:trend, name: "trend_2")
    @category4 = FactoryGirl.create(:category)
    @tag = FactoryGirl.create(:tag)
    @article19 = FactoryGirl.create(:article, published: true, article_group_id: 19, category_id: @category4.id, news_source_id: @news_source2.id, title: "19", text: "19", created_at: DateTime.now - 19, tags: [@tag])
    @article18 = FactoryGirl.create(:article, published: true, article_group_id: 18, category_id: @category4.id, news_source_id: @news_source2.id, title: "18", text: "18", created_at: DateTime.now - 18)
    @article17 = FactoryGirl.create(:article, published: true, article_group_id: 17, category_id: @category4.id, news_source_id: @news_source2.id, title: "17", text: "17", created_at: DateTime.now - 17)
    @article16 = FactoryGirl.create(:article, published: true, article_group_id: 16, category_id: @category4.id, news_source_id: @news_source2.id, title: "16", text: "16", created_at: DateTime.now - 16)
    @article15 = FactoryGirl.create(:article, published: true, article_group_id: 15, category_id: @category4.id, news_source_id: @news_source2.id, title: "15", text: "15", created_at: DateTime.now - 15)
    @article14 = FactoryGirl.create(:article, published: true, article_group_id: 14, category_id: @category4.id, news_source_id: @news_source2.id, title: "14", text: "14", created_at: DateTime.now - 14)
    @article13 = FactoryGirl.create(:article, published: true, article_group_id: 13, category_id: @category4.id, news_source_id: @news_source2.id, title: "13", text: "13", created_at: DateTime.now - 13)
    @article12 = FactoryGirl.create(:article, published: true, article_group_id: 12, category_id: @category1.id, news_source_id: @news_source2.id, title: "12", text: "12", created_at: DateTime.now - 12)
    @article11 = FactoryGirl.create(:article, published: true, article_group_id: 11, category_id: @category4.id, news_source_id: @news_source2.id, title: "11", text: "11", created_at: DateTime.now - 11)
    @article10 = FactoryGirl.create(:article, published: true, article_group_id: 10, category_id: @category4.id, news_source_id: @news_source2.id, title: "10", text: "10", created_at: DateTime.now - 10)
    @article9  = FactoryGirl.create(:article, published: true, article_group_id: 9,  category_id: @category4.id, news_source_id: @news_source2.id, title: "9 ", text: "9 ", created_at: DateTime.now - 9 )
    @article8  = FactoryGirl.create(:article, published: true, article_group_id: 8,  category_id: @category4.id, news_source_id: @news_source2.id, title: "8 ", text: "8 ", created_at: DateTime.now - 8 )
    @article7  = FactoryGirl.create(:article, published: true, article_group_id: 7,  category_id: @category4.id, news_source_id: @news_source2.id, title: "7 ", text: "7 ", created_at: DateTime.now - 7 )
    @article6  = FactoryGirl.create(:article, published: true, article_group_id: 6,  category_id: @category4.id, news_source_id: @news_source2.id, title: "6 ", text: "6 ", created_at: DateTime.now - 6 )
    @article5  = FactoryGirl.create(:article, published: true, article_group_id: 3,  category_id: @category4.id, news_source_id: @news_source2.id, title: "5",  text: "5",  created_at: DateTime.now - 4 )
    @article4  = FactoryGirl.create(:article, published: true, article_group_id: 3,  category_id: @category3.id, news_source_id: @news_source1.id, title: "4",  text: "4",  created_at: DateTime.now - 3 )
    @article3  = FactoryGirl.create(:article, published: true, article_group_id: 3,  category_id: @category1.id, news_source_id: @news_source3.id, title: "3",  text: "3",  created_at: DateTime.now - 2 , trend_id: @trend2.id)
    @article2  = FactoryGirl.create(:article, published: true, article_group_id: 1,  category_id: @category2.id, news_source_id: @news_source1.id, title: "2",  text: "2",  created_at: DateTime.now - 1 , trend_id: @trend1.id)
    @article1  = FactoryGirl.create(:article, published: true, article_group_id: 1,  category_id: @category1.id, news_source_id: @news_source2.id, title: "1",  text: "1",  created_at: DateTime.now     , trend_id: @trend1.id)
  end

  it "should filter articles and return roots if no params given" do
    ArticleFilter.filter().should == {articles: [@article1, @article3, @article6, @article7, @article8, @article9, @article10, @article11, @article12, @article13, @article14, @article15, @article16, @article17, @article18], page: 1, per_page: 15, cat_id: nil, source_id: [], total_count: 16, trend_id: nil, tag_id: nil}
  end

  it "should filter articles with given limit" do
    ArticleFilter.filter(page: 2, per_page: 3).should == {articles: [@article7, @article8, @article9], page: 2, per_page: 3, cat_id: nil, source_id: [], total_count: 16, trend_id: nil, tag_id: nil}
  end

  it "should filter articles within given cateogry" do
    ArticleFilter.filter(page: 1, cat_id: @category1.id).should == {articles: [@article1, @article3, @article12], page: 1, per_page: 15, cat_id: @category1.id, source_id: [], total_count: 3, trend_id: nil, tag_id: nil}
  end

  it "should filter articles within given news_source" do
    ArticleFilter.filter(page: 1, source_id: @news_source1.id).should == {articles: [@article2, @article4], page: 1, per_page: 15, cat_id: nil, source_id: [@news_source1.id], total_count: 2, trend_id: nil, tag_id: nil}
  end

  it "should filter articles from multiple sources" do
    ArticleFilter.filter(source_id: [@news_source1.id, @news_source3.id]).should == {articles: [@article2, @article3, @article4], page: 1, per_page: 15, cat_id: nil, source_id: [@news_source1.id, @news_source3.id], total_count: 3, trend_id: nil, tag_id: nil}
  end

  it "should filter articles within given news_source and category" do
    ArticleFilter.filter(page: 1, source_id: @news_source2.id, cat_id: @category4.id).should == {articles: [@article5, @article6, @article7, @article8, @article9, @article10, @article11, @article13, @article14, @article15, @article16, @article17, @article18, @article19], page: 1, per_page: 15, cat_id: @category4.id, source_id: [@news_source2.id], total_count: 14, trend_id: nil, tag_id: nil}
  end

  it "should not confuse if there are no articles matching the filter and act like index without params" do
    ArticleFilter.filter(cat_id: -1).should == {articles: [@article1, @article3, @article6, @article7, @article8, @article9, @article10, @article11, @article12, @article13, @article14, @article15, @article16, @article17, @article18], page: 1, per_page: 15, cat_id: nil, source_id: [], total_count: 16, trend_id: nil, tag_id: nil}
  end

  it "should do full text search if there is query in params" do
    ArticleFilter.filter(query: "1").should ==  {articles: [@article1], page: 1, per_page: 15, cat_id: nil, source_id: [], total_count: 1, trend_id: nil, tag_id: nil}
  end

  it "should return articles from trend if trend param is given" do
    ArticleFilter.filter(trend_id: @trend1.id).should ==  {articles: [@article1], page: 1, per_page: 15, cat_id: nil, source_id: [], total_count: 1, trend_id: @trend1.id, tag_id: nil}
  end

  it "should return default articles if there is no trend or trend is inactive" do
    ArticleFilter.filter(trend_id: -1).should ==  {articles: [@article1, @article3, @article6, @article7, @article8, @article9, @article10, @article11, @article12, @article13, @article14, @article15, @article16, @article17, @article18], page: 1, per_page: 15, cat_id: nil, source_id: [], total_count: 16, trend_id: nil, tag_id: nil}
  end

  it "should return articles even from inactive trends" do
    ArticleFilter.filter(trend_id: @trend2.id).should ==  {articles: [@article3], page: 1, per_page: 15, cat_id: nil, source_id: [], total_count: 1, trend_id: @trend2.id, tag_id: nil}
  end

  it "should order articles by rating trends" do
    article = FactoryGirl.create(:article, published: true,  rating: 1, trend: @trend2)
    ArticleFilter.filter(trend_id: @trend2.id).should ==  {articles: [article, @article3], page: 1, per_page: 15, cat_id: nil, source_id: [], total_count: 2, trend_id: @trend2.id, tag_id: nil}
  end

  it "should order articles just by date when order parameter is specified" do
    article = FactoryGirl.create(:article, published: true,  rating: 1, trend: @trend2, created_at: @article3.created_at - 10000)
    ArticleFilter.filter(order: "created_at DESC", trend_id: @trend2.id).should ==  {articles: [@article3, article], page: 1, per_page: 15, cat_id: nil, source_id: [], total_count: 2, trend_id: @trend2.id, tag_id: nil}
  end

  it "should filter articles by given tag" do
    ArticleFilter.filter(tag_id: @tag.id).should == {articles: [@article19], page: 1, per_page: 15, cat_id: nil, source_id: [], total_count: 1, trend_id: nil, tag_id: @tag.id}
  end

end
