# # -*- encoding: utf-8 -*-
# require "active_support"
# require "page_processor"
# require "spec_helper"

# def parse(options)
#   url     = options[:url]
#   find    = options[:find]
#   ignored = options[:ignored]

#   feeds = get_feeds(url)

#   return [] if feeds.class == Fixnum or feeds.nil?

#   feeds.entries.each do |feed|
#     next if VisitedPage.find_by_url(feed.url)

#     url = feed.url
#     title = feed.title

#     page = Page.new(feed.url, find, ignored)
#     summary = feed.summary || page.summary

#     page_html = page.text
#     img = page.image

#     form_respond(page_html, "_"*80)
#   end
# end

# class Page
#   attr_reader :text
#   def initialize(url, find, ignored)
#     @find    = find
#     @ignored = ignored
#     @host    = URI(url).host
#     @text = process_page(get_page(url))
#     @soup = Nokogiri::HTML(@text)
#   end

#   def image
#     @soup.at_css('img').nil? ? '/assets/default_news.png' : @soup.at_css('img')["src"]
#   end

#   def summary
#     if text.length > 300
#       text[0...300] + '...'
#     else
#       text
#     end
#   end

#   private

#   def process_page(html)
#     PageProcessor.process(
#       html,
#       [ImageProcessor, IgnoredProcessor, AttributeProcessor, LinkProcessor],
#       {find: @find, ignored: @ignored, host: @host}
#     )
#   end

#   def get_page(url)
#     request = Curl::Easy.perform(url) do |curl|
#       curl.follow_location = true
#       curl.max_redirects = 3
#       curl.headers["User-Agent"] = 'Mozilla/5.0 (X11; Linux i686) AppleWebKit/535.7 (KHTML, like Gecko) Chrome/16.0.912.77 Safari/535.7'
#     end
#     request.body_str
#   end
# end


# def get_feeds(url)
#   Feedzirra::Feed.fetch_and_parse(url)
# end


# def form_respond(title, summary)
#   puts title
#   puts summary
#   puts
# end

