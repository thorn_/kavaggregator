# -*- encoding: utf-8 -*-
describe PageProcessor do
  before(:each) do
    @html = '
      <div class="newstext normal">
        <p>Национальный антитеррористический комитет (НАК) назвал имена всех шести боевиков, уничтоженных в ходе спецоперации со 2 по 4 апреля в Сергокалинском районе Дагестана, сообщает РИА Новости ria.ru.
          <br><br>
          По сообщению ведомства, данная бандгруппа, возглавляемая турецким наемником Муханнедом, известным также под кличкой Шейх Абдусалам, причастна к целому ряду преступлений террористической направленности, покушениям на жизнь и убийствам гражданских лиц, священнослужителей, военнослужащих и сотрудников полиции, а также вымогательствам.
          <br><br>
          <a href="#me">ME</a>
          <a name="me">ME</a>
          <a>EMPTY LINK</a>
          <a href="#" data-href="http://www.example.com">Link with data-href attribute</a>
          <a href="/news/1333">Relative Link</a>
          Помимо <a href="/news/258385/">опознанных</a> ранее активного члена бандгруппы Рашида Газалиева и пособника боевиков Магомеда Газалиева, среди боевиков находился Салман Будайчиев 1984 года рождения, уроженец села Бурдеки Сергокалинского района (кличка Хузейфа). Был "завербован" боевиками в июне 2010 года, когда он приехал из Челябинской области на свадьбу своей родственницы, после чего "ушел в лес" и примкнул к бандподполью.
          <br><br> Также <a href="/news/258367/">уничтожен</a> Руслан Капиев 1984 года рождения, уроженец села Сергокала Сергокалинского района (кличка Адам), активный участник бандгруппы с апреля 2011 года. По оперативным данным, он причастен к убийству 2 мая 2011 года начальника сергокалинского РОВД Насрулы Магомедова.
          <br><br> Среди боевиков был Ильяс Закарьяев 1984 года рождения, уроженец села Сергокала Сергокалинского района, кличка Абдулазиз. Активный участник бандгруппы, 4 мая 2011 года он принимал участие в обстреле полицейских-кинологов, в результате чего погиб один сотрудник. Вместе с Капиевым принимал участие в убийстве начальника сергокалинского РОВД.
          <br><br> <em>"От своих родственников &ndash; сотрудников полиции </em><em>&ndash;</em><em> под угрозой физической расправы требовл уволтьсяиз правоохранительных органов"</em>, &ndash; отмечает НАК. <br><br> Также уничтожен Юсуп Халимбеков 1995 года рождения. Под воздествиембандитской пропаганды пять дней назад ушел из дома в селе Бурдеки Сергокалинского района и "взял в руки оружие", информирует НАК.
          <br><br> Блокированы и уничтожены боевики были в лесу около села Кадиркент. Как сообщает НАК, на месте боестолкновений и при нейтралзованныхбандитах обнаружены три противотанковых гранатомета, три пулемета Калашникова, четыре автомата, охотничий карабин с прицелом, приспообленныйпод снайперскую винтовку, подствольный гранатомет, большое количество патронов, гранаты, компоненты самодельных взрывных устройств иметодические пособия по их изготовлению, обмундирование.
          <br><br> <em>"Контртеррористическая операция в Сергокалинском районе продолжается, ведутся дальнейшие активные оперативно-боевые мероприятияи следственные действия", </em>&ndash; отмечает НАК.
          <img src="/assets/image.png" id="img1"><img>
          <img src="/assets/new.png" alt="sandwich_" id="img2"><img>
        </p>
        <textarea>
          текст
        </textarea>
        <script type="text/javascript">alert("that is to add comments");</script>
        <div class="advertisement"> Some shitty adverts or comments and so on</div>
        <input type="text" id="s" name="s" value="">
      </div>
      <div class="hidden">That should not be on text
        <p class="cont2" style="holy_crap">
          New Content
        </p>
      </div>'
    # @processor = PageProcessor.new(".newstext.normal, .cont2", ".advertisement" , "www.example.com")
    result = PageProcessor.process(
      @html,
      [ImageProcessor, IgnoredProcessor, AttributeProcessor, LinkProcessor],
      {find: ".newstext.normal, .cont2", ignored: ".advertisement", host: "www.example.com"}
    )
    @text = Nokogiri::HTML(result)
  end
  describe Processor do
    before(:each) do
      @soup = Nokogiri::HTML(@html).css("*").first
    end

    it "should throw error when process is invoked" do
      @p = Processor.new(soup: @soup)
      expect{@p.process}.to raise_error(RuntimeError, "Abstract method was called")
    end

    describe ImageProcessor do
      it "should add host to relative images path" do
        ImageProcessor.new(soup: @soup, host: "www.example.com").process
        @soup.css('img').each{|img| img["src"].scan("www.example.com").should_not be_empty}
      end

      it "should insert random vale from injection array" do
        injections = [
          ". новости дагестана",
          ". новости дагестана сегодня",
          ". новости дагестана последние",
          ". новости дагестан #{Date.today.year}",
          ". новости дагестана свежие",
          ". риа новости дагестан сегодня",
          ". криминальные новости дагестана",
          ". новости дагестана криминал",
          ". дагестан буйнакск новости",
          ". дагестан новости махачкале",
          ". новости дагестана хасавюрт",
          ". новости республики дагестан",
          ". новости дня в дагестане"]
        ImageProcessor.new(soup: @soup, host: "www.example.com").process
        injections.should include(@soup.at_css('#img1')["alt"])
        injections.should include(@soup.at_css("#img2")["alt"].split("_")[1])
        @soup.at_css("#img2")["alt"].split("_")[0].should == "sandwich"
      end
    end

    describe IgnoredProcessor do
      it "should remove any ignored parts of page" do
        IgnoredProcessor.new(soup: @soup, ignored: ".advertisement").process
        @soup.css('.advertisement').count.should be_zero
        blacklist = ["script", "style", "link", "iframe", "textarea", "input"]
        blacklist.each { |item| @soup.css(item).count.should be_zero }
      end
    end

    describe AttributeProcessor do
      it "should remove unwanted attributes" do
        AttributeProcessor.new(soup: @soup, ignored_attributes: []).process
        blacklist_attributes = ["style", "width", "height", "class"]
        blacklist_attributes.each do |attr|
          @soup.css("[#{attr}]").count.should be_zero
        end
      end
    end

    describe LinkProcessor do
      before(:each) do
        LinkProcessor.new(soup: @soup, host: "www.example.com").process
      end

      it "should fix relative links" do
        @soup.css('a').each { |link| link["href"].scan("www.example.com").should_not be_empty unless link["name"] or link["href"][0] == "#" }
      end

      it "should delete unnecessary links" do
        @soup.css('a[target=_blank]').count.should == 3
        @soup.text.scan("EMPTY LINK").count.should == 0
      end

      it "should not delete anchor without href but with name attribute" do
        @soup.css('a[name=me]').count.should == 1
      end

      it "should fix link with data-href attribute" do
        @soup.css('a[data-href="http://www.example.com"]').first['href'].should == "http://www.example.com"
      end
    end
  end
  it "should concatenate parts of page" do
    @text.text.scan("New Content").should_not be_empty
    @text.text.scan("That should not be on text").should be_empty
  end

  it "should delete unnecessary links" do
    @text.css('a[target=_blank]').count.should == 3
    @text.text.scan("EMPTY LINK").count.should == 0
  end

  it "should fix relative links" do
    @text.css('a').each { |link| link["href"].scan("www.example.com").should_not be_empty unless link["name"] or link["href"][0] == "#" }
  end

  it "should not delete anchor without href but with name attribute" do
    @text.css('a[name=me]').count.should == 1
  end

  it "should left only 3 images" do
    @text.css('img').count.should == 2
  end

  it "should add host to relative images path" do
    @text.css('img').each{|img| img["src"].scan("www.example.com").should_not be_empty}
  end

  it "should add rel=nofollow to all images" do
    @text.css('img').each{|img| img['rel'].should == 'nofollow'}
  end

  # it "should return appropriate image when article has image" do
  #   @img.should_not be_nil
  #   @img.scan("www.example.com").should_not be_empty
  # end

  # it "should return standart image if there are no images in text" do
  #   @without_image = Nokogiri::HTML(@text.to_s)
  #   @without_image.css("img").each {|img| img.remove }
  #   PageProcessor.process(
  #     @without_image.to_s,
  #     [ImageProcessor, IgnoredProcessor, AttributeProcessor, LinkProcessor],
  #     {find: ".newstext.normal, .cont2", ignored: ".advertisement", host: "www.example.com"}
  #   )[1].should == "/assets/default_news.png"
  # end

  it "should delete all blacklist items" do
    blacklist = ["script", "style", "link", "iframe", "textarea", "input"]
    blacklist.each { |item| @text.css(item).count.should be_zero }
  end

  it "should remove any ignored parts of page" do
    @text.css('.advertisement').count.should be_zero
  end

  it "should remove any unwanted attributes" do
    blacklist_attributes = ["style", "width", "height", "class"]
    blacklist_attributes.each do |attr|
      @text.css("[#{attr}]").count.should be_zero
    end
  end
end
