require "spec_helper"

describe Api::V1::CategoriesController do
  render_views
  it "should get some articles using article filter" do
    get :index, format: :json
    response.should be_ok
  end

  before(:each) do
    @category1 = FactoryGirl.create(:category, name: "cat1")
    @category2 = FactoryGirl.create(:category, name: "cat2")
  end

  it "should return json response for all categories" do
    get :index, format: :json
    resp = JSON.parse(response.body)
    response.body.should include(@category1.name, @category2.name)
    resp.length.should == 2
  end
end
