require "spec_helper"

describe Api::V1::NewsSourcesController do
  render_views
  it "should get some articles using article filter" do
    get :index, format: :json
    response.should be_ok
  end

  before(:each) do
    @news_source1 = FactoryGirl.create(:news_source, name: "cat1")
    @news_source2 = FactoryGirl.create(:news_source, name: "cat2")
  end

  it "should return json response for all categories" do
    get :index, format: :json
    resp = JSON.parse(response.body)
    response.body.should include(@news_source1.name, @news_source2.name)
    resp.length.should == 2
  end
end
