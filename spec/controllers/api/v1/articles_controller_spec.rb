require "spec_helper"

describe Api::V1::ArticlesController do
  render_views
  it "should get some articles using article filter" do
    get :index, format: :json
    response.should be_ok
  end

  before(:each) do
    @category1 = FactoryGirl.create(:category, name: "cat1")
    @category2 = FactoryGirl.create(:category, name: "cat2")
    @news_source1 = FactoryGirl.create(:news_source, name: "news1")
    @news_source2 = FactoryGirl.create(:news_source, name: "news3")
    @article2  = FactoryGirl.create(:article, published: true, article_group_id: 1,  category_id: 2, news_source_id: @news_source1.id, title: "2", text: "2", created_at: DateTime.now - 1)
    @article1  = FactoryGirl.create(:article, published: true, article_group_id: 2,  category_id: 1, news_source_id: @news_source2.id, title: "1", text: "1", created_at: DateTime.now, title: "lorem")
  end

  describe "GET 'index'" do
    it "should return json response for last articles" do
      get :index, format: :json
      resp = JSON.parse(response.body)
      response.body.should include(@article1.title, @article2.title)
      resp.length.should == 2
    end

    it "should return as much articles as you want" do
      get :index, format: :json, per_page: 1
      resp = JSON.parse(response.body)
      resp.length.should == 1
    end
  end

  describe "POST 'share'" do
    let(:article) { FactoryGirl.create(:article)}
    it "should be ok" do
      post :share, id: article.id
      response.should be_ok
    end
    it "should mark article as shared" do
      expect do
        post :share, id: article.id
        article.reload
      end.to change{article.shared}.from(false).to(true)

    end
  end
end
