require "spec_helper"

describe Api::V1::TrendsController do
  render_views
  it "should get some articles using article filter" do
    get :index, format: :json
    response.should be_ok
  end

  before(:each) do
    @trend1 = FactoryGirl.create(:trend, name: "cat1")
    @trend2 = FactoryGirl.create(:trend, name: "cat2")
  end

  it "should return json response for all categories" do
    get :index, format: :json
    resp = JSON.parse(response.body)
    response.body.should include(@trend1.name, @trend2.name)
    resp.length.should == 2
  end
end
