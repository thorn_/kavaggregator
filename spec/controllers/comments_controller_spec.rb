require 'spec_helper'

describe CommentsController do
  before(:each) do
    @user = FactoryGirl.create(:user)
    @article = FactoryGirl.create(:article)
    @comment = FactoryGirl.create(:comment, article_id: @article.id)
    sign_in(@user)
  end

  describe "GET 'index'" do
    it "should be success" do
      get :index, article_id: @article.id, format: :json
      response.should be_ok
    end

    it "should render error without article parameter" do
      get :index, format: :json
      response.should_not be_ok
    end
  end

  describe "POST 'create'" do
    it "should create a freakin' comment" do
      lambda do
        post :create, comment: {content: "lorem ipsum", article_id: @article.id}
      end.should change(Comment, :count).by(1)
    end

    it "should not create a comment if there is no article with give id" do
      lambda do
        post :create, comment: {content: "lorem ipsum", user_id: @user.id, article_id: -1}
      end.should_not change(Comment, :count).by(1)
    end

    it "should redirect back to article if format is html" do
      post :create, comment: {content: "lorem ipsum", article_id: @article.id}
      response.should redirect_to(article_path(@article))
    end
  end

  describe "DELETE 'destroy'" do
    it "should destroy article without actually destroying them" do
      lambda do
        delete :destroy, id: @comment.id
      end.should_not change(Comment, :count)
    end

    it "should mark comment as deleted" do
      delete :destroy, id: @comment.id
      @comment.reload
      @comment.deleted.should == true
    end

    it "should not change deleted element if there is some mistakes" do
      lambda do
        delete :destroy, id: -1
      end.should_not change(Comment, :count)
    end

    it "should redirect back to article" do
      delete :destroy, id: @comment.id
      response.should redirect_to article_path(@article)
    end
  end

  describe "GET 'new'" do
    it "should be success" do
      get :new, article_id: @article.id
      response.should be_ok
    end
  end
end
