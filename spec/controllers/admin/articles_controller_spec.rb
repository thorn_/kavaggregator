# -*- encoding: utf-8 -*-
require 'spec_helper'

describe Admin::ArticlesController do

  before(:each) do
    @cat = FactoryGirl.create(:category)
    @source = FactoryGirl.create(:news_source)
    @article = FactoryGirl.create(:article, news_source_id: @source.id, published: true)

    @auth = {"provider" => "identity", "uid" => "1", "info" => { name: "test_user_1", image: "image.jpg"}}
    @user = FactoryGirl.create(:user)
    @user.role = "administrator"
    @user.save
    sign_in(@user)
  end
  let(:attr){ {
                text: "article_text",
                category_id: @cat.id,
                news_source_id: @source.id,
                title: "article title",
                published: true,
                summary: "article summary"
              }
            }
  describe "GET 'index'" do

    it "should be success" do
      get :index
      response.should be_success
    end

    it "should get only not classified articles" do
      get :index
      assigns(:articles).should == Article.not_published.order("created_at DESC")
    end
  end

  describe "POST train" do

    describe "success" do
      it "should sort article aritlce" do
        post :train, id: @article, cat_id: @cat.id
        response.should redirect_to(admin_articles_path)
      end

      it "should include category" do
        post :train, id: @article, cat_id: @cat.id
        Article.find(@article.id).category.should == @cat
      end

      it "should have a success flash" do
        post :train, id: @article, cat_id: @cat.id
        flash[:success].should =~ /Статья успешно классифицирована./i
      end
    end

    describe "failure" do
      it "should redirect to articles path" do
        post :train, id: @article, cat_id: -1
        response.should redirect_to(admin_articles_path)
      end

      it "should have a error flash" do
        post :train, id: @article, cat_id: -1
        flash[:error].should =~ /Статья не классифицирована./i
      end
    end
  end

  describe "delete 'destroy'" do
    before(:each) do
      request.env["HTTP_REFERER"] = admin_articles_path
    end
    it "should delete single article" do
      lambda do
        delete :destroy, id: @article.id
        response.should redirect_to(admin_articles_path)
      end.should change(Article, :count).by(-1)
    end

    it "should delete all unpublished articles" do
      FactoryGirl.create(:article, published: true)
      FactoryGirl.create(:article, published: false)
      lambda do
        delete :destroy, id: 'all'
        Article.not_published.should be_empty
      end.should change(Article, :count).by(-1)
      response.should redirect_to(admin_articles_path)
    end
  end

  describe "get 'feed_me'" do
    it "should call feed me method and redirect_to article path" do
      Article.should_receive :feed_me
      get :feed_me
      response.should redirect_to admin_articles_path
    end
  end

  describe "GET 'classify'" do
    it "should classify some articles" do
      art = FactoryGirl.create(:article)
      get :classify, id: art.id
      res = FisherClassifier.new.classify(Nokogiri::HTML(@article.text).text)
      assigns(:cat).should == res[0]
      assigns(:prob).should == res[1]
    end
  end

  describe "GET 'new'" do
    it "should be ok" do
      get :new
      response.should be_ok
    end
  end

  describe "GET 'edit'" do
    it "should have good response" do
      get :edit, id: @article.id
      response.should be_ok
    end

    it "should fetch appropriate article" do
      get :edit, id: @article.id
      assigns(:article).should == @article
    end
  end

  describe "POST 'create'" do
    it "should create aritlce if all attributes are valid" do
      expect{post :create, article: attr}.to change{Article.count}.by(1)
    end

    it "should redirect to articles path" do
      post :create, article: attr
      response.should redirect_to(article_path(assigns(:article)))
    end

    it "should also assign article group to new article" do
      post :create, article: attr
      assigns(:article).article_group_id.should_not be_blank
    end

    it "should not create article if some attributes are wrong" do
      expect{post :create, article: attr.merge(title: nil)}.not_to change{Article.count}
    end

    it "should redirect to articles path" do
      post :create, article: attr.merge(title: nil)
      response.should render_template(:new)
    end
  end

  describe "PUT 'update'" do
    let(:article){ FactoryGirl.create(:article, title: "title")}
    it "should update articles if all attributes are valid" do
      put :update, id: article.id, article: attr.merge(title: "Brand new title")
      article.reload
      article.title.should == "Brand new title"
    end

    it "should redirect to article edit path" do
      put :update, id: article.id, article: attr.merge(title: "Brand new title")
      response.should redirect_to(article_path(article))
    end

    it "should deny invalid changes" do
      put :update, id: article.id, article: attr.merge(title: nil)
      response.should render_template(:edit)
    end
  end
end
