require 'spec_helper'

describe Admin::TrendsController do

  describe "GET 'new'" do
    it "returns http success" do
      get 'new'
      response.should be_success
    end
  end

  describe "POST 'create'" do
    before(:each) do
      @attr = {name: "trend1", description: "Trend description"}
    end

    describe "success" do
      it "should create an instance of trend" do
        lambda do
          post :create, trend: @attr
          response.should redirect_to(admin_trends_path)
        end.should change(Trend, :count).by(1)
      end
    end

    describe "failure" do
      it "should not create an instance of trend" do
        lambda do
          post :create, trend: @attr.merge(name: "")
          response.should render_template('trends/new')
        end.should_not change(Trend, :count)
      end
    end
  end

  describe "PUT 'update'" do
    it "should update existing record" do
      @trend = FactoryGirl.create(:trend)
      put :update, id: @trend.id, trend: {name: "tr1", description: "description"}
      @trend.reload
      @trend.name.should == "tr1"
    end

    it "should not change name with blank value" do
      @trend = FactoryGirl.create(:trend, name: "TREND1")
      put :update, id: @trend.id, trend: {name: "", description: "description"}
      response.should render_template(:edit)
      @trend.reload
      @trend.name.should == "TREND1"
    end
  end

  describe "DELETE 'destroy'" do
    it "should delete trend" do
      @trend = FactoryGirl.create(:trend)
      lambda do
        delete :destroy, id: @trend.id
        response.should redirect_to(admin_trends_path)
      end.should change(Trend, :count).by(-1)
    end
  end

  describe "GET 'index'" do
    it "should be success" do
      get :index
      response.should be_ok
    end

    it "should fetch trends if special order" do
      get :index
      assigns(:trends).should == Trend.order("active DESC, created_at DESC")
    end
  end
end
