# -*- encoding: utf-8 -*-
require 'spec_helper'

describe Admin::NewsSourcesController do

  describe "GET 'index'" do
    it "should be ok" do
      get :index
      response.should be_ok
      assigns(:news_sources).should == NewsSource.all
    end
  end

  describe "GET 'new'" do
    it "should be ok" do
      get :new
      response.should be_ok
      assigns(:news_source) == NewsSource.new
    end
  end

  describe "GET 'clear'" do
    it "should call the clear method on founded news source" do
      @news_source = FactoryGirl.create(:news_source)
    end
  end

  describe "POST 'create'" do

    describe "success" do
      before(:each) do
        @attr = { name: "Коммерсант",
                  url: "http://www.feeds.kommersant.ru/RSS_Export/RU/daily.xml",
                  period: "15" ,
                  find_helper: '<div id="_left">'}
      end

      it "should create a new source of news" do
        lambda do
          post :create, news_source: @attr
        end.should change(NewsSource, :count).by(1)
      end

      it "should redirect to index" do
        lambda do
          post :create, news_source: @attr
        end.should change(NewsSource, :count)
        response.should redirect_to(admin_news_sources_path)
      end

      it "should have a success flash message" do
        post :create, news_source: @attr
        flash[:success].should =~ /источник новостей успешно создан/i
      end
    end

    describe "failure" do
      before(:each) do
        @attr = {name: "", url: "", period: ""}
      end

      it "should render 'new' action" do
        post :create, news_source: @attr
        response.should render_template('news_sources/new')
      end
      it "should not create a new source" do
        lambda do
          post :create, news_source: @attr
        end.should_not change(NewsSource, :count)
      end

      it "should have a failure flash" do
        post :create, news_source: @attr
        flash[:error].should =~ /источник новостей не добавлен/i
      end
    end
  end

  describe "PUT 'update'" do

    describe "success" do
      before(:each) do
        @attr = { name: "Коммерсант",
                  url: "http://www.feeds.kommersant.ru/RSS_Export/RU/daily.xml",
                  period: "15" ,
                  find_helper: '#_left'}
        @source = NewsSource.create!(@attr)
      end

      it "should update source of news" do
        put :update, id: @source.id, news_source: @attr.merge(name: "Kommersant")
        @source.reload
        @source.name.should == "Kommersant"
      end

      it "should redirect to index" do
        put :update, id: @source.id, news_source: @attr.merge(name: "Kommersant")
        response.should redirect_to admin_news_sources_path
      end

      it "should have a success flash message" do
        put :update, id: @source.id, news_source: @attr
        flash[:success].should =~ /источник новостей успешно изменен/i
      end
    end

    describe "failure" do
      before(:each) do
        @attr = {name: "", url: "", period: ""}
        @source = NewsSource.create!(name: "Коммерсант",
                  url: "http://www.feeds.kommersant.ru/RSS_Export/RU/daily.xml",
                  period: "15" ,
                  find_helper: '#_left')
      end

      it "should render 'edit' action" do
        put :update, id: @source.id, news_source: @attr
        response.should render_template('news_sources/edit')
      end

      it "should not update news source" do
          put :update, id: @source.id, news_source: @attr
          @source.reload
          @source.name.should == "Коммерсант"
      end

      it "should have a failure flash" do
        put :update, id: @source.id, news_source: @attr
        flash[:error].should =~ /Источник новостей не изменен/i
      end
    end
  end

  describe "DELETE 'destroy'" do
    it "should destroy news source" do
      @attr = { name: "Коммерсант",
                url: "http://www.feeds.kommersant.ru/RSS_Export/RU/daily.xml",
                period: "15" ,
                find_helper: '<div id="_left">'}
      @ns = NewsSource.create!(@attr)

      lambda do
        delete :destroy, id: @ns
        response.should redirect_to(admin_news_sources_path)
      end.should change(NewsSource, :count).by(-1)
    end
  end

  describe "GET 'clear'" do

    before(:each) do
      article = FactoryGirl.create(:article)
      @ns = article.news_source
      get :clear, id: @ns.id
    end

    it "should redirect to news sources index path" do
      response.should redirect_to admin_news_sources_path
    end

    it "should destroy article" do
      @ns.articles.should be_empty
    end
  end

end
