require 'spec_helper'

describe Admin::TagsController do
  before(:each) do
    @attr = {name: "tag1"}
  end

  describe "GET 'new'" do
    it "returns http success" do
      get 'new'
      response.should be_success
    end
  end

  describe "POST 'create'" do
    describe "success" do
      it "should create an instance of tag" do
        lambda do
          post :create, tag: @attr
          response.should redirect_to(admin_tags_path)
        end.should change(Tag, :count).by(1)
      end
    end

    describe "failure" do
      it "should not create an instance of tag" do
        lambda do
          post :create, tag: @attr.merge(name: "")
          response.should render_template('tags/new')
        end.should_not change(Tag, :count)
      end
    end
  end

  describe "PUT 'update'" do
    it "should update existing record" do
      @tag = Tag.create!({name: "tag1"})
      put :update, id: @tag.id, tag: {name: "tag2"}
      @tag.reload
      @tag.name.should == "tag2"
    end

    it "should not change name with blank value" do
      @tag = Tag.create! @attr
      put :update, id: @tag.id, tag: {name: ""}
      response.should render_template(:edit)
      @tag.reload
      @tag.name.should == "tag1"
    end
  end

  describe "DELETE 'destroy'" do
    it "should delete tag" do
      @tag = Tag.create! @attr
      lambda do
        delete :destroy, id: @tag.id
        response.should redirect_to(admin_tags_path)
      end.should change(Tag, :count).by(-1)
    end
  end

  describe "GET 'index'" do
    it "should be success" do
      get :index
      response.should be_ok
    end
  end
end
