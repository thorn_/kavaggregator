require 'spec_helper'

describe Admin::CategoriesController do

  describe "GET 'new'" do
    it "returns http success" do
      get 'new'
      response.should be_success
    end
  end

  describe "POST 'create'" do
    before(:each) do
      @attr = {name: "cat1"}
    end

    describe "success" do
      it "should create an instance of category" do
        lambda do
          post :create, category: @attr
          response.should redirect_to(admin_categories_path)
        end.should change(Category, :count).by(1)
      end
    end

    describe "failure" do
      it "should not create an instance of category" do
        lambda do
          post :create, category: @attr.merge(name: "")
          response.should render_template('categories/new')
        end.should_not change(Category, :count)
      end
    end
  end

  describe "PUT 'update'" do
    it "should update existing record" do
      @cat = Category.create!({name: "CATEGORY1"})
      put :update, id: @cat.id, category: {name: "cat1"}
      @cat.reload
      @cat.name.should == "cat1"
    end

    it "should not change name with blank value" do
      @cat = Category.create!({name: "CATEGORY1"})
      put :update, id: @cat.id, category: {name: ""}
      response.should render_template(:edit)
      @cat.reload
      @cat.name.should == "CATEGORY1"
    end
  end

  describe "DELETE 'destroy'" do
    it "should delete category" do
      @cat = Category.create!({name: "CATEGORY1"})
      lambda do
        delete :destroy, id: @cat.id
        response.should redirect_to(admin_categories_path)
      end.should change(Category, :count).by(-1)
    end
  end

  describe "GET 'index'" do
    it "should be success" do
      get :index
      response.should be_ok
    end
  end
end
