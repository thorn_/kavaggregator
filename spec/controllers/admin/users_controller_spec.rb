# -*- encoding: utf-8 -*-
require 'spec_helper'

describe Admin::UsersController do

  before(:each) do
    @user = FactoryGirl.create(:user)
  end

  describe "GET 'index'" do
    it "returns http success" do
      get 'index'
      response.should be_success
    end
  end

  describe "DELETE 'destroy'" do
    it "should destroy given user" do
      lambda do
        delete :destroy, id: @user.id
        response.should redirect_to(admin_users_path)
      end.should change(User, :count).by(-1)
    end

    it "should have a flash message" do
      delete :destroy, id: @user.id
      flash[:success].should =~ /успешно удален/i
    end
  end

  describe "PUT 'update'" do
    before(:each) do
      put :update, id: @user.id, user: {role: "administrator"}
    end

    it "should update users credentials" do
      @user.reload
      @user.role.should == "administrator"
    end

    it "should redirecto to users path" do
      response.should redirect_to(admin_users_path)
    end

    it "should have appropriate flash message" do
      flash[:success].should =~ /успешно/i
    end
  end
end
