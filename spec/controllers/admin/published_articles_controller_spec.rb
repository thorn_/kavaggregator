#-*- encoding: utf-8 -*-
require "spec_helper"

describe Admin::PublishedArticlesController do
  describe "GET 'index'" do
    before(:each) do
      @ns1 = FactoryGirl.create(:news_source, name: "test_ns_1")
      @ns2 = FactoryGirl.create(:news_source, name: "test_ns_2")
      @cat1 = FactoryGirl.create(:category, name: "test_cat1")
      @cat2 = FactoryGirl.create(:category, name: "test_cat2")
      @a1 = FactoryGirl.create(:article, published: true, category: @cat1, news_source: @ns2, created_at: Date.today)
      @a2 = FactoryGirl.create(:article, published: true, category: @cat2, news_source: @ns1, created_at: Date.today - 1)
      @trend1 = FactoryGirl.create(:trend, active: true)
      @trend2 = FactoryGirl.create(:trend, active: false)
    end
    it "should get some articles" do
      get :index
      assigns(:articles).should == [@a1, @a2]
    end

    it "should get all trends instead of active" do
      get :index
      assigns(:trends).should == Trend.order("active DESC, created_at DESC")
    end
  end

  describe "POST 'group'" do
    before(:each) do
      2.times {FactoryGirl.create(:article, published: true)}
    end

    it "should change group of given articles" do
      @a = Article.first
      request.env["HTTP_REFERER"] = admin_published_articles_path
      lambda do
        post :group, article_group: {model_ids: Article.scoped.map(&:id).join(',')}
        @a.reload
        response.should redirect_to(admin_published_articles_path)
      end.should change(@a, :article_group_id)
    end
  end

  describe "POST 'trend'" do
    it "should assign trend to article" do
      article = FactoryGirl.create(:article, published: true)
      @trend = FactoryGirl.create(:trend, active: true)
      post :trend, id: article.id, trend_id: @trend.id
      response.should be_ok
      article.reload.trend_id.should == @trend.id
    end
  end

  describe "GET 'ungroup'" do
    it "should ungroup grouped articles" do
      group = FactoryGirl.create(:article_group)
      article1 = FactoryGirl.create(:article, article_group_id: group.id)
      article2 = FactoryGirl.create(:article, article_group_id: group.id)
      get :ungroup, id: article1.id
      response.should redirect_to(admin_published_articles_path)
      article1.reload
      article1.article_group.articles.count.should == 1
      article2.article_group.articles.count.should == 1

    end
  end

end
