#-*- encoding: utf-8 -*-
require 'spec_helper'

describe Admin::CommentsController do

  before(:each) do
    @article = FactoryGirl.create(:article, title: "lorem ipsum")
    @comment1 = FactoryGirl.create(:comment, content: "hello world", article_id: @article.id)
    @comment2 = FactoryGirl.create(:comment, content: "hello world", article_id: @article.id)
  end

  describe "GET 'index'" do
    it "should get freaking comments from database" do
      get :index
      assigns(:comments).should == Comment.all
    end
  end

  describe "DELETE 'destroy'" do
    it "should destroy selected comment" do
      delete :destroy, id: @comment1.id
      response.should redirect_to admin_comments_path
    end

    it "should not really delete comment" do
      lambda do
        delete :destroy, id: @comment1.id
      end.should_not change(Comment, :count)
      @comment1.reload
      @comment1.should be_deleted
    end

    it "should render empty comment if ther is error" do
      delete :destroy, id: -1, format: :json
      response.body.should == "{}"
    end

    it "should redirect if there is no comment" do
      delete :destroy, id: -1
      response.should redirect_to(admin_comments_path)
    end
  end
end
