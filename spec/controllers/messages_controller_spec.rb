require 'spec_helper'

describe MessagesController do
  before(:each) do
    @message1 = FactoryGirl.create(:message, :created_at => Date.today)
    @message2 = FactoryGirl.create(:message, :created_at => Date.today-1)
    @message3 = FactoryGirl.create(:message, :created_at => Date.today-2)
    get :index
  end

  it "should respond ok" do
    response.should be_ok
  end

  it "shold respond messages" do
    assigns(:messages).should == [@message1, @message2, @message3]
  end

  describe "application controller testing" do
    before(:each) do
      11.times {|i| FactoryGirl.create(:trend, active: true, name: "Trend_#{i}")}
      @inactive_trend = FactoryGirl.create(:trend)
    end
    it "should get 8 active trends" do
      get :index
      assigns(:trends).count.should == 8
      assigns(:trends).should_not include(@inactive_trend)
    end

    it "should include categories and news sources" do
      FactoryGirl.create(:category)
      FactoryGirl.create(:news_source)
      assigns(:news_sources).should == NewsSource.active.order(:created_at)
      assigns(:categories).should == Category.order(:created_at)
      assigns(:categories).should == Category.order(:created_at)
    end
  end

end
