# -*- encoding: utf-8 -*-
require "spec_helper"
require "date"

describe ArticlesController do
render_views

  before(:each) do
    @news_source1 = FactoryGirl.create(:news_source, name: "news")
    @news_source2 = FactoryGirl.create(:news_source, name: "news")
    @news_source3 = FactoryGirl.create(:news_source, name: "news")
    @article19 = FactoryGirl.create(:article, published: true, article_group_id: 19, category_id: 4, news_source_id: @news_source2.id, title: "19", text: "19", created_at: DateTime.now - 19)
    @article18 = FactoryGirl.create(:article, published: true, article_group_id: 18, category_id: 4, news_source_id: @news_source2.id, title: "18", text: "18", created_at: DateTime.now - 18)
    @article17 = FactoryGirl.create(:article, published: true, article_group_id: 17, category_id: 4, news_source_id: @news_source2.id, title: "17", text: "17", created_at: DateTime.now - 17)
    @article16 = FactoryGirl.create(:article, published: true, article_group_id: 16, category_id: 4, news_source_id: @news_source2.id, title: "16", text: "16", created_at: DateTime.now - 16)
    @article15 = FactoryGirl.create(:article, published: true, article_group_id: 15, category_id: 4, news_source_id: @news_source2.id, title: "15", text: "15", created_at: DateTime.now - 15)
    @article14 = FactoryGirl.create(:article, published: true, article_group_id: 14, category_id: 4, news_source_id: @news_source2.id, title: "14", text: "14", created_at: DateTime.now - 14)
    @article13 = FactoryGirl.create(:article, published: true, article_group_id: 13, category_id: 4, news_source_id: @news_source2.id, title: "13", text: "13", created_at: DateTime.now - 13)
    @article12 = FactoryGirl.create(:article, published: true, article_group_id: 12, category_id: 1, news_source_id: @news_source2.id, title: "12", text: "12", created_at: DateTime.now - 12)
    @article11 = FactoryGirl.create(:article, published: true, article_group_id: 11, category_id: 4, news_source_id: @news_source2.id, title: "11", text: "11", created_at: DateTime.now - 11)
    @article10 = FactoryGirl.create(:article, published: true, article_group_id: 10, category_id: 4, news_source_id: @news_source2.id, title: "10", text: "10", created_at: DateTime.now - 10)
    @article9  = FactoryGirl.create(:article, published: true, article_group_id: 9,  category_id: 4, news_source_id: @news_source2.id, title: "9 ", text: "9 ", created_at: DateTime.now - 9 )
    @article8  = FactoryGirl.create(:article, published: true, article_group_id: 8,  category_id: 4, news_source_id: @news_source2.id, title: "8 ", text: "8 ", created_at: DateTime.now - 8 )
    @article7  = FactoryGirl.create(:article, published: true, article_group_id: 7,  category_id: 4, news_source_id: @news_source2.id, title: "7 ", text: "7 ", created_at: DateTime.now - 7 )
    @article6  = FactoryGirl.create(:article, published: true, article_group_id: 6,  category_id: 4, news_source_id: @news_source2.id, title: "6 ", text: "6 ", created_at: DateTime.now - 6 )
    @article5  = FactoryGirl.create(:article, published: true, article_group_id: 3,  category_id: 4, news_source_id: @news_source2.id, title: "5", text: "5", created_at: DateTime.now - 4)
    @article4  = FactoryGirl.create(:article, published: true, article_group_id: 3,  category_id: 3, news_source_id: @news_source1.id, title: "4", text: "4", created_at: DateTime.now - 3)
    @article3  = FactoryGirl.create(:article, published: true, article_group_id: 3,  category_id: 1, news_source_id: @news_source3.id, title: "3", text: "3", created_at: DateTime.now - 2)
    @article2  = FactoryGirl.create(:article, published: true, article_group_id: 1,  category_id: 2, news_source_id: @news_source1.id, title: "2", text: "2", created_at: DateTime.now - 1)
    @article1  = FactoryGirl.create(:article, published: true, article_group_id: 1,  category_id: 1, news_source_id: @news_source2.id, title: "1", text: "1", created_at: DateTime.now, title: "lorem")
  end

  describe "GET 'index'" do
    it "should filter articles and return roots if no params given" do
      get :index
      assigns(:articles).should == [@article1, @article3, @article6, @article7, @article8, @article9, @article10, @article11, @article12, @article13, @article14, @article15, @article16, @article17, @article18]
    end

    it "should perform a search if query parameter is given" do
      get :index, {query: "1"}
      assigns(:articles).should == [@article1]
    end

    it "should set meta if we getting trend" do
      trend = FactoryGirl.create(:trend, description: "Trend description")
      article = FactoryGirl.create(:article, trend: trend)
      get :index, trend_id:trend.id
      assigns(:meta).should == trend.description
    end

    it "should set meta if we getting trend" do
      tag = FactoryGirl.create(:tag, description: "Trend description")
      article = FactoryGirl.create(:article, tags: [tag])
      get :index, tag_id: tag.id
      assigns(:meta).should == tag.description
    end

    it "should get last message" do
      @message  = FactoryGirl.create(:message, created_at: Date.today)
      @message1 = FactoryGirl.create(:message, created_at: 1.day.ago)
      get :index
      assigns(:message).should == @message
    end

    it "should add sticky article to the top" do
      sticky = FactoryGirl.create(:article, published: true, sticky: true)
      get :index
      assigns(:articles).first.should == sticky
    end
  end

  describe "GET 'show'" do
    it "should get not only article, but also similar articles" do
      group = FactoryGirl.create(:article_group)
      article1 = FactoryGirl.create(:article, article_group_id: group.id)
      article2 = FactoryGirl.create(:article, article_group_id: group.id, created_at: Date.today)
      article3 = FactoryGirl.create(:article, article_group_id: group.id, created_at: Date.today - 1)
      get :show, id: article1.id
      assigns(:similar_articles).should include(article2, article3)
    end
  end

end
