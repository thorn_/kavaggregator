#= require jquery
#= require underscore
#= require action_button
#= require_tree .

describe "Grouping and deleting button plugin", ->

  beforeEach ->
    loadFixtures('publihsed_articles')

  it 'should do nothing when there is no checked', ->
    form = $('#new_article_group')
    $('.action_button').action_button('#new_article_group')
    submitCallback = jasmine.createSpy().andReturn(false)
    form.submit(submitCallback)
    $('#group').click()
    expect($('input[id*=model_ids]:hidden').attr('value')).toEqual('');
    expect(submitCallback).not.toHaveBeenCalled();

  it 'submits form when button is clicked', ->
    form = $('#new_article_group')
    $("input[id=model_ids_group_]").attr('checked', true)
    $('.action_button').action_button('#new_article_group')
    submitCallback = jasmine.createSpy().andReturn(false)
    form.submit(submitCallback)

    $('#group').click()
    expect($('input[id*=model_ids]:hidden').attr('value')).toEqual('76,75');
    expect(submitCallback).toHaveBeenCalled();
