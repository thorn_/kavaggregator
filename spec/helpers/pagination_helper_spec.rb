require "ostruct"
require 'spec_helper'
describe PaginationHelper do
  it "should return window array" do
    windowed_page_numbers(1000, 87, 3).should ==
[1, 2, "...", 83, 84, 85, 86, 87, 88, 89, 90, 91, "...", 333, 334]
  end
end
