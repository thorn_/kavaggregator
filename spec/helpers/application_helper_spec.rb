# -*- encoding: utf-8 -*-
require 'spec_helper'

describe ApplicationHelper do


  it "should return appropriate meta description when @meta is not specified" do
    meta_tags.should =~ /Кавигатор - все новости Дагестана в одном месте. Последние события, происшествия и аналитика./
  end

  it "should return appropriate meta description when @meta is not specified" do
    @meta = "lorem ipsum"
    meta_tags.should =~ /lorem ipsum/
  end

  describe "image_url_for" do
    it "should return default image url for article if it hasn't it's own" do
      article = FactoryGirl.create(:article, img_url: Article::DEFAULT_IMAGE)
      image_url_for(article).should == "http://test.host/#{Article::DEFAULT_IMAGE}"
    end

    it "should return defaultimage if article's image is null" do
      article = FactoryGirl.create(:article, img_url: nil)
      image_url_for(article).should == "http://test.host/#{Article::DEFAULT_IMAGE}"
    end

    it "should return default image url for article if it hasn't it's own" do
      article = FactoryGirl.create(:article, img_url: "some_junk_url")
      image_url_for(article).should == "some_junk_url"
    end

  end

  describe "title maker" do
    before(:each) do
      @category = FactoryGirl.create(:category, name: "Category1 Name")
      @source1   = FactoryGirl.create(:news_source, name: "Source1 Name")
      @source2   = FactoryGirl.create(:news_source, name: "Source2 Name")
      @trend    = FactoryGirl.create(:trend, name: "Trend1 Name")
      @article = nil
      @default_title = "Новости Дагестана | Кавигатор"
    end

    it "should return default title if filter is empty" do
      @filter = { cat_id: nil, source_id: nil, trend_id: nil }
      show_title.should == @default_title
    end

    it "should return title + default title if there is variable" do
      @title = "New title"
      show_title.should == "#{@title} | #{@default_title}"
    end

    it "should include article title and article category in page title" do
      @filter = nil
      @article = FactoryGirl.create(:article, title: "Article1 Title")
      show_title.should == "#{@article.title} | #{@article.category_name} | #{@default_title}"
    end

    it "should include category name if category filter enabled" do
      @filter = { cat_id: @category.id, source_id: nil, trend_id: nil }
      show_title.should == "#{@category.name} в Дагестане | #{@default_title}"
    end

    it "should include all news sources if there are some" do
      @filter = { cat_id: nil, source_id: [@source1.id, @source2.id], trend_id: nil }
      show_title.should == "#{@source1.name} | #{@source2.name} | #{@default_title}"
    end

    it "should include both news sources and categories" do
      @filter = { cat_id: @category.id, source_id: [@source1.id, @source2.id], trend_id: nil }
      show_title.should == "#{@source1.name} | #{@source2.name} | #{@category.name} | #{@default_title}"
    end

    it "should include only trend in title even if there are articles and sources" do
      @filter = { cat_id: @category.id, source_id: [@source1.id, @source2.id], trend_id: @trend.id }
      show_title.should == "#{@trend.name} | #{@default_title}"
    end
  end

  describe "additional javascript" do
    it "should return false in test and development groups" do
      Rails.env = 'test'
      additional_javascript_permitted.should be_false
      Rails.env = 'development'
      additional_javascript_permitted.should be_false
    end
  end

end
