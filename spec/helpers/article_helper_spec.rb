# -*- encoding: utf-8 -*-
require "spec_helper"

describe ArticleHelper do
  it "should return mnogabkaff sign in large articles" do
    article = FactoryGirl.create(:article, text:"
        Т. Гасанова
        Это история о верности слову, что сильнее времени. Это история о материнской любви и кровном родстве, стирающих расстояния. Это история о памяти, которая не меркнет. С первых дней замужества Таибат поняла, что в ее новой семье есть какая-то давняя печаль. Чуткое юное сердце живо откликалось на глубокие вздохи, порой вырывавшиеся у мужа Абумуслима, на отрешенный взгляд свекрови Атикат. Она замечала, как подолгу застывал глава семьи Гаджибутта перед портретом на стене. С фотографии смотрел светло и открыто совсем еще молодой парень. Таибат знала, что это брат ее мужа, Гаджи. Знала и то, что он не вернулся с войны. Но было, было, она чувствовала, что-то еще, невысказанное, затаенное.
        Однажды, когда Атикат, сидя за вязанием, всплакнула без видимой причины, Таибат решилась спросить, чем печалится ее сердце. Свекровь встала, достала из шкафа коробку, где хранились семейные реликвии, и протянула невестке небольшой, истончившийся от времени листок: «... лейтенант Гаджи Гаджиев, уроженец села Хури Лакского района ДАССР, пропал без вести 30 апреля 1942 года». Таибат пробежала лаконичный текст еще раз. Пропал без вести. Ее словно ударило по глазам. Внезапная вспышка высветила причину непреходящей грусти в глазах Атикат, которую она с первых дней звала мамой. Женщины - старая и молодая - обнялись и заплакали вместе...
        Гаджи был бравый курсант Буйнакского пехотного училища. В удостоверении подпись помощника начальника строевого отдела старшего лейтенанта Луночкина и дата - 13 мая 1940 года. Если б не было войны, паренек из Хури мог бы сделать воинскую карьеру. У него были для этого все необходимые качества: упорство, целеустремленность, хорошая физическая подготовка. А главное - дух. Это был храбрый юноша, настоящий горец.
        Мать знала, что он на фронте с первых дней. Черная тарелка репродуктора – одна на все село - была единственным средством узнать о событиях на передовой. Даже скудного знания русского языка хватало, чтобы понять: дела на фронте плохи. Тревожный голос Левитана, от которого по коже пробегали мурашки, перечислял названия оставленных населенных пунктов. Все мрачнело и мрачнело лицо мужа, которому горская сдержанность не позволяла делиться с женой своими опасениями. Последнюю весточку получили из-под Курска: «Жив, здоров, воюю». И ни слова о том, что перебросили их часть в самую мясорубку. Битва на Курской дуге вошла в историю Великой Отечественной как рекордная по количеству погибших и раненых. Тогда в том кипящем котле как было считать их? Как идентифицировать кого-либо в месиве человеческих тел?
        И полетело в далекий Лакский район извещение: пропал без вести. И хуже похоронки, и лучше. Может, живой? Где ж его искать? А если нет в живых, где зарыт, какой земле предан? Мать потеряла покой навсегда. Она просыпалась с этой мыслью и засыпала с ней: «Где могилка сына?». А во сне видела его живого, тянулась, чтобы обнять. Он исчезал, растворялся, и Атикат просыпалась в слезах.
        Сын и дочь росли, не зная брата. Утешали маму, как могли. Она крепилась, чтобы не выдать сердечную муку, но на дне ее глаз, как темная вода, плескалась неизбывная грусть. Куда только не обращались Гаджиевы в своих попытках найти родную могилу. Все было тщетно. Ушел из жизни отец, так и не дождавшись. Завещал не прекращать поиски. Даже в затухающих его глазах брезжила надежда. Мать осталась с наказом мужа. Не переставая, рассылала письма в военкоматы, Советы ветеранов войны, архивы. Но дети стали замечать, что она все чаще говорит о Гаджи как о живом. Видно, чем дальше от войны, тем больше тешила она себя мыслью: а вдруг он жив? Может, был в плену? Может, был ранен, контужен, потерял память? О материнское сердце! Сколько в тебе любви и веры.
        Силы Атикат слабели. Она стала часто дремать за вязанием. Но одно, как заметили домочадцы, действовало на нее подобно бодрящему лекарству. Стоило зазвучать по телевизору позывным передачи «Служу Советскому Союзу», как старая женщина стряхивала с себя оцепенение и приковывалась глазами к экрану. «Бабушка, ты что смотришь так внимательно?», - не выдержали как-то внуки. На экране сменяли друг друга кадры хроники. Офицеры, солдаты в строю. «Может, нашего Гаджи среди них увижу», - глухо произнесла Атикат, а потом долго шепталась о чем-то с невесткой. О чем могут говорить матери? Они говорили о сыновьях. Бережно, как бусины из драгоценного ожерелья, перебирала Атикат воспоминания о Гаджи. Как он рос, мужал, как радовал их.
        Когда она слегла в 1974-м, не было дня, чтобы не вспоминала сына. И в ее отлегающее дыхание вплелись слова: «Найдите его, найдите...». Таибат попросила подключиться к поискам сестру. Криминалист с большим опытом, ветеран МВД, Асият Цахаева взялась за дело профессионально. Ее трудно было сбить с толку стандартными ответами. Настойчивости ей не занимать. Начала она поиск с Буйнакского военкомата. Там подняли все карточки. Потом пошла Асият Цахаева в Министерство социальной защиты. Будто сам Всевышний направлял ее шаги. Посоветовала, как надо действовать, руководитель рабочей группы Книги Памяти Валентина Васильевна Макарова.
        Последнее письмо ведь Гаджи прислал из-под Курска. А там в той ужасной сумятице вполне могли спутать что-нибудь. Может, это «двойное» отчество Гаджибутаевич причина путаницы, подсказала она. Следующие запросы сделала Асият Цахаевна уже с «сокращенным» отчеством: Бутаевич. Месяцы шли за месяцами, складывались в годы. Росла кипа писем, ответов, аккуратно собираемых Цахаевой. Лишь в конце 2005-го переписка принесла результат. Дрожащими от волнения руками держала она вынутую из конверта бумагу. Несколько сухих строк.
        (6092/6189) ФИО: Гаджиев Гаджи Бутаевич; МЕСТО РОЖДЕНИЯ: РЕСП.: Дагестанская АССР; ГЕОГР.: Лакский р-н, с.Хуры; МЕСТО ПРИЗЫВА: ВК: К; В/Ч: 1032 стр.полк, 293 стр. дивизия, 40 армия; ЗВАНИЕ: л-т; ДОЛЖН.: ком-р пуль.взвода; ВРЕМЯ ВЫБ.: 26.11.1941; ПРИЧ.ВЫБ.: погиб в бою; МЕСТО ГИБЕЛИ: МЕСТО ЗАХОРОН: ГЕОГР.: г. Тим, с.Выгорное; ОП.: 818883 с; Д. 476; Л.: 95.
        Таибат, несмотря на то, что сама уже немолода, поехала бы сразу, таково было ее нетерпение поклониться могиле ни разу не видевшего ее деверя. Но зима выдалась суровая, и поездку отложили на весну. Тем временем Асият Цахаева регулярно каждую пятницу созванивалась с председателем Тимского районного Совета ветеранов Виктором Черных. Он докладывал ей «обстановку». Районный Совет ветеранов обратился с ходатайством на имя военного комиссара Тимского района о внесении в книгу именного списка на военнослужащих и партизан Великой Отечественной войны, погибших в боях и умерших от ран, захороненных на территории Тимского района Курской области, Гаджи Бутаевича Гаджиева.
        Они сделали все, что от них зависело, чтобы увековечить память воина, погибшего, защищая Родину. Подробнейшим образом описал Черных в письме, как добраться до поселка Тим, села Выгорное. По телефону рассказывал, что в апреле установили плиту на братской могиле, где покоятся 250 воинов. Золотом высечены имена сорока человек, кто известен. Отдельно - как офицера - табличка с именем лейтенанта Гаджи Гаджиева.
        А в мае Таибат собралась в дорогу со своим старшим сыном Асланом. Ехали 600 км от Москвы на машине. Их встретил Виктор Черных. Теплая была встреча, трогательная. Он для Гаджиевых и Цахаевых как родной.
        Братская могила расположена возле школы. Она огорожена, ухожена руками школьников и учителей. Таибат поехала не с пустыми руками. Сладости раздала в школе, чтобы помянули Гаджи. Со слезами на глазах стояли мать и сын у могилы. Может, видела это с небес Атикат? Мысленно обратилась к ней Таибат: «Мама, вот мы и нашли нашего Гаджи». И запоздалые цветы легли на могилу, где покоятся рядом русский и лакец, татарин и казах, украинец и грузин. Они знали одну Родину. И та, что звалась Победа, не различала наций.
      ")
    large_article_sign(article).should have_content(raw("&nbsp"))
  end

  it "should not return sign in small articles" do
    article = FactoryGirl.create(:article)
    large_article_sign(article).should == raw("&nbsp;")
  end

  it "should show link to category if some is present" do
    article = FactoryGirl.create(:article)
    article_category_link(article).should have_css("a[href='#{articles_path(cat_id: article.category.id)}']")
  end

  it "should show empty span if there is no category in article" do
    article = FactoryGirl.create(:article, category_id: nil)
    article_category_link(article).should == raw("&nbsp;")
  end

  it "should return filtering link to source if some is present" do
    article = FactoryGirl.create(:article)
    article_source_link(article).should have_css("a[href='#{articles_path(source_id: article.news_source.id)}']")
  end
end
