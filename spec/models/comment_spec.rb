# -*- encoding: utf-8 -*-
require "spec_helper"

describe Comment do
  before(:each) do
    @user = FactoryGirl.create(:user)
    @article = FactoryGirl.create(:article)
    @attr = {user_id: @user.id, article_id: @article.id, content: "lorem ipsum"}
  end
  it "should create an instance with valid attributes" do
    Comment.new(@attr).should be_true
  end

  describe "validation" do
    it "should deny comment without article" do
      Comment.new(@attr.merge(article_id: nil)).should_not be_valid
    end

    it "should deny comments without user" do
      Comment.new(@attr.merge(user_id: nil)).should_not be_valid
    end

    it "should deny empty comments" do
      Comment.new(@attr.merge(content: '')).should_not be_valid
    end
  end

  describe "associations" do

    before(:each) do
      @comment = Comment.create!(@attr)
    end

    it "should have user association" do
      @comment.should respond_to(:user)
      @comment.user.should == @user
    end

    it "should have article association" do
      @comment.should respond_to(:article)
      @comment.article.should == @article
    end

  end

  describe "methods" do

    before(:each) do
      @user = FactoryGirl.create(:user, name: "Test User")
      @comment = Comment.create!(@attr.merge(user_id: @user.id))
    end
    it "get_content should return comment content if it is not deleted" do
      @comment.get_content.should == "lorem ipsum"
    end

    it "get_content should return appropriate message of deleted comment" do
      @comment.deleted = true
      @comment.save
      @comment.get_content.should == "Комментарий был удален"
    end

    it "user_name should return info about user that made a comment" do
      @comment.user_name.should == "Test User"
    end

    context "managable_by" do
      it "should return true if user role is administrator" do
        user = FactoryGirl.create(:user, name: "Admin", role: "administrator")
        comment = Comment.create!(@attr)
        comment.managable_by(user).should be_true
      end

      it "should return true if user created comment" do
        user = FactoryGirl.create(:user, name: "Simple user", role: "user")
        comment = Comment.create!(@attr.merge(user_id: user.id))
        comment.managable_by(user).should be_true
      end

      it "should return false if there is no current_user" do
        user = FactoryGirl.create(:user, name: "Simple user", role: "user")
        comment = Comment.create!(@attr.merge(user_id: user.id))
        comment.managable_by(nil).should be_false
      end

      it "should return false if comment does not belongs to current_user" do
        user = FactoryGirl.create(:user, name: "Simple user", role: "user")
        comment = Comment.create!(@attr.merge(user_id: @user.id))
        comment.managable_by(user).should be_false
      end
    end

  end
end
