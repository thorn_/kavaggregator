require "spec_helper"

describe Trend do
  before(:each) do
    @attributes = {name: "Trend1", active: false, description: "Trend1 description"}
  end

  it "should create a valid trend" do
    Trend.create!(@attributes).should be_true
  end

  describe "scopes" do
    before(:each) do
      @trend1 = Trend.create!(@attributes.merge(active: true))
      @trend2 = Trend.create!(@attributes.merge(active: true))
      @trend3 = Trend.create!(@attributes)
    end
    it "should return active trends" do
      Trend.active_trends.should include(@trend1, @trend2)
    end
  end
  describe "validations" do
    it "should not create trend without name" do
      trend = Trend.new(@attributes.merge(name: ""))
      trend.should_not be_valid
    end
    it "should require trend description" do
      trend = Trend.new(@attributes.merge(description: ""))
      trend.should_not be_valid
    end
  end

  describe "associations" do
    it "should have many articles" do
      Trend.new.should respond_to(:articles)
    end
  end
end
