require 'spec_helper'

describe Feature do

  it "should create an instance with valid attributes" do
    @category = FactoryGirl.create(:category)
    Feature.create!(category_id: @category.id, count: 1)
  end

  describe "relations" do
    before(:each) do
      @category = FactoryGirl.create(:category)
      @feature_count = Feature.create!(category_id: @category.id)
    end

    it "should have category association" do
      @feature_count.should respond_to(:category)
    end
  end
end
