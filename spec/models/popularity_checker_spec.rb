# --- encoding: utf-8 ---
require "spec_helper"

describe PopularityChecker do
  # Барцухи
  let(:url) {'http://kavigator.ru'}

  it "should get some likes from vk" do
    VCR.use_cassette 'vk_likes' do
      p = PopularityChecker.new(url)
      p.likes({vk: 'http://vk.com/share.php?act=count&index=1&url='}).should == 1
    end
  end

  it "should get some facebook likes" do
    VCR.use_cassette 'fb_likes' do
      p = PopularityChecker.new(url)
      p.likes({fb: 'http://graph.facebook.com/?ids='}).should == 38
    end
  end

  it "should get tweets count" do
    VCR.use_cassette 'tw_likes' do
      p = PopularityChecker.new(url)
      p.likes({tw: 'http://urls.api.twitter.com/1/urls/count.json?callback=twttr.receiveCount&url='}).should == 35
    end
  end

  it "should get klass from odnoklassniki" do
    VCR.use_cassette 'od_likes' do
      p = PopularityChecker.new(url)
      p.likes({od: 'http://www.odnoklassniki.ru/dk?st.cmd=extLike&uid=odklcnt0&ref='}).should == 0
    end
  end

  it "should get 0 likes from moi_mir even if link is empty" do
    VCR.use_cassette 'mm_empty_likes' do
      p = PopularityChecker.new(url)
      p.likes({mm: 'http://connect.mail.ru/share_count?callback=1&func=YOUR_CALLBACK&url_list='}).should == 0
    end
  end

  it "should get likes from moi_mir if there are some" do
    VCR.use_cassette 'mm_success_likes' do
      p = PopularityChecker.new("http://lenta.ru")
      p.likes({mm: 'http://connect.mail.ru/share_count?callback=1&func=YOUR_CALLBACK&url_list='}).should == 211
    end
  end

  it "should get likes from yandex" do
    VCR.use_cassette 'ya_likes' do
      p = PopularityChecker.new(url)
      p.likes({ya: 'http://wow.ya.ru/ajax/share-counter.xml?url='}).should == 0
    end
  end

  it "should get likes from" do
    VCR.use_cassette 'pi_likes' do
      p = PopularityChecker.new(url)
      p.likes({pi: 'http://api.pinterest.com/v1/urls/count.json?callback=YOUR_CALLBACK&url='}).should == 0
    end
  end

  it "should get overall popularity of link" do
    VCR.use_cassette 'all_likes' do
      p = PopularityChecker.new(url)
      p.likes.should == 74
    end
  end

end
