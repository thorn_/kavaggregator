# -*- encoding: utf-8 -*-
require 'spec_helper'

describe NewsSource do
  before(:each) do
    @attr = { name: "Коммерсант", url: "http://www.feeds.kommersant.ru/RSS_Export/RU/daily.xml", period: "15" }
  end

  it "should create an instance with valid attributes" do
    source = NewsSource.create!(@attr)
    source.active.should be_true
  end

  describe "validations" do

    it "should not create instance with no name" do
      NewsSource.new(@attr.merge(name: "")).should_not be_valid
    end

    it "should not create instance without link" do
      NewsSource.new(@attr.merge(url: "")).should_not be_valid
    end

    it "should not create instance without period" do
      NewsSource.new(@attr.merge(period: "")).should_not be_valid
    end

    it "should not create sources with same links" do
      FactoryGirl.create(:news_source, url: "https://www.example.com/feed.xml")
      NewsSource.new(@attr.merge(url: "https://www.example.com/feed.xml")).should_not be_valid
    end

    it "should have valid url" do
      addresses = %w[barbar foobarbar.bazc ftp:\/\/www.me.com]
      addresses.each do |address|
        invalid_url_source = NewsSource.new(@attr.merge(:url => address))
        invalid_url_source.should_not be_valid
      end
    end
  end

  describe "associations" do
    before(:each) do
      @source = NewsSource.create!(@attr)
      @article = FactoryGirl.create(:article, news_source: @source)
    end

    it "should respond to articles" do
      @source.should respond_to(:articles)
    end

    it "should contain article in articles" do
      @source.articles.should include(@article)
    end

    it "should destroy associated articles" do
      lambda do
        @source.destroy
      end.should change(Article, :count).by(-1)
    end
  end

  describe "process_attributes" do
    it "should strip off ignore_helper and find_helper" do
      @source = NewsSource.new(@attr.merge(find_helper: "a,b,c,", ignore_helper: "a,b,c,"))
      lambda do
        @source.save
      end.should change(@source, :find_helper).to("a, b, c")
      @source.split_string("a,b,c,").should == "a, b, c"
      @source.split_string(nil).should == nil
    end
  end

  describe "clear" do
    before(:each) do
      @source = NewsSource.create!(name:"source1", url: "http://www.me.example.com/feed.rss", period: 1)
      @article = FactoryGirl.create(:article, news_source: @source)
      VisitedPage.create!(url: "http://www.he.example.com/page1.html")
      VisitedPage.create!(url: "http://www.me.example.com/page1.html")
      @source.clear
    end

    it "should destroy all associated articles" do
      @source.articles.should be_empty
    end

    it "should destroy all associated visited pages" do
      VisitedPage.where("visited_pages.url LIKE '%me.example.com%'").should be_empty
    end
  end

  describe "scopes" do
    it "should get active sources only" do
      @source1 = NewsSource.create!(@attr)
      @source2 = NewsSource.create!(@attr.merge(active: false, url: FactoryGirl.generate(:url)))
      NewsSource.active.should include(@source1)
      NewsSource.active.should_not include(@source2)
    end
  end
end
