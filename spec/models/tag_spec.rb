require 'spec_helper'

describe Tag do
  describe "relations" do
    it "should have many articles" do
      Tag.new.should respond_to(:articles)
    end
  end
end
