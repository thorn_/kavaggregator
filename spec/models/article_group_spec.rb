require 'spec_helper'

describe ArticleGroup do

  describe "description" do
    before(:each) do
      @group = FactoryGirl.create(:article_group)
      @article1 = FactoryGirl.create(:article, article_group_id: @group.id, title: "lorem1")
      @article2 = FactoryGirl.create(:article, article_group_id: @group.id, title: "lorem2")
      @article3 = FactoryGirl.create(:article, article_group_id: @group.id, title: "lorem1")
    end

    it "should have models_id method" do
      ArticleGroup.new.should respond_to(:model_ids)
    end

    it "should get all freaking ids from all freaking articles" do
      group = ArticleGroup.new(model_ids: [@article1.id, @article3.id].join(','))
      group.save
      group.reload
      group.article_ids.should include(@article1.id, @article2.id, @article3.id)
    end

    it "should increment counter when article is added" do
      # expect{FactoryGirl.create(:article, article_group_id: @group)}.to change{@group.articles_count}.by(1)
      group = FactoryGirl.create(:article_group)
      FactoryGirl.create(:article, article_group_id: group.id)
      group.reload
      group.articles_count.should == 1
    end

    it "should create article group with 0 articles_count" do
      ArticleGroup.new.articles_count.should == 0
    end
  end
end
