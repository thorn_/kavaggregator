require 'spec_helper'

describe Keyword do

  describe "successful creation" do
    it "should create a keyword with valid attributes" do
      Keyword.create!(name:"Keyword", word_type: 1).should be_true
    end
  end

  describe "validations" do

    it "should validate presence of name of keywords" do
      Keyword.new(word_type: 1).should_not be_valid      
    end
    it "should validate type of keyword" do
      Keyword.new(name: "Keyword", word_type: nil).should_not be_valid
    end
  end

  describe "scopes" do
    before(:each) do
      @required_keyword = Keyword.create!({name:"required", word_type: "1"})
      @should_be_one = Keyword.create!({name:"should_be_one", word_type: "2"})
      @not_inlcuded = Keyword.create!({name:"not_inlcuded", word_type: "3"})
    end

    it "should have a required keywords scope" do
      Keyword.required.should include(@required_keyword)
    end

    it "should have a should_be_one keywords scope" do
      Keyword.should_be_one.should include(@should_be_one)
    end

    it "should have a required keywords scope" do
      Keyword.not_included.should include(@not_inlcuded)
    end
  end
end
 
