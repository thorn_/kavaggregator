#-*- encoding: utf-8 -*-
require "spec_helper"

describe User do
  before(:each) do
    @attr = {name: "user", email: "me@example.com", password: "secret"}
    @article = FactoryGirl.create(:article)
  end

  describe "methods" do

    before(:each) do
      @user = User.create(@attr)
    end

    it "should get online users" do
      @user.online?.should be_true
    end

    it "should have user role" do
      @user.role.should == "user"
    end

    it "should not show user online after user logging of" do
      @user.offline
      @user.online?.should be_false
    end

    it "should convert string role to symbol for declarative authorization" do
      @user.role_symbols.class.should == Array
      @user.role_symbols[0].class.should == Symbol
    end

    it "should return user role name" do
      @user.user_role.should == "Пользователь"
    end
  end


  describe "associations" do
    before(:each) do
      @user = User.create(@attr)
      @comment = FactoryGirl.create(:comment, user: @user)
    end
    it "should have comments association" do
      @user.should respond_to(:comments)
      @user.comments.should include(@comment)
    end
  end
end
