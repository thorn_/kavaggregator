# -*- encoding: utf-8 -*-
require 'spec_helper'

describe Category do
  it "should create a category with valid attributes" do
    @attr = {name: "category1"}
    Category.create!(@attr).should be_true
  end

  it "should have a zero default value of count" do
    @attr = {name: "category1"}
    @cat = Category.create!(@attr)
    @cat.count.should be_zero
  end

  describe "validations" do
    it "should not create a category without name" do
      @attr = {name: ""}
      Category.new.should_not be_valid
    end
  end

  describe "relationships" do
    before(:each) do
      @attr = {name: "Дагестан"}
      @cat = Category.create!(@attr)
      @keyword = FactoryGirl.create(:keyword)
    end

    it "should respond to articles" do
      @cat.should respond_to(:articles)
    end

    describe "classifier relations" do
      before(:each) do
        @cat = Category.create!(name: "Category")
        @ft = FactoryGirl.create(:feature, category: @cat)
      end

      it "should have a categories count relations" do
        @cat.should respond_to(:features)
      end

      it "should have feature in features" do
        @cat.features.should include(@ft)
      end
    end
  end

  describe "defaults" do
    it "should create a category with default value of minimum 0" do
      cat = Category.create!(name: "name")
      cat.minimum.should be_zero
    end
  end
end
