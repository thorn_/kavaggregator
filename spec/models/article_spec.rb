# -*- encoding: utf-8 -*-
require 'spec_helper'

describe Article do
  before(:each) do
    @cat1 = FactoryGirl.create(:category)
    @cat2 = FactoryGirl.create(:category)
    @source = FactoryGirl.create(:news_source)
    @attr = {title: "lorem",
      text: "<p>Lorem ipsum dolor sit amet, consectetur</p>",
      category_id: @cat1.id,
      news_source_id: @source.id}
    @large_text = "
          Т. Гасанова
          Это история о верности слову, что сильнее времени. Это история о материнской любви и кровном родстве, стирающих расстояния. Это история о памяти, которая не меркнет. С первых дней замужества Таибат поняла, что в ее новой семье есть какая-то давняя печаль. Чуткое юное сердце живо откликалось на глубокие вздохи, порой вырывавшиеся у мужа Абумуслима, на отрешенный взгляд свекрови Атикат. Она замечала, как подолгу застывал глава семьи Гаджибутта перед портретом на стене. С фотографии смотрел светло и открыто совсем еще молодой парень. Таибат знала, что это брат ее мужа, Гаджи. Знала и то, что он не вернулся с войны. Но было, было, она чувствовала, что-то еще, невысказанное, затаенное.
          Однажды, когда Атикат, сидя за вязанием, всплакнула без видимой причины, Таибат решилась спросить, чем печалится ее сердце. Свекровь встала, достала из шкафа коробку, где хранились семейные реликвии, и протянула невестке небольшой, истончившийся от времени листок: «... лейтенант Гаджи Гаджиев, уроженец села Хури Лакского района ДАССР, пропал без вести 30 апреля 1942 года». Таибат пробежала лаконичный текст еще раз. Пропал без вести. Ее словно ударило по глазам. Внезапная вспышка высветила причину непреходящей грусти в глазах Атикат, которую она с первых дней звала мамой. Женщины - старая и молодая - обнялись и заплакали вместе...
          Гаджи был бравый курсант Буйнакского пехотного училища. В удостоверении подпись помощника начальника строевого отдела старшего лейтенанта Луночкина и дата - 13 мая 1940 года. Если б не было войны, паренек из Хури мог бы сделать воинскую карьеру. У него были для этого все необходимые качества: упорство, целеустремленность, хорошая физическая подготовка. А главное - дух. Это был храбрый юноша, настоящий горец.
          Мать знала, что он на фронте с первых дней. Черная тарелка репродуктора – одна на все село - была единственным средством узнать о событиях на передовой. Даже скудного знания русского языка хватало, чтобы понять: дела на фронте плохи. Тревожный голос Левитана, от которого по коже пробегали мурашки, перечислял названия оставленных населенных пунктов. Все мрачнело и мрачнело лицо мужа, которому горская сдержанность не позволяла делиться с женой своими опасениями. Последнюю весточку получили из-под Курска: «Жив, здоров, воюю». И ни слова о том, что перебросили их часть в самую мясорубку. Битва на Курской дуге вошла в историю Великой Отечественной как рекордная по количеству погибших и раненых. Тогда в том кипящем котле как было считать их? Как идентифицировать кого-либо в месиве человеческих тел?
          И полетело в далекий Лакский район извещение: пропал без вести. И хуже похоронки, и лучше. Может, живой? Где ж его искать? А если нет в живых, где зарыт, какой земле предан? Мать потеряла покой навсегда. Она просыпалась с этой мыслью и засыпала с ней: «Где могилка сына?». А во сне видела его живого, тянулась, чтобы обнять. Он исчезал, растворялся, и Атикат просыпалась в слезах.
          Сын и дочь росли, не зная брата. Утешали маму, как могли. Она крепилась, чтобы не выдать сердечную муку, но на дне ее глаз, как темная вода, плескалась неизбывная грусть. Куда только не обращались Гаджиевы в своих попытках найти родную могилу. Все было тщетно. Ушел из жизни отец, так и не дождавшись. Завещал не прекращать поиски. Даже в затухающих его глазах брезжила надежда. Мать осталась с наказом мужа. Не переставая, рассылала письма в военкоматы, Советы ветеранов войны, архивы. Но дети стали замечать, что она все чаще говорит о Гаджи как о живом. Видно, чем дальше от войны, тем больше тешила она себя мыслью: а вдруг он жив? Может, был в плену? Может, был ранен, контужен, потерял память? О материнское сердце! Сколько в тебе любви и веры.
          Силы Атикат слабели. Она стала часто дремать за вязанием. Но одно, как заметили домочадцы, действовало на нее подобно бодрящему лекарству. Стоило зазвучать по телевизору позывным передачи «Служу Советскому Союзу», как старая женщина стряхивала с себя оцепенение и приковывалась глазами к экрану. «Бабушка, ты что смотришь так внимательно?», - не выдержали как-то внуки. На экране сменяли друг друга кадры хроники. Офицеры, солдаты в строю. «Может, нашего Гаджи среди них увижу», - глухо произнесла Атикат, а потом долго шепталась о чем-то с невесткой. О чем могут говорить матери? Они говорили о сыновьях. Бережно, как бусины из драгоценного ожерелья, перебирала Атикат воспоминания о Гаджи. Как он рос, мужал, как радовал их.
          Когда она слегла в 1974-м, не было дня, чтобы не вспоминала сына. И в ее отлегающее дыхание вплелись слова: «Найдите его, найдите...». Таибат попросила подключиться к поискам сестру. Криминалист с большим опытом, ветеран МВД, Асият Цахаева взялась за дело профессионально. Ее трудно было сбить с толку стандартными ответами. Настойчивости ей не занимать. Начала она поиск с Буйнакского военкомата. Там подняли все карточки. Потом пошла Асият Цахаева в Министерство социальной защиты. Будто сам Всевышний направлял ее шаги. Посоветовала, как надо действовать, руководитель рабочей группы Книги Памяти Валентина Васильевна Макарова.
          Последнее письмо ведь Гаджи прислал из-под Курска. А там в той ужасной сумятице вполне могли спутать что-нибудь. Может, это «двойное» отчество Гаджибутаевич причина путаницы, подсказала она. Следующие запросы сделала Асият Цахаевна уже с «сокращенным» отчеством: Бутаевич. Месяцы шли за месяцами, складывались в годы. Росла кипа писем, ответов, аккуратно собираемых Цахаевой. Лишь в конце 2005-го переписка принесла результат. Дрожащими от волнения руками держала она вынутую из конверта бумагу. Несколько сухих строк.
          (6092/6189) ФИО: Гаджиев Гаджи Бутаевич; МЕСТО РОЖДЕНИЯ: РЕСП.: Дагестанская АССР; ГЕОГР.: Лакский р-н, с.Хуры; МЕСТО ПРИЗЫВА: ВК: К; В/Ч: 1032 стр.полк, 293 стр. дивизия, 40 армия; ЗВАНИЕ: л-т; ДОЛЖН.: ком-р пуль.взвода; ВРЕМЯ ВЫБ.: 26.11.1941; ПРИЧ.ВЫБ.: погиб в бою; МЕСТО ГИБЕЛИ: МЕСТО ЗАХОРОН: ГЕОГР.: г. Тим, с.Выгорное; ОП.: 818883 с; Д. 476; Л.: 95.
          Таибат, несмотря на то, что сама уже немолода, поехала бы сразу, таково было ее нетерпение поклониться могиле ни разу не видевшего ее деверя. Но зима выдалась суровая, и поездку отложили на весну. Тем временем Асият Цахаева регулярно каждую пятницу созванивалась с председателем Тимского районного Совета ветеранов Виктором Черных. Он докладывал ей «обстановку». Районный Совет ветеранов обратился с ходатайством на имя военного комиссара Тимского района о внесении в книгу именного списка на военнослужащих и партизан Великой Отечественной войны, погибших в боях и умерших от ран, захороненных на территории Тимского района Курской области, Гаджи Бутаевича Гаджиева.
          Они сделали все, что от них зависело, чтобы увековечить память воина, погибшего, защищая Родину. Подробнейшим образом описал Черных в письме, как добраться до поселка Тим, села Выгорное. По телефону рассказывал, что в апреле установили плиту на братской могиле, где покоятся 250 воинов. Золотом высечены имена сорока человек, кто известен. Отдельно - как офицера - табличка с именем лейтенанта Гаджи Гаджиева.
          А в мае Таибат собралась в дорогу со своим старшим сыном Асланом. Ехали 600 км от Москвы на машине. Их встретил Виктор Черных. Теплая была встреча, трогательная. Он для Гаджиевых и Цахаевых как родной.
          Братская могила расположена возле школы. Она огорожена, ухожена руками школьников и учителей. Таибат поехала не с пустыми руками. Сладости раздала в школе, чтобы помянули Гаджи. Со слезами на глазах стояли мать и сын у могилы. Может, видела это с небес Атикат? Мысленно обратилась к ней Таибат: «Мама, вот мы и нашли нашего Гаджи». И запоздалые цветы легли на могилу, где покоятся рядом русский и лакец, татарин и казах, украинец и грузин. Они знали одну Родину. И та, что звалась Победа, не различала наций.
        "
  end

  describe "with valid attributes" do
    it "should create an article" do
      Article.create!(@attr).should be_true
    end

    it "should be unpublished" do
      @art = Article.create!(@attr)
      @art.published.should be_false
    end
  end

  describe "associations" do
    before(:each) do
      @article = Article.create!(@attr)
    end

    it "should have a trend" do
      Article.new.should respond_to(:trend)
    end

    it "should respond to groups method" do
      Article.new.should respond_to(:article_group)
    end

    it "should have some groups" do
      @group = FactoryGirl.create(:article_group)
      @art = Article.create!(@attr.merge(article_group_id: @group.id))
      @art.article_group.should == @group
    end

    it "should respond to categories method" do
      @article.should respond_to(:category)
    end

    it "should have category" do
      @article.category.should == @cat1
    end

    it "should have a published scope" do
      @article.published = true
      @article.save
      Article.published.should include(@article)
    end

    it "should respond to source method" do
      @article.should respond_to(:news_source)
    end

    it "should have a source" do
      @article.news_source.should == @source
    end

    it "should respond to comments" do
      @article.should respond_to(:comments)
    end

    it "should include comment" do
      comment = FactoryGirl.create(:comment, article_id: @article.id)
      @article.comments.should include(comment)
    end

    it "should destroy associated comments when article is deleted" do
      comment = FactoryGirl.create(:comment, article_id: @article.id)
      expect {@article.destroy}.to change(Comment, :count).by(-1)
    end

    it "should have many tags" do
      Article.new.should respond_to(:tags)
    end
  end

  describe "validations" do

    it "should reject articles without title" do
      Article.new(@attr.merge(title:"")).should_not be_valid
    end

    it "should reject articles without text" do
      Article.new(@attr.merge(text:"")).should_not be_valid
    end

    it "should reject articles without source" do
      Article.new(@attr.merge(news_source_id: nil)).should_not be_valid
    end
  end

  describe "methods" do

    context "calculate_hotness" do
      let(:article) {FactoryGirl.create(:article, url: "http://kavigator.ru")}
      it "should fill votes field" do
        VCR.use_cassette 'all_likes' do
          article.calculate_hotness!
          article.votes.should == 74
        end
      end

      it "should fill the rating field" do
        VCR.use_cassette 'all_likes' do
          article.calculate_hotness!
          article.rating.should > 0
        end
      end

    end

    it "should fix counters" do
      group = FactoryGirl.create(:article_group)
      article = Article.create!(@attr.merge(article_group_id: group.id, title: "lorem1"))
      article.fix_counter
      article.reload
      article.article_group.articles_count.should == 1
    end

    context "nested_articles" do
      let(:group) { FactoryGirl.create(:article_group) }
      let(:article) { Article.create!(@attr.merge(article_group_id: group.id)) }

      it "should get articles from same group" do
        article2 = Article.create!(@attr.merge(article_group_id: group.id))
        article.nested_articles.should include(article2)
      end

      it "should not get other articles" do
        article2 = Article.create!(@attr)
        article.nested_articles.should_not include(article2)
      end

      it "should return empty array if article has no group" do
        article3 = FactoryGirl.create(:article, article_group_id: nil)
        article3.nested_articles.should be_empty
      end
    end

    it "should get just nested ids" do
      group = FactoryGirl.create(:article_group)
      article1 = FactoryGirl.create(:article, article_group: group, title: "lorem1", created_at: Date.today)
      article2 = FactoryGirl.create(:article, article_group: group, title: "lorem2", created_at: Date.today - 1)
      article1.nested_article_ids.should include(article1.id, article2.id)
    end

    it "should return empty array if article has no group" do
      article3 = FactoryGirl.create(:article, article_group_id: nil)
      article3.nested_article_ids.should be_empty
    end

    it "should return source name at any cost" do
      article1 = Article.create!(@attr.merge(news_source_id: -1))
      article1.source_name.should == "Источник не указан"
      source = FactoryGirl.create(:news_source, name: "My source")
      article2 = Article.create!(@attr.merge(news_source_id: source.id))
      article2.source_name.should == "My source"
    end

    it "should retrun comments count" do
      article = FactoryGirl.create(:article)
      article.comments_count.should be_zero
      FactoryGirl.create(:comment, article: article)
      article.comments_count.should == 1
    end

    describe "train" do
      it "should categorize article" do
        art = Article.create!(@attr.merge(category_id: nil))
        category = FactoryGirl.create(:category)
        art.train(category)
        art.reload
        art.category.should eq(category)
      end
    end

    describe "other posts function" do
      it "should fetch other posts from article's trend and add last articles published before given" do
        trend = FactoryGirl.create(:trend)
        article1 = FactoryGirl.create(:article, trend: trend)
        article2 = FactoryGirl.create(:article, trend: nil)
        article = FactoryGirl.create(:article, trend: trend)
        article.other_articles.should include(article1)
        article.other_articles.should_not include(article)
      end

      it "should take articles from active trends if it does not belongs to one and add last articles published before given" do
        trend = FactoryGirl.create(:trend, active: true)
        article1 = FactoryGirl.create(:article, trend: trend)
        article2 = FactoryGirl.create(:article, trend: trend)
        article3 = FactoryGirl.create(:article, trend: nil)
        article = FactoryGirl.create(:article, trend: nil)
        article.other_articles.should include(article1)
        article.other_articles.should include(article2)
        article.other_articles.should include(article3)
        article.other_articles.should_not include(article)
      end
    end

  end

  describe "training classificator" do
    before(:each) do
      @source = FactoryGirl.create(:news_source, url: FactoryGirl.generate(:url))
      @article = Article.create!(@attr)
      @category = FactoryGirl.create(:category)
    end

    it "should place article in appropriate category" do
      @article.train(@category)
      @article.category.should == @category
      @article.published.should be_true
    end

    it "should change the feature table" do
      lambda do
        @article.train(@category)
      end.should change(Feature, :count)
    end

    it "should not train classifier if there are more than 300 words" do
      lambda do
        @article = Article.create!(@attr.merge(text: @large_text))
        @article.train(@category)
      end.should_not change(Feature, :count)
    end
  end

  it "should group articles that created 3 days ago and are published" do
    @article1 = FactoryGirl.create(:article, published: true, text: "lorem ipsum dolor sit amet", created_at: DateTime.now)
    @article2 = FactoryGirl.create(:article, published: true, text: "lorem ipsum dolor sit amet", created_at: DateTime.now - 1)
    @article3 = FactoryGirl.create(:article, published: false, text: "lorem ipsum dolor sit amet", created_at: DateTime.now - 2)
    @article4 = FactoryGirl.create(:article, published: true, text: "lorem ipsum dolor sit amet", created_at: DateTime.now - 8)
    Article.group_similar(3)
    @article1.reload
    @article2.reload
    @article3.reload
    @article4.reload
    @article1.article_group.articles.should include(@article1, @article2)
  end

end
