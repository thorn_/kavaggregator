require 'faye'

class ServerAuth
  def incoming(message, callback)
    if message['channel'] !~ %r{^/meta/}
      if message['ext']['auth_token'] != "holy_crap_token_oh_my_gosh"
        message['error'] = 'Invalid authentication token'
      end
    end
    callback.call(message)
  end
end

bayeux = Faye::RackAdapter.new(mount: '/faye', timeout: 25)

bayeux.add_extension(ServerAuth.new)
bayeux.listen(9292)
