class FeedbackMailer < ActionMailer::Base
  default from: "kavigator@gmail.com"

  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.feedback_mailer.feedback.subject
  #
  def feedback(params)
    @contacts = params[:contacts]
    @text = params[:text]

    mail to: "admin@kavigator.ru"
  end
end
