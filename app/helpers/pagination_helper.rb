module PaginationHelper

  def windowed_page_numbers(total, page, per_page)
    return [1] if total < 1
    inner_window = 4
    outer_window = 1
    total_pages = (total / per_page).next
    window_from = page - inner_window
    window_to   = page + inner_window

    if window_to > total_pages
      window_from -= window_to - total_pages
      window_to    = total_pages
    end
    if window_from < 1
      window_to  += 1 - window_from
      window_from = 1
      window_to   = total_pages if window_to > total_pages
    end

    middle = (window_from..window_to).to_a

    if outer_window + 3 < middle[0]
      left = (1..(outer_window + 1)).to_a
      left << "..."
    else
      left = (1...middle[0]).to_a
    end

    if (total_pages - outer_window - 2) > middle[middle.length - 1]
      right = ((total_pages - outer_window)..total_pages).to_a
      right.unshift "..."
    else
      right_start = [middle[middle.length - 1] + 1, total_pages].min
      right = (right_start..total_pages).to_a
      right = [] if right_start == total_pages
    end
    left.concat(middle.concat(right))
  end

  def page_info(params)
    total = params[:total_count]
    page = params[:page]
    per_page = params[:per_page]
    total_pages = (total / per_page).next
    windowed_page_numbers = windowed_page_numbers(total, page, per_page)
    previous_page = page - 1 if page > 1
    next_page = page + 1 if page < total_pages
    {
      total: total,
      page: page,
      per_page: per_page,
      pagination: windowed_page_numbers,
      previous_page: previous_page,
      next_page: next_page,
      original_filter: params
    }
  end
end
