# -*- encoding: utf-8 -*-
module CommentHelper
  def nested_comments(comments)
    comments.map do |comment, sub_comments|
      content_tag(:div, (render(comment) + content_tag(:div, nested_comments(sub_comments), class: "replies")), class: "comment")
    end.join.html_safe
  end

  def prepare_comments(comments)
    if comments.count > 0
      comments = comments.inject([]) { |res, comment| res << comment.subtree }
      comments.flatten
    end
  end
end
