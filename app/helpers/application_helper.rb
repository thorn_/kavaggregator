# -*- encoding: utf-8 *-
module ApplicationHelper
  def show_title
    default_title = "Новости Дагестана | Кавигатор"
    if @title
      "#{@title} | #{default_title}"
    elsif @article
      "#{@article.title} | #{@article.category_name} | #{default_title}"
    elsif @filter
      category = Category.find_by_id(@filter[:cat_id])
      sources  = NewsSource.find_all_by_id(@filter[:source_id])
      trend    = Trend.find_by_id(@filter[:trend_id])
      sources_title = sources.map(&:name).join(' | ') if sources.any?
      if trend
        "#{trend.name} | #{default_title}"
      elsif category && sources.any?
        "#{sources_title} | #{category.name} | #{default_title}"
      elsif category && sources.empty?
        "#{category.name} в Дагестане | #{default_title}"
      elsif category.nil? && sources.any?
        "#{sources_title} | #{default_title}"
      else
        default_title
      end
    else
      default_title
    end
  end

  def broadcast(channel, comment)
    message = {channel: channel, data: comment, ext: {auth_token: FAYE_TOKEN}}
    uri = URI.parse("http://91.205.130.20:9292/faye")
    Net::HTTP.post_form(uri, message: message.to_json)
  end

  def meta_tags
    meta = ''
    if @meta
      meta += '<meta name="description" content="'+ @meta + '">'
    else
      meta += '<meta name="description" content="Кавигатор - все новости Дагестана в одном месте. Последние события, происшествия и аналитика.">'
    end
    meta += '<meta charset="UTF-8">'
    meta += '<meta name="viewport" content="width=device-width, initial-scale=1.0">'
  end

  def additional_javascript_permitted
    Rails.env == 'production' && !permitted_to?(:be_free, :from_tracking)
  end

  def image_url_for(article)
    if article.img_url == Article::DEFAULT_IMAGE || article.img_url.blank?
      root_url + Article::DEFAULT_IMAGE
    else
      article.img_url
    end
  end

end
