# -*- encoding: utf-8 -*-
module ArticleHelper
  def article_category_link(article)
    if article.category
      link_to article.category.name, articles_path(cat_id: article.category)
    else
      raw("&nbsp;")
    end
  end

  def article_source_link(article)
    link_to article.news_source.name, articles_path(source_id: article.news_source.id), target: "_blank"
  end

  def large_article_sign(article)
    if send(:words_count, Nokogiri::HTML(article.text).text) > 300
      link_to raw("&nbsp"), article_path(article), class: "icon-list"
    else
      raw("&nbsp;")
    end
  end
end
