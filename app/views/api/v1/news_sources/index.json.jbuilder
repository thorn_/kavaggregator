json.array!(@news_sources) do |news_source|
  json.id news_source.id
  json.name news_source.name
end
