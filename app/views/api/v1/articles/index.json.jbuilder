json.array!(@articles) do |article|
  json.title article.title
  json.url article_url(article)
  json.img_url article.img_url
  json.summary article.summary
  json.published_date article.published_date
  json.source_name article.source_name
  json.category_id article.category_id
  json.source_id article.news_source_id
end
