json.articles @articles do |article|
  json.id article.id
  json.title article.title
  json.url article.url
  json.text article.text
  json.img_url article.img_url
  json.summary article.summary
  json.category_id article.category_id
  json.published_date article.published_date
  json.source_name article.source_name
  json.source_id article.news_source_id
  json.category_name article.category_name
  json.comments_count article.comments_count
  json.nested_articles article.nested_articles
end

json.pagination do
  json.total @filter[:total_count]
  json.page @filter[:page].to_i
  json.per_page @filter[:per_page]
end

json.current_state do
  json.cat_id @filter[:cat_id]
  json.source_id @filter[:source_id].first
  json.trend_id @filter[:trend_id]
end

json.current_user current_user ? current_user.id : -1

json.categories @categories do |category|
  json.id category.id
  json.name category.name
  json.active (category.id == @filter[:cat_id])
end

json.news_sources @news_sources do |news_source|
  json.id news_source.id
  json.name news_source.name
  json.active (news_source.id == @filter[:source_id].first)
end

json.trends @trends do |trend|
  json.id trend.id
  json.name trend.name
  json.active (trend.id == @filter[:trend_id])
end
