class Tag < ActiveRecord::Base
  attr_accessible :name, :description
  has_and_belongs_to_many :articles

  validates :name, presence: true
end
