class Category < ActiveRecord::Base
  has_many :articles
  has_many :features, dependent: :destroy

  attr_accessible :name, :count, :minimum

  validates :name, presence: true

end
