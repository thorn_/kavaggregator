# -*- encoding: utf-8 -*-
class NewsSource < ActiveRecord::Base

  attr_accessible :name, :url, :find_helper, :ignore_helper, :period, :active

  before_save :process_attributes

  has_many :articles, dependent: :destroy

  validates :name, presence: true
  validates :url,  presence: true,
                   uniqueness: true,
                   format: /\A(http:\/\/|https:\/\/)www\.[a-zA-Z0-9\-\_\.]+\.[a-zA-Z]{2,4}(\/\S*)?\z/i
                   # format: /\A(http:\/\/|https:\/\/|\S)[a-zA-Z0-9\-\_\.]+\.[a-zA-Z]{2,3}(\/\S*)?\z/i
  validates :period, presence: true

  scope :active, ->{ where(:active => true) }

  def process_attributes
    self.ignore_helper = split_string(ignore_helper)
    self.find_helper   = split_string(find_helper)
  end

  def split_string(str)
    str.split(",").collect {|s| s.chomp.strip}.select{|s| s != ''}.join(', ') if str
  end

  def clear
    self.articles.destroy_all
    host = URI(url).host.match(/^www.(\S*)/i)[1]

    VisitedPage.where("visited_pages.url LIKE :host", host: "%#{host}%").destroy_all
  end

end
