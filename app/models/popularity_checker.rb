require "curb"

class PopularityChecker
  def initialize(url)
    @url = url
  end

  def likes(links = nil)
    @links_to_check = links
    likes = links_to_check.map do |link|
      begin
        http = get_link(link[1])
        self.send("#{link[0]}_likes", http.body_str)
      rescue
        0
      end
    end
    likes.compact.inject(0, :+)
  end

  def get_link(link)
    url = link + @url
    Curl.get(url)
  end

  def links_to_check
    @links_to_check || {
      vk: 'http://vk.com/share.php?act=count&index=1&url=',
      fb: 'http://graph.facebook.com/?ids=',
      tw: 'http://urls.api.twitter.com/1/urls/count.json?callback=twttr.receiveCount&url=',
      od: 'http://www.odnoklassniki.ru/dk?st.cmd=extLike&uid=odklcnt0&ref=',
      mm: 'http://connect.mail.ru/share_count?callback=1&func=YOUR_CALLBACK&url_list=',
      ya: 'http://wow.ya.ru/ajax/share-counter.xml?url=',
      pi: 'http://api.pinterest.com/v1/urls/count.json?callback=YOUR_CALLBACK&url='
    }
  end

  def vk_likes(body_str)
    body_str.match(/VK.Share.count\(1, (\d*)\)/)[1].to_i
  end

  def fb_likes(body_str)
    response = JSON.parse(body_str)
    response[@url]["shares"]
  end

  def tw_likes(body_str)
    body_str.match(/"count":(\d*)/i)[1].to_i
  end

  def od_likes(body_str)
    body_str.match(/ODKL.updateCount\('odklcnt0','(\d*)'/)[1].to_i
  end

  def mm_likes(body_str)
    if res = body_str.match(/shares":(\d*),/m)
      res[1].to_i
    else
      0
    end
  end

  def ya_likes(body_str)
    body_str.match(/(\d+)/)[1].to_i
  end

  def pi_likes(body_str)
    body_str.match(/"count":(\d*)/i)[1].to_i
  end

end
