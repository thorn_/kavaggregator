# -*- encoding: utf-8 -*-
require "classifier"
require "date"
require "sitemap_generator"
require "checker"
require "hstore_methods"

class Article < ActiveRecord::Base
  include ActionView::Helpers::DateHelper
  include HstoreMethods
  serialize :properties, ActiveRecord::Coders::Hstore

  DEFAULT_IMAGE = '/assets/default_news.png'

  attr_accessible :text, :title, :url, :img_url, :published, :news_source_id, :category_id, :tag_ids, :summary, :article_group_id, :trend_id, :properties, :rating

  define_boolean(:reviewed, :shared, :sticky)
  define_integer(:review_count, :votes)

  belongs_to :category
  belongs_to :news_source
  belongs_to :article_group, counter_cache: true
  belongs_to :trend

  has_many :comments, dependent: :destroy
  has_and_belongs_to_many :tags

  validates :title,  presence: true
  validates :text, presence: true
  validates :news_source_id, presence: true

  scope :published     ,lambda { where(published: true)}
  scope :not_published ,lambda { where(published: false)}

  after_update :fix_counter

  def fix_counter
    if group_id = self.article_group_id
      ArticleGroup.reset_counters(group_id, :articles)
    end
  end

  def self.text_search(query)
    if query.present?
      rank = <<-RANK
        ts_rank(to_tsvector('russian', title), plainto_tsquery('russian', #{sanitize(query)})) +
        ts_rank(to_tsvector('russian', text), plainto_tsquery('russian', #{sanitize(query)}))
      RANK
      # where("title @@ :q or text @@ :q", q: "#{query}").order("#{rank} DESC")
      where("title @@ :q or text @@ :q", q: "#{query}").order("created_at DESC")
    else
      scoped
    end
  end

  def self.feed_me
    NewsSource.active.each do |source|
      feed = Feedzirra::Feed.fetch_and_parse(source.url)
      next if feed.class == Fixnum or feed.nil?
      feed.entries.each do |entry|
        next if VisitedPage.find_by_url(entry.url)
        page = Page.new(entry.url)
        page.process do |html|
          PageProcessor.process(
            html,
            [ImageProcessor, IgnoredProcessor, AttributeProcessor, LinkProcessor],
            { find: source.find_helper,
              ignored: source.ignore_helper,
              host: URI.parse(URI.encode(entry.url)).host })
        end

        if check_page(page) and Article.find_by_url(entry.url).nil?
          if entry.summary and entry.summary.length > 0
            summary = Nokogiri::HTML(entry.summary).text[0..300]
          else
            summary = page.summary
          end
          article = Article.create!(title: entry.title, url: entry.url,
                          news_source_id: source.id,
                          img_url: page.image, text: page.text, summary: summary)
          article.calculate_hotness!
        end
        VisitedPage.create(url: entry.url)
      end
    end
  end

  def self.check_page(page)
    checkers = [RequiredWordsCheck, ArticleLengthCheck, ExcludedWordCheck]
    checkers.each do |checker|
      ch = checker.new(page.text)
      return false unless ch.check
    end
    true
  end

  def train(category)
    text = Nokogiri::HTML(self.text).text
    if send(:get_words, text).count < 150
      cl = FisherClassifier.new()
      cl.train(text, category)
    end
    self.update_attributes(category_id: category.id, published: true, article_group_id: ArticleGroup.create.id)
  end

  def published_date
    if created_at < DateTime.now - 3
      created_at.to_s(:ru_datetime)
    else
      time_ago_in_words(self.created_at) + " назад"
    end
  end

  def source_name
    if news_source then news_source.name else "Источник не указан" end
  end

  def category_name
    if category then category.name else "" end
  end

  def comments_count
    comments.count
  end

  def self.fetch_and_classify(days = 7, percent = 25)
    cl = FisherClassifier.new
    feed_me
    Article.not_published.order("created_at DESC").each do |art|
      text = Nokogiri::HTML(art.text).text
      next if send(:get_words, text).count > 500
      res = cl.classify(text)
      art.update_attributes(published: true, category_id: res[0].id, article_group_id: ArticleGroup.create.id) if res[0]
    end

    # grouper = ArticleGrouper.new(Article.published.where("created_at >= ?", DateTime.now - days), percent)
    # grouper.group()
    ArticleGroup.includes(:articles).where(articles: {article_group_id: nil}).destroy_all
  end

  def nested_articles
    group = self.article_group
    return [] unless group
    group.articles.order("created_at DESC").where("id != ?", self.id)
  end

  def nested_article_ids
    group = self.article_group
    return [] unless group
    group.article_ids
  end

  def self.group_similar(days = 7, percent=20)
    grouper = ArticleGrouper.new(Article.published.where("created_at >= ?", DateTime.now - days), percent)
    grouper.group()
    ArticleGroup.includes(:articles).where(articles: {article_group_id: nil}).destroy_all
  end

  def self.filter_articles(params)
    ArticleFilter.filter(params)
  end

  def other_articles
    @other_articles = []
    if self.trend
      @other_articles << Article.order("updated_at DESC").where("updated_at < ?", self.updated_at).limit(3)
      @other_articles << self.trend.articles.order("updated_at DESC").where("updated_at < ?", self.updated_at).limit(12)
    else
      @other_articles << Article.order("updated_at DESC").where("updated_at < ?", self.updated_at).limit(3)
      Trend.active_trends.order("updated_at DESC").limit(4).each {|t| @other_articles << t.articles.where("updated_at < ?", self.updated_at).order("updated_at DESC").limit(3)}
    end
    @other_articles.flatten!
  end

  def calculate_hotness!
    p = PopularityChecker.new(self.url)

    seconds = (self.created_at.to_i - Time.local(2010, 8, 10, 7, 46, 43).to_time.to_i).to_i
    s = p.likes
    displacement = Math.log( [s.abs, 1].max,  10 )

    self.rating = (displacement + ( seconds / 45000 )).to_i
    self.votes = s
    self.save
  end
end
