class ArticleGroup < ActiveRecord::Base
  has_many :articles

  attr_accessible :model_ids, :articles_counter

  attr_reader :model_ids

  def model_ids=(ids)
    ids = ids.split(',').map(&:to_i)
    articles = Article.find_all_by_id(ids)
    self.article_ids = articles.map{|a| a.nested_article_ids}.flatten
  end
end
