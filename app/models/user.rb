# -*- encoding: utf-8 -*-
class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :token_authenticatable, :confirmable,
  # :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable, :omniauthable,
         :recoverable, :rememberable, :trackable, :validatable

  attr_accessible :email, :password, :password_confirmation, :remember_me
  attr_accessible :name, :provider, :uid, :salt

  validates_presence_of :name
  has_many :comments

  def self.from_omniauth(auth)
    where(auth.slice(:provider, :uid)).first_or_create do |user|
      user.provider = auth.provider
      user.uid = auth.uid
      user.name = auth.info.name
      user.email = auth.info.email
      if ["twitter"].include?(auth.provider) and auth.info.email.nil?
        user.email = "#{auth.info.nickname}@#{auth.provider}.com"
      end
    end
  end

  def online?
    self.updated_at >= 5.minutes.ago
  end

  def offline
    self.updated_at = 6.minutes.ago
    self.save
  end

  def role_symbols
    user_role = self.role
    if user_role then [user_role.to_sym] else [:guest] end
  end

  def user_role
    roles = {"administrator" => "Администратор", "user" => "Пользователь", "moderator" => "Модератор"}
    roles[self.role]
  end

  def self.new_with_session(params, session)
    if session['devise.user_attributes']
      new(session['devise.user_attributes'], without_protection: true) do |user|
        user.attributes = params
        user.valid?
        session['devise.user_attributes'] = nil
      end
    else
      super
    end
  end

  def password_required?
    super && provider.blank?
  end

end
