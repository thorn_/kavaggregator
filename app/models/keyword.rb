class Keyword < ActiveRecord::Base

  attr_accessible :name, :public_name, :word_type
  validates_presence_of :name
  validates_presence_of :word_type

  scope :required,      where(word_type: 1)
  scope :should_be_one, where(word_type: 2)
  scope :not_included,  where(word_type: 3)

end
