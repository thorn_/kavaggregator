# -*- encoding: utf-8 -*-
class Comment < ActiveRecord::Base

  include ActionView::Helpers::DateHelper

  attr_accessible :ancestry, :article_id, :content, :user_id, :parent_id
  has_ancestry

  validates :article_id, presence: true
  validates :user_id, presence: true
  validates :content, presence: true

  belongs_to :article, touch: true
  belongs_to :user

  def get_content
    deleted ? 'Комментарий был удален' : content
  end

  def user_name
    user.name
  end

  def published_date
    if created_at < DateTime.now - 3
      created_at.to_s(:ru_datetime)
    else
      time_ago_in_words(created_at) + " назад"
    end
  end

  def managable_by(cuser)
    !cuser.nil? and (cuser.id == user_id or cuser.role == "administrator")
  end

  def as_json(options = {})
    super (options || { }).merge(
      except: [:created_at, :updated_at, :article_id, :deleted, :ancestry, :content],
      methods: [:get_content, :user_name, :published_date, :parent_id])
  end
end
