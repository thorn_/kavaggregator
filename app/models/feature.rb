class Feature < ActiveRecord::Base
  attr_accessible :name, :category_id, :count

  belongs_to :category
end
