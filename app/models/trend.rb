class Trend < ActiveRecord::Base
  has_many :articles
  validates :name, presence: true
  validates :description, presence: true

  attr_accessible :name, :description, :active

  scope :active_trends, where(active: true)
end
