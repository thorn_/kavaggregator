# -*- encoding: utf-8 -*-
class ArticlesController < ApplicationController
  before_filter :update_user_state, only: [:index, :show]

  def index
    @filter = Article.filter_articles(params.merge(order: "created_at DESC"))
    @articles = Article.published.has_sticky(true) + @filter[:articles]
    @page_info = page_info(@filter)

    if trend = Trend.find_by_id(@filter[:trend_id])
      @meta = trend.description
    elsif tag = Tag.find_by_id(@filter[:tag_id])
      @meta = tag.description
    end

    @message = Message.order(:created_at).last

  end

  def show
    @article = Article.find(params[:id])
    @meta = (@article.summary || "Новости на ").truncate(56) + "... Кавигатор - Новости Дагестана"
    @similar_articles = @article.article_group ? @article.article_group.articles.where("id != ?", @article.id) : []
    @other_articles = @article.other_articles
    @comments = @article.comments.order("created_at ASC").page(params[:page]).per_page(15)
    @comment = @article.comments.new
  end

  def update_user_state
    current_user.touch if current_user
  end

end
