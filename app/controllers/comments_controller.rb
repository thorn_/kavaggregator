# -*- encoding: utf-8 -*-
class CommentsController < ApplicationController

  def index
    if params[:article_id]
      whole_count = Comment.where("article_id = ?", params[:article_id]).count
      roots_count = Comment.where("article_id = ?", params[:article_id]).roots.count

      per_page = 15
      @comments = Comment.where("article_id = ?", params[:article_id]).roots.order("created_at ASC").page(params[:page]).per_page(per_page)
      current_page = @comments.current_page
      @comments = prepare_comments(@comments)

      @pagination = { whole_comments: whole_count, total: roots_count, page: current_page, per_page: per_page }
      respond_to do |format|
        format.json { render json: { comments: @comments, pagination: @pagination }}
      end
    else
      respond_to do |format|
        format.json { render json: {}, status: :unprocessable_entity}
      end
    end
  end

  def create
    user_id = current_user.id if current_user
    @comment = Comment.new(params[:comment].merge(user_id: user_id))
    if @comment.article and @comment.save
      begin
        broadcast("/#{@comment.article.id}/comments/new", @comment) unless Rails.env == 'test' || Rails.env == 'development'
      rescue
      end
      respond_to do |format|
        format.html { redirect_to article_path(@comment.article) }
        format.json { render json: @comment, status: :created }
      end
    else
      respond_to do |format|
        format.html { render :new}
        format.json { render json: @comment.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @comment = Comment.find_by_id(params[:id])
    if @comment and @comment.update_column(:deleted, true) and @comment.save
      respond_to do |format|
        format.json { render json: @comment}
        format.html {redirect_to article_path(@comment.article), success: "Комментарий успешно удален"}
      end
    else
      respond_to do |format|
        format.json { render json: {}, status: unprocessable_entity }
        format.html {redirect_to articles_path, error: "Не удалось удалить комментарий"}
      end
    end
  end

  def new
    @comment = Comment.new(parent_id: params[:parent_id], article_id: params[:article_id])
  end

end
