class ApplicationController < ActionController::Base
  protect_from_forgery
  filter_access_to :all unless Rails.env == 'test'
  include ApplicationHelper
  include CommentHelper
  include ArticleHelper
  include PaginationHelper
  before_filter :find_news_sources

  def default_url_options
    if Rails.env.production?
      {:host => "kavigator.ru"}
    else
      {:host => "localhost"}
    end
  end

private

  def find_news_sources
    @news_sources = NewsSource.active.order(:created_at)
    @categories = Category.order(:created_at)
    @trends = Trend.active_trends.order("updated_at DESC").limit(8)
    @admin_trends = Trend.order("active DESC, created_at DESC")
    @tags = Tag.order(:created_at)
  end

end
