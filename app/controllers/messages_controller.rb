# --- encoding: utf-8 ---
class MessagesController < ApplicationController
  def index
    @title = "Новости и объявления"
    @messages = Message.order("created_at DESC").page(params[:page]).per_page(5)
  end
end
