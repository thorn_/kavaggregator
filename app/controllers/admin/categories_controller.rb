# -*- encoding: utf-8 -*-
class Admin::CategoriesController < Admin::BaseController
  before_filter :find_category, only: [:show, :edit, :update, :destroy]

  def new
    @category = Category.new
  end

  def create
    @category = Category.new(params[:category])
    if @category.save
      redirect_to admin_categories_path, flash: {success: "Категория успешно создана."}
    else
      flash.now[:error] = "Категория не создана"
      render :new
    end
  end

  def index
    @categories = Category.all
    @users = User.all
  end

  def show
  end

  def edit
  end

  def update
    if @category.update_attributes(params[:category])
      redirect_to admin_categories_path, flash: {success: "Категория успешно изменена."}
    else
      flash.now[:error] = "Категория не изменена."
      render :edit
    end
  end

  def destroy
    @category.destroy
    redirect_to admin_categories_path, flash: {success: "Категория успешно удалена."}
  end

private
  def find_category
    @category = Category.find(params[:id])
  end
end
