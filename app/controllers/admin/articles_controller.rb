# -*- encoding: utf-8 -*-
class Admin::ArticlesController < Admin::BaseController
  before_filter :update_user_state, only: [:index, :show]

  def index
    @categories = Category.order(:name)
    @articles = Article.not_published.order("created_at DESC")
  end

  def destroy
    if params[:id] == "all"
      Article.not_published.destroy_all
      redirect_to :back, flash: {:success => "Все статьи удалены."}
    else
      @article = Article.find(params[:id])
      @article.destroy
      respond_to do |format|
        format.html { redirect_to :back, flash: {:success => "Статья успешно удалена."} }
        format.js { render nothing: true, status: 200, content_type: "text/javascript"}
      end
    end
  end

  def train
    cat = Category.find_by_id(params[:cat_id])
    article = Article.find_by_id(params[:id])
    if cat && article
      article.train(cat)
      flash = {success: "Статья успешно классифицирована."}
    else
      flash = {error: "Статья не классифицирована."}
    end
    redirect_to admin_articles_path, flash: flash
  end

  def classify
    @article = Article.find(params[:id])
    @cl = FisherClassifier.new
    res = @cl.classify(Nokogiri::HTML(@article.text).text)
    @cat = res[0]
    @prob = res[1]
  end

  def feed_me
    Article.feed_me
    redirect_to admin_articles_path
  end

  def update_user_state
    current_user.touch if current_user
  end

  def new
    @article = Article.new
    @title = "Новая статья"
  end

  def edit
    @article = Article.find(params[:id])
    @title = "Редактирование статьи"
  end

  def create
    @article = Article.new(params[:article])
    @article.article_group_id = ArticleGroup.create.id
    if @article.save
      flash[:success] = "Статья успешно добавлена."
      redirect_to article_path(@article)
    else
      flash[:error] = "Статья не добавлена"
      render :new
    end
  end

  def update
    @article = Article.find(params[:id])
    if @article.update_attributes(params[:article])
      flash[:success] = "Статья успешно изменена."
      respond_to do |format|
        format.html {redirect_to article_path(@article)}
        format.json { render nothing: true, content_type: 'text/javascript'}
      end
    else
      flash[:error] = "Статья не изменена"
      render :edit
    end
  end
end
