#-*- encoding: utf-8 -*-
class Admin::PublishedArticlesController < Admin::BaseController
  respond_to :json
  def index
    filter = Article.filter_articles(params.merge(per_page: 10, order: "created_at DESC"))
    @articles = filter[:articles]
    @category = Category.find_by_id(filter[:cat_id])
    @source = NewsSource.find_by_id(filter[:source_id].first)
    @categories = Category.all
    @trends = Trend.order("active DESC, created_at DESC")
  end

  def group
    ArticleGroup.create(params[:article_group])
    redirect_to :back
  end

  def ungroup
    @article = Article.find(params[:id])
    @article.update_attributes(article_group_id: ArticleGroup.create.id)
    redirect_to :back
  rescue ActionController::RedirectBackError
    redirect_to admin_published_articles_path
  end

  def trend
    Article.find(params[:id]).update_attributes(trend_id: params[:trend_id])
    render nothing: true, status: 200
  end
end
