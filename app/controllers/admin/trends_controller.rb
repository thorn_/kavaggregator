# -*- encoding: utf-8 -*-
class Admin::TrendsController < Admin::BaseController
  before_filter :find_trend, only: [:show, :edit, :update, :destroy]

  def new
    @trend = Trend.new
  end

  def create
    @trend = Trend.new(params[:trend])
    if @trend.save
      redirect_to admin_trends_path, flash: {success: "Тренд успешно создан."}
    else
      flash.now[:error] = "Тренд не создан"
      render :new
    end
  end

  def index
    @trends = Trend.order("active DESC, created_at DESC")
  end

  def show
  end

  def edit
  end

  def update
    if @trend.update_attributes(params[:trend])
      redirect_to admin_trends_path, flash: {success: "Тренд успешно изменен."}
    else
      flash.now[:error] = "Тренд не изменен."
      render :edit
    end
  end

  def destroy
    @trend.destroy
    redirect_to admin_trends_path, flash: {success: "Тренд успешно удален."}
  end

private
  def find_trend
    @trend = Trend.find(params[:id])
  end
end
