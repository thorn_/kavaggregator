# -*- encoding: utf-8 -*-
class Admin::CommentsController < Admin::BaseController

  def index
    @comments = Comment.scoped.page(params[:page]).per_page(100)
    @total = Comment.count
  end

  def destroy
    @comment = Comment.find_by_id(params[:id])
    if @comment and @comment.deleted = true and @comment.save
      respond_to do |format|
        format.json { render json: @comment}
        format.html {redirect_to admin_comments_path, success: "Комментарий успешно удален"}
      end
    else
      respond_to do |format|
        format.json { render json: {}, status: :unprocessable_entity }
        format.html {redirect_to admin_comments_path, error: "Не удалось удалить комментарий"}
      end
    end
  end
end
