class Admin::MessagesController < Admin::BaseController
  before_filter :find_message, only: [:edit, :update, :destroy]

  def index
    @messages = Message.order("created_at DESC")
  end

  def new
    @message = Message.new
  end

  def edit
  end

  def create
    @message = Message.new(params[:message])
    if @message.save
      redirect_to admin_messages_path
    else
      render :new
    end
  end

  def update
    if @message.update_attributes(params[:message])
      redirect_to admin_messages_path
    else
      render :new
    end
  end

  def destroy
    Message.destroy(params[:id])
    redirect_to admin_messages_path
  end

private

  def find_message
    @message = Message.find(params[:id])
  end

end
