# -*- encoding: utf-8 -*-
class Admin::UsersController < Admin::BaseController

  before_filter :find_user, only: [:edit, :update, :destroy]

  def index
    @users = User.all
  end

  def destroy
    @user.destroy
    redirect_to admin_users_path, flash: {success: "Пользователь успешно удален."}
  end

  def edit
  end

  def update
    @user.role = params[:user][:role]
    @user.save
    redirect_to admin_users_path, flash: { success: "Роль пользователя успешно изменена." }
  end

  def find_user
    @user = User.find(params[:id])
  end
end
