# -*- encoding: utf-8 -*-
class Admin::NewsSourcesController < Admin::BaseController

  before_filter :find_source, only: [:show, :clear, :destroy, :edit, :update]

  def index
    @news_sources = NewsSource.all
  end

  def new
    @news_source = NewsSource.new
  end

  def create
    @news_source = NewsSource.new(params[:news_source])
    if @news_source.save
      redirect_to admin_news_sources_path, flash: {success: "Источник новостей успешно создан."}
    else
      flash.now[:error] = "Источник новостей не добавлен."
      render :new
    end
  end

  def show
  end

  def edit
  end

  def update
    if @news_source.update_attributes(params[:news_source])
      redirect_to admin_news_sources_path, flash: {success: "Источник новостей успешно изменен."}
    else
      flash.now[:error] = "Источник новостей не изменен."
      render :edit
    end
  end

  def clear
    @news_source.clear
    redirect_to admin_news_sources_path, flash: {success: "Статьи успешно удалены."}
  end

  def destroy
    @news_source.destroy
    redirect_to admin_news_sources_path, flash: {success: "Источник новостей успешно удален."}
  end

private
  def find_source
    @news_source = NewsSource.find(params[:id])
  end
end
