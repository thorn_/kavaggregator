# -*- encoding: utf-8 -*-
class Admin::TagsController < Admin::BaseController
  before_filter :find_tag, only: [:show, :edit, :update, :destroy]

  def new
    @tag = Tag.new
  end

  def create
    @tag = Tag.new(params[:tag])
    if @tag.save
      redirect_to admin_tags_path, flash: {success: "Так успешно создан."}
    else
      flash.now[:error] = "Так не создан"
      render :new
    end
  end

  def index
    @tags = Tag.all
    @users = User.all
  end

  def show
  end

  def edit
  end

  def update
    if @tag.update_attributes(params[:tag])
      redirect_to admin_tags_path, flash: {success: "Так успешно изменен."}
    else
      flash.now[:error] = "Так не изменен."
      render :edit
    end
  end

  def destroy
    @tag.destroy
    redirect_to admin_tags_path, flash: {success: "Так успешно удален."}
  end

private
  def find_tag
    @tag = Tag.find(params[:id])
  end
end
