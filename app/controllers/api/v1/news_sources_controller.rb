class Api::V1::NewsSourcesController < ApplicationController
  def index
    @news_sources = NewsSource.active
  end
end
