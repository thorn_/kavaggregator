require "curb"
class Api::V1::ArticlesController < ApplicationController
  def index
    @filter = Article.filter_articles(params)
    @articles = @filter[:articles]
  end

  def share
    article = Article.find(params[:id])
    article.shared = true
    article.save
    short_text = params[:short_text] || ''
    text =       params[:text] || ''
    url =        params[:url] || ''
    c = Curl::Easy.http_post(
      "http://91.205.130.20/api/v1/message_sets",
      Curl::PostField.content('api_key', ENV['multiposting_key']),
      Curl::PostField.content('message[short_text]', short_text),
      Curl::PostField.content('message[text]',       text),
      Curl::PostField.content('message[url]',        url)
    ) unless Rails.env == 'test'
    render nothing: true
  end
end
