class FeedbacksController < ApplicationController
  def send_feedback
    FeedbackMailer.feedback(params).deliver
    render nothing: true
  end
end
