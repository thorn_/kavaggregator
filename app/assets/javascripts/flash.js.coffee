$.fn.make_flash = ->
  @each ->
    $(this).hide()
    classes = {"alert": "warning", "success": "success", "notice": "info", "error": "error"}
    $('p', this).each (el) ->
      klass = $(this).attr('class')
      message_type = classes[klass]
      text = $(this).text()
      toastr[message_type](text)
