$.fn.action_button = (form_id, trigger_count = 1) ->
  @each ->
    $(this).on 'click', ->
      elements = $("#model_ids_#{$(this).attr("id")}_:checked").length
      a = $("input[id=model_ids_#{$(this).attr('id')}_]:checked:visible").map (i, el) -> return $(el).val()

      if a.length >= trigger_count
        checked_values = $.makeArray(a).join(',')
        $(form_id).find("input[id*=model_ids]").val(checked_values)
        $(form_id).submit()
      else
        false
