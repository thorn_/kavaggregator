#= require jquery_ujs
#= require bootstrap-dropdown
#= require bootstrap-alert
#= require bootstrap-tab
#= require chosen-jquery
#= require toastr
#= require flash
#= require bPopup
#= require action_button
#= require_self

$ ->
  $('.choosen_select').chosen
    allow_single_deselect: true
    no_results_text: 'No results matched'
    width: '200px'
    placeholder_text: '\0'
    placeholder_text_multiple: '\0'
    display_selected_options: false

  window.resize_navigation = (elements, whole_width) ->
    element_count = $(elements).length
    sum = 0
    $(elements).each (index, element) ->
      sum += $(element).width()
    space_to_allocate = whole_width - sum;
    width_to_each_element = Math.floor(space_to_allocate/(element_count))
    $(elements).each (index, element) ->
      if (index isnt 0)
        $(element).width($(element).width() + width_to_each_element)

  window.resize_navigation($('.navigation_local.span18>li'), $('.navigation_local.span18').first().width()) if screen.width > 780

  $('.events_not_ready').on 'click', ->
    toastr.info("UNDER CONSTRUCTION")
    false

  $(".reset_articles").on 'click', ->
    window.router.articles.reset_collection()
    $('.news_source').removeClass('active')
    $('.news_source').first().addClass('active')
    $('.category').first().click()

  if window.Faye
    window.faye = new Faye.Client 'http://91.205.130.20:9292/faye'
    window.faye.disable 'autodisconnect'

  $('.action_button').action_button("#new_article_group")

  $('body').on 'click', '.delete_article',->
    $(this).parents('*[id^=article_]').first().remove()

  $('.search_add_on').on 'click', ->
    $(this).parents('form').first().submit()

  # Spinner
  $("body").ajaxStart ->
    $(".ajax_loader").slideDown(100)
  $("body").ajaxStop ->
    $(".ajax_loader").slideUp(100)


  # infinitie scrolling
  $(window).scroll ->
    url = $('.pagination .next_page').attr('href')
    if url and $(window).scrollTop() > $(document).height() - $(window).height() - 50
      $('.pagination').html('<h3>Загружаем новости...</h3>')
      $.getScript(url)

  # Trending
  $("body").on 'change', '#article_trend_id, #article_tag_ids, #article_sticky', (ev)->
    article_id = parseInt($(this).parents('*[id^=article_]').attr('id').split('_')[1])
    if $(ev.target).attr('type') is 'checkbox'
      value = if $(ev.target).attr('checked') is 'checked' then 'true' else 'false'
    else
      value = $(ev.target).val()
    input_name = 'article[' + $(ev.target).attr('id').split('article_')[1] + ']'
    data = {}
    data[input_name] = value
    $.ajax
      url: "/admin/articles/#{article_id}"
      type: 'PUT'
      data: data
      success: (result) ->
        console.log(result)

      error: (result) ->
        console.log(result)
    false

  # fix for tablets not showing dropdown on touch
  $('a.dropdown-toggle, .dropdown-menu a, .btn-group').on 'touchstart', (e) ->
    e.stopPropagation()

  # Sharing
  $('body').on 'click', '.share_button', (e) ->
    e.preventDefault()
    article = $(this).parents('*[id^=article_]')
    article_id = parseInt(article.attr('id').split('_')[1])
    $("#shared_popup_#{article_id}").bPopup({opacity: 0})

  $('body').on 'click', '.submit_share', (e) ->
    article = $(this).parents('*[id^=shared_popup_]')
    article_id = parseInt(article.attr('id').split('_')[2])
    $("#shared_popup_#{article_id} .b-close").trigger('click')
    short_text = $("#short_text_#{article_id}").val()
    text = $("#text_#{article_id}").val()
    url = $("#url_#{article_id}").val()
    $.post(
      "/api/v1/articles/#{article_id}/share",
      {
        'short_text': short_text,
        'text': text,
        'url': url
      }
    )
    $("#article_#{article_id} .share_button").first().parents().first().addClass('label info')

  $('body').on 'click', '.feedback_button', (e) ->
    e.preventDefault()
    $("#feedback_popup").bPopup({opacity: 0})

  $('body').on 'click', '.feedback_submit', (e) ->
    contacts = $("#feedback_contacts").val()
    text = $("#feedback_text").val()
    if text.replace(/^\s+|\s+$/g, '').length > 0
      $.post(
        "/feedbacks",
        {
          'contacts': contacts,
          'text': text
        }
      )
      text = $("#feedback_text").val('')
      $("#feedback_popup").bPopup().close()
      toastr.info("Спасибо за отзыв!")
    else
      toastr.info("Напишите хоть что-нибудь!")

  $('body').on 'click', '.news_source.all', (ev) ->
    $('.sources_list').slideToggle()
    return false

  $('body').on 'click', '.category.all', (ev) ->
    $('.categories_list').slideToggle()
    return false

