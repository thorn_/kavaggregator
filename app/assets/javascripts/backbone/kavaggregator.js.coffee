#= require_self
#= require_tree ./services
#= require_tree ./templates
#= require_tree ./models
#= require_tree ./views
#= require_tree ./routers

window.Kavaggregator =
  Models: {}
  Collections: {}
  Routers: {}
  Views: {}
  Functions: {}
  Variables: {}
  Vent: _.extend {}, Backbone.Events

window.Kavaggregator.Functions.scroll_to_top = (selector = $('.logo'), time = 300) ->
  destination = jQuery(selector).offset().top
  jQuery('html:not(:animated),body:not(:animated)').animate({scrollTop: destination}, time)

window.Kavaggregator.Functions.raise_error = (model, response) ->
  if response.responseText is "{\"content\":[\"\\u043d\\u0435 \\u043c\\u043e\\u0436\\u0435\\u0442 \\u0431\\u044b\\u0442\\u044c \\u043f\\u0443\\u0441\\u0442\\u044b\\u043c\"]}"
    text = 'Комментарий не может быть пустым'
  else if response.responseText is "You are not allowed to access this action." or "{\"user_id\":[\"\\u043d\\u0435 \\u043c\\u043e\\u0436\\u0435\\u0442 \\u0431\\u044b\\u0442\\u044c \\u043f\\u0443\\u0441\\u0442\\u044b\\u043c\"],\"content\":[\"\\u043d\\u0435 \\u043c\\u043e\\u0436\\u0435\\u0442 \\u0431\\u044b\\u0442\\u044c \\u043f\\u0443\\u0441\\u0442\\u044b\\u043c\"]}"
    text = '<a href="/users/login">Войдите</a> или <a href="/users/sign_up">зарегистируйтесь</a> чтобы создавать комментарии.'
  else
    text = response.responseText
  toastr.info(text)

window.Kavaggregator.Functions.notify = (msg, type = "info") ->
  messages =
    'info': toastr.info
    'error': toastr.error
    'warning': toastr.warning
  messages[type].call this, msg

window.Kavaggregator.Functions.set_title = (title) ->
  $('title').html(title)
