Kavaggregator.Views.Categories ||= {}
class Kavaggregator.Views.Categories.CategoryView extends Backbone.View
  template: JST["backbone/templates/categories/category"]

  tagName: "li"

  events:
    'click .category' : 'set_category'

  render: =>
    $(@el).html(@template(@model.toJSON()))
    return this

  set_category: =>
    window.Kavaggregator.Vent.trigger 'category/set', @model
