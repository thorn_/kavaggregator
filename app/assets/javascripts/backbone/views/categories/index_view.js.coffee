Kavaggregator.Views.Categories ||= {}
class Kavaggregator.Views.Categories.IndexView extends Backbone.View

  template: JST["backbone/templates/categories/index"]
  tagName: "ul"
  className: "navigation_local span17"

  events:
    'click .all_categories' : 'all_categories'

  initialize: (options) ->
    @collection.off 'reset'
    @collection.on 'reset', @render

  render: =>
    $(@el).html(@template(active_category: @get_active_category()))
    @collection.each @add_one
    window.resize_navigation($('.navigation_local.span17 li'), $('.navigation_local.span17').first().width())

    return this

  add_one: (model) =>
    view = new Kavaggregator.Views.Categories.CategoryView({model: model})
    $(@el).append(view.render().el)

  all_categories: =>
    window.Kavaggregator.Vent.trigger 'category/set', {id: -1}
    window.Kavaggregator.Variables.index_top = null

  get_active_category: =>
    @collection.find (el) -> return el.get('active') == true
