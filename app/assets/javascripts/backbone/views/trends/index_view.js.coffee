Kavaggregator.Views.Trends ||= {}
class Kavaggregator.Views.Trends.IndexView extends Backbone.View

  template: JST["backbone/templates/trends/index"]
  tagName: "div"
  className: "span19 trend_list"

  initialize: (options) ->
    @collection.off 'reset'
    @collection.on 'reset', @render

  render: =>
    $(@el).html(@template())
    @collection.each @add_one
    return this

  add_one: (model) =>
    view = new Kavaggregator.Views.Trends.TrendView({model: model})
    $(@el).append(view.render().el)
