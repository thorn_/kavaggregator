Kavaggregator.Views.Trends ||= {}
class Kavaggregator.Views.Trends.TrendView extends Backbone.View
  template: JST["backbone/templates/trends/trend"]

  tagName: "div"
  className: "trend"

  events:
    'click .trend' : 'set_trend'

  render: =>
    $(@el).html(@template(@model.toJSON()))
    return this

  set_trend: =>
    window.Kavaggregator.Vent.trigger 'trend/set', @model
    window.Kavaggregator.Vent.trigger 'category/reset', {id: -1}
    window.Kavaggregator.Vent.trigger 'source/reset', {id: -1}
