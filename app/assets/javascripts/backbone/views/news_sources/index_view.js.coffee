Kavaggregator.Views.NewsSources ||= {}
class Kavaggregator.Views.NewsSources.IndexView extends Backbone.View

  template: JST["backbone/templates/news_sources/index"]
  tagName: 'ul'
  className: "sources_list unstyled"

  events:
    'click .all_sources' : 'all_sources'

  initialize: (options) ->
    @collection.off 'reset'
    @collection.on 'reset', @render

  render: =>
    $(@el).html(@template(active_source: @get_active_source()))

    @collection.each @add_one
    return this

  add_one: (model) =>
    view = new Kavaggregator.Views.NewsSources.NewsSourceView({model: model})
    $(@el).find('.sources_list.unstyled').append(view.render().el)

  all_sources: =>
    window.Kavaggregator.Variables.index_top = null
    window.Kavaggregator.Vent.trigger 'source/set', {id: -1}

  get_active_source: =>
    @collection.find (el) -> return el.get('active') == true
