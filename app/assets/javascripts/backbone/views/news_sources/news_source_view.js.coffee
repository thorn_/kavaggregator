Kavaggregator.Views.NewsSources ||= {}
class Kavaggregator.Views.NewsSources.NewsSourceView extends Backbone.View
  template: JST["backbone/templates/news_sources/news_source"]

  tagName: "li"

  events:
    'click .news_source' : 'set_source'

  render: =>
    $(@el).html(@template(@model.toJSON()))
    return this

  set_source: =>
    # window.Kavaggregator.Functions.scroll_to_top()
    window.Kavaggregator.Vent.trigger 'source/set', @model
