Kavaggregator.Views.Comments ||= {}

class Kavaggregator.Views.Comments.CommentView extends Backbone.View
  template: JST['backbone/templates/comments/comment']

  events:
    'click .destroy'           : 'destroy'
    'click .reply'             : 'reply'
    'click .register_to_reply' : 'show_register_alert'

  destroy: =>
    $(@el).find('.comment_content').first().html('<div class="comment_deleted">Комментарий был удален</div>')
    @model.destroy()

    return false

  render: =>
    $(@el).html(@template(model: @model))
    return this

  reply: (ev) =>
    new_view = new Kavaggregator.Views.Comments.CommentsNewView({collection: @collection, parent_id: @model.id, article: @article})
    $('.new_reply').html('')
    $(@el).find('.new_reply').first().html(new_view.render().el)

    return false

  show_register_alert: =>
    Kavaggregator.Functions.raise_error(null, {responseText: 'You are not allowed to access this action.'})
    return false
