Kavaggregator.Views.Comments ||= {}

class Kavaggregator.Views.Comments.CommentsNewView extends Backbone.View
  template: JST['backbone/templates/comments/new']

  className: 'new_comment'

  events:
    'click .submit_reply'   : 'reply'
    'click .cancel_reply'   : 'destroy'

  initialize: (options) ->
    @parent_id = options.parent_id

  render: =>
    $(@el).html(@template())
    $(@el).find('textarea').focus()
    return this

  reply: (ev) =>
    content = $(@el).find('textarea').first().val()

    model = new Kavaggregator.Models.Comment()
    model.save({content: content, article_id: @collection.article.id, parent_id: @parent_id},
      { success: @add_comment, error: Kavaggregator.Functions.raise_error})

    if $(@el).parent().attr('class') is 'comments span17'
      $(@el).find('textarea').text('')
    else
      @destroy()

    return false

  destroy: =>
    this.remove()

  add_comment: (model, data) =>
    @collection.inc_total(model)
    @collection.article.set('comments_count', parseInt(@collection.pageInfo().total))
    if @collection.pageInfo().page is @collection.pageInfo().pages or $(@el).parent().attr('class') isnt 'comments span17'
      @collection.add(model)
    else
      @collection.gotoPage(@collection.pageInfo().pages)
