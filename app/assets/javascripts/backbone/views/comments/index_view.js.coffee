Kavaggregator.Views.Comments ||= {}

class Kavaggregator.Views.Comments.IndexView extends Backbone.View

  template:  JST['backbone/templates/comments/index']
  className: 'comments span17'

  events:
    'click a.previous_page' : 'previous'
    'click a.next_page'     : 'next'
    'click a.set_page'      : 'goto_page'

  initialize: (options) ->
    @collection.on  'reset', @render
    @collection.on  'add', @add_comment
    @collection.gotoPage parseInt(@collection.article.get('comment_page')) if @collection.article.get('comment_page')
    if window.faye
      window.subscription = faye.subscribe "/#{@collection.article.id}/comments/new", @comment_from_server

  add_comment: (comment) =>
    if comment.get('user_id') is current_user
      @render()

  comment_from_server: (data) =>
    if data.user_id isnt current_user
      comment = new Kavaggregator.Models.Comment data
      @collection.inc_total(comment)
      Kavaggregator.Functions.notify('Есть новые комментарии')
      @collection.add(comment)
      @addOne(comment) if comment.parent_id or @collection.pageInfo().page is @collection.pageInfo().pages
      @will_paginate()
      $(@el).find('.comments_total').text(@collection.pageInfo().total)

  addAll: () =>
    self = this
    @collection.each (model) ->
      self.addOne(model) if model.get('parent_id') is null
    @collection.each (model) ->
      self.addOne(model) if model.get('parent_id') isnt null

  addOne: (comment) =>
    view = new Kavaggregator.Views.Comments.CommentView({collection: @collection, model : comment})
    if comment.get('parent_id')
      $(@el).find("#comment_#{comment.get('parent_id')} .replies").first().append(view.render().el)
    else
      $(@el).find('.all_comments').before(view.render().el)

  render: =>
    $(@el).html(@template(@collection))
    new_view = new Kavaggregator.Views.Comments.CommentsNewView({collection: @collection})
    $(@el).find('.all_comments').first().before(new_view.render().el)
    @addAll()
    @will_paginate()

    return this

  will_paginate: =>
    if @collection.pageInfo().pages > 1
      $(@el).find('.will_paginate').html(JST['backbone/templates/articles/pagination'](page_info: @collection.pageInfo()))

  previous: =>
    @collection.previousPage()
    window.Kavaggregator.Functions.scroll_to_top($('.submit_reply'))
    return false

  next: =>
    @collection.nextPage()
    window.Kavaggregator.Functions.scroll_to_top()
    return false

  goto_page: (ev) =>
    window.Kavaggregator.Functions.scroll_to_top($('.submit_reply'))
    @collection.gotoPage parseInt($(ev.target).text())
    return false
