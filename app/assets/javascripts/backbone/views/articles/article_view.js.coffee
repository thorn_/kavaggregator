Kavaggregator.Views.Articles ||= {}

class Kavaggregator.Views.Articles.ArticleView extends Backbone.View
  template: JST['backbone/templates/articles/article'], article: @model

  events:
    'click .show_similar': 'show_similar'
    'click .category'    : 'set_category'
    'click .source'      : 'set_source'

  tagName: 'li'
  className: 'news'

  show_similar: =>
    $(@el).find('.similar_articles').slideToggle()
    false

  render: =>
    $(@el).html(@template(@model.toJSON() ))
    return this

  set_category: (ev) =>
    id = $(ev.target).data("cat_id")
    $('.category').removeClass('active')
    $(".category[data-cat_id=#{$(ev.target).data("cat_id")}]").addClass('active')
    @model.collection.set_category(id)
    false

  set_source: (ev) =>
    id = $(ev.target).data("source_id")
    $('.news_source').removeClass('active')
    $(".news_source[data-source_id=#{$(ev.target).data("source_id")}]").addClass('active')
    @model.collection.set_source($(ev.target).data("source_id"))
    false
