Kavaggregator.Views.Articles ||= {}

class Kavaggregator.Views.Articles.ShowView extends Backbone.View
  template: JST["backbone/templates/articles/show"]

  className: "news_container"

  render: ->
    $(@el).html(@template(@model.toJSON()))
    @comments = new Kavaggregator.Collections.CommentsCollection({article: @model, pagination: {}})
    @comments.fetch({success: (collection, data)->
      collection.init_pagination(data) # cause we know about total count only after fetching
      comments_view = new Kavaggregator.Views.Comments.IndexView({collection: collection})
      $("#comments").html(comments_view.render().el)
    })

    return this
