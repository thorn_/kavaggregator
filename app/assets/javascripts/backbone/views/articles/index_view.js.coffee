Kavaggregator.Views.Articles ||= {}

class Kavaggregator.Views.Articles.IndexView extends Backbone.View
  template: JST['backbone/templates/articles/index']

  tagName: 'ul'
  className: 'news_container span17'

  events:
    'click .previous_page, .next_page, .set_page' : 'scroll_to_top'

  initialize: () ->
    @collection.on 'reset', @render

  addOne: (article) =>
    view = new Kavaggregator.Views.Articles.ArticleView({model : article})
    $(@el).append(view.render().el)

  render: =>
    $(@el).html('')
    $(@el).append(@template(articles: @collection.toJSON() ))
    @collection.each(@addOne)
    @will_paginate()
    window.subscription.cancel() if window.subscription
    return this

  will_paginate: =>
    if @collection.pageInfo().pages > 1
      $(@el).append(JST['backbone/templates/articles/pagination'](page_info: @collection.pageInfo()))

  scroll_to_top: =>
    window.Kavaggregator.Functions.scroll_to_top()
