class Kavaggregator.Models.Article extends Backbone.Model
  urlRoot: '/articles'

  defaults:
    title: null
    text: null
    url: null
    category_id: null

class Kavaggregator.Collections.ArticlesCollection extends PaginatedCollection
  model: Kavaggregator.Models.Article

  initialize: (options) =>
    @baseUrl = '/articles'
    @set_current_state(options)
    window.Kavaggregator.Vent.on('trend/set', @set_trend, this)
    window.Kavaggregator.Vent.on('category/set', @set_category, this)
    window.Kavaggregator.Vent.on('source/set', @set_source, this)
    super(options)

  set_current_state: (options) =>
    @cat_id    = options.current_state.cat_id
    @source_id = options.current_state.source_id
    @trend_id  = options.current_state.trend_id

  parse: (resp) =>
    super(resp)
    @set_current_state(resp)
    return resp.articles

  url: =>
    @baseUrl + '?' + $.param({page: @page, perPage: @perPage, cat_id: @cat_id, source_id: @source_id, trend_id: @trend_id})

  set_source: (source) =>
    @page = 1
    @source_id = source.id
    @set_trend(-1)
    return @fetch(reset: true)

  set_category: (category) =>
    @page = 1
    @cat_id = category.id
    @set_trend(-1)
    return @fetch(reset: true)

  set_trend: (trend) =>
    @page = 1
    @trend_id = trend.id
    return @fetch(reset: true)

  reset_collection: =>
    @set_source(-1)
    @set_category(-1)
    @set_trend(-1)
    super()
