class Kavaggregator.Models.NewsSource extends Backbone.Model

class Kavaggregator.Collections.NewsSourcesCollection extends Backbone.Collection
  model: Kavaggregator.Models.NewsSource

  initialize: (options) ->
    window.Kavaggregator.Vent.on 'source/set', @set_active, this
    window.Kavaggregator.Vent.on 'source/reset', @set_active, this

  parse: (resp) =>
    return resp.news_sources

  set_active: (model) =>
    for source in @models
      if source.id == model.id
        source.set('active', true)
      else
        source.set('active', false)
    @trigger 'reset'
