class Kavaggregator.Models.Comment extends Backbone.Model
  urlRoot: '/comments'

  toJSON: ->
    comment:
      content: this.get('content')
      get_content: this.get('get_conent')
      id: this.get('id')
      parent_id: this.get('parent_id')
      published_date: this.get('published_date')
      user_name: this.get('user_name')
      article_id: this.get('article_id')

class Kavaggregator.Collections.CommentsCollection extends PaginatedCollection
  model: Kavaggregator.Models.Comment

  initialize: (options) =>
    @baseUrl = '/comments'
    @article = options.article
    super(options)

  set_current_status: (options) =>
    @whole_comments     = options.pagination.whole_comments

  parse: (resp) =>
    @set_current_status(resp)
    return resp.comments

  url: =>
    @baseUrl + '?' + $.param({page: @page, perPage: @perPage, article_id: @article.id})

  roots: =>
    (model for model in @models when model.get('parent_id') is null)

  inc_total: (comment) =>
    @article.set 'comments_count', parseInt(@article.get('comments_count')) + 1
    @total += 1 if comment.get('parent_id') is null
    @whole_comments += 1
