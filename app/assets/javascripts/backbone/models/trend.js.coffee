class Kavaggregator.Models.Trend extends Backbone.Model

class Kavaggregator.Collections.TrendsCollection extends Backbone.Collection
  model: Kavaggregator.Models.Trend

  initialize: (args) ->
    window.Kavaggregator.Vent.on 'trend/set', @set_active, this
    window.Kavaggregator.Vent.on 'category/set', @reset_trends, this
    window.Kavaggregator.Vent.on 'source/set', @reset_trends, this
  parse: (resp) =>
    return resp.trends

  set_active: (model) =>
    for trend in @models
      if trend.id == model.id
        trend.set('active', true)
      else
        trend.set('active', false)
    @trigger 'reset'

  reset_trends: (data) =>
    @set_active({id: -1})
