class Kavaggregator.Models.Category extends Backbone.Model

class Kavaggregator.Collections.CategoriesCollection extends Backbone.Collection
  model: Kavaggregator.Models.Category

  initialize: (options) ->
    window.Kavaggregator.Vent.on 'category/set', @set_active, this
    window.Kavaggregator.Vent.on 'category/reset', @set_active, this

  parse: (resp) =>
    return resp.categories

  set_active: (model) =>
    for category in @models
      if category.id == model.id
        category.set('active', true)
      else
        category.set('active', false)
    @trigger 'reset'
