class @PaginatedCollection extends Backbone.Collection

  initialize: (options) =>
    @init_pagination(options)
    typeof(options) != 'undefined' || (options = {})
    @page = 1
    typeof(@perPage) != 'undefined' || (@perPage = 15)

  init_pagination: (options) =>
    @page      = options.pagination.page
    @perPage   = options.pagination.per_page
    @total     = options.pagination.total

  parse: (options) =>
    @init_pagination(options)

  pageInfo: =>
    info =
      total: @total
      page: @page
      perPage: @perPage
      pages: @total_pages()
      prev: false
      next: false
      pagination: @windowed_page_number()

    max = Math.min(@total, @page * @perPage)
    max = @total if @total is @pages * @perPage

    info.range = [(@page - 1) * @perPage + 1, max]
    info.prev = @page - 1 if @page > 1
    info.next = @page + 1 if @page < info.pages
    info

  total_pages: =>
    Math.ceil(@total / @perPage)

  nextPage: =>
    return false unless @pageInfo().next
    @page = @page + 1
    return @fetch(reset: true)

  previousPage: =>
    return false unless @pageInfo().prev
    @page = @page - 1
    return @fetch(reset: true)

  gotoPage: (page) =>
    @page = page
    return @fetch(reset: true)

  reset_collection: =>
    @page =  1

  windowed_page_number: =>
    inner_window = 3
    outer_window = 1
    total_pages = @total_pages()
    window_from = @page - inner_window
    window_to   = parseInt(@page) + inner_window

    if window_to > total_pages
      window_from -= window_to - total_pages
      window_to    = total_pages
    if window_from < 1
      window_to  += 1 - window_from
      window_from = 1
      window_to   = total_pages if window_to > total_pages

    middle = [window_from..window_to]

    if outer_window + 3 < middle[0]
      left = [1..(outer_window + 1)]
      left.push "..."
    else
      left = [1...middle[0]]

    if (total_pages - outer_window - 2) > middle[middle.length - 1]
      right = [(total_pages - outer_window)..total_pages]
      right.unshift "..."
    else
      right_start = Math.min(middle[middle.length - 1] + 1, total_pages)
      right = [right_start..total_pages]
      right = [] if right_start is total_pages

    return left.concat(middle.concat(right))
