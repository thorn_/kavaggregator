# -*- encoding: utf-8 -*-
class Kavaggregator.Routers.ArticlesRouter extends Backbone.Router
  initialize: (options) ->
    @bind 'all', @_trackPageview

    @articles     = new Kavaggregator.Collections.ArticlesCollection(options)
    @news_sources = new Kavaggregator.Collections.NewsSourcesCollection(options)
    @categories   = new Kavaggregator.Collections.CategoriesCollection(options)
    @trends       = new Kavaggregator.Collections.TrendsCollection(options)
    @articles.reset     options.articles
    @news_sources.reset options.news_sources
    @categories.reset   options.categories
    @trends.reset       options.trends
    window.current_user = options.current_user


  routes:
    "index"          : "index"
    "page/:id"       : "index"
    ":id"            : "show"
    ":id/page/:page" : "show"
    ".*"             : "index"

  _trackPageview: ->
    url = Backbone.history.getFragment()
    _gaq.push(['_trackPageview', "/#{url}"]) if _gaq?

  index: (id) ->
    @articles.gotoPage(id) if (id)
    @article_index_view = new Kavaggregator.Views.Articles.IndexView   (collection: @articles)
    @news_sources_view  = new Kavaggregator.Views.NewsSources.IndexView(collection: @news_sources)
    @categories_view    = new Kavaggregator.Views.Categories.IndexView (collection: @categories)
    @trends_view        = new Kavaggregator.Views.Trends.IndexView     (collection: @trends)
    $(".content.row").html(@article_index_view.render().el)
    $('.sources_main').html(@news_sources_view.render().el)
    $(".categories.row").html(@categories_view.render().el)
    $(".trends.row").html(@trends_view.render().el)
    window.Kavaggregator.Functions.set_title('Новости Дагестана | Кавигатор')
    window.resize_navigation($('.navigation_local.span17 li'), $('.navigation_local.span17').first().width())
    naow = window.Kavaggregator.Variables.index_top
    $(window).scrollTop(naow) if naow

    $('.back_block').hide()

  show: (id, comment_page) ->
    window.Kavaggregator.Variables.index_top = $(window).scrollTop()
    window.Kavaggregator.Functions.scroll_to_top()
    article = @articles.get(id)
    com_page = comment_page
    if article
      if com_page is "1"
        article.unset('comment_page')
      else if com_page
        article.set('comment_page', com_page)
      @view = new Kavaggregator.Views.Articles.ShowView(model: article)
      $(".content.row").html(@view.render().el)
      if window.FB?
        window.FB.XFBML.parse $('#facebook_comments').get(0), -> $(".FB_Loader").remove()

      window.Kavaggregator.Functions.set_title(article.get('title') + ' | Новости Дагестана | Кавигатор')

      $('.back_block').show()

    else
      article = new Kavaggregator.Models.Article({id: id, comment_page: comment_page})
      article.fetch({success: ->
        if com_page is "1"
          article.unset('comment_page')
        else if com_page
          article.set('comment_page', com_page)
        view = new Kavaggregator.Views.Articles.ShowView(model: article)
        $(".content.row").html(view.render().el)
        if window.FB?
          window.FB.XFBML.parse $('#facebook_comments').get(0), -> $(".FB_Loader").remove()

        $('title').html(article.get('title') + ' | Kavigator - Новости Дагестана')

        $('.back_block').show()
      })
