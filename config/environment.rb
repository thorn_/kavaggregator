# -*- encoding: utf-8 -*-
# Load the rails application
require File.expand_path('../application', __FILE__)

# Load sensitive data
app_environment_variables = File.join(Rails.root, 'config', 'environment_variables.rb')
load(app_environment_variables) if File.exists?(app_environment_variables)

# Initialize the rails application
Kavaggregator::Application.initialize!
Time::DATE_FORMATS[:ru_datetime] = "%Y.%m.%d"
# Time::DATE_FORMATS[:ru_datetime] = "%Y.%m.%d в %k:%M:%S"
