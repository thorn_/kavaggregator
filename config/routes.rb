Kavaggregator::Application.routes.draw do

  devise_for :users, path_names: {sign_in: "login", sign_out: "logout"},
                     controllers: {omniauth_callbacks: "omniauth_callbacks"}

  resources :articles, only: [:index, :show]
  resources :comments, only: [:index, :create, :destroy, :new]
  resources :messages, only: [:index]
  match "feedbacks" => "feedbacks#send_feedback"

  namespace :admin do
    resources :published_articles, only: [:index] do
      collection do
        match :group
      end
      member do
        match :trend
        match :ungroup
      end
    end
    root :to => "articles#index"
    resources :news_sources do
      member do
        match :clear
      end
    end
    resources :keywords
    resources :categories
    resources :tags
    resources :trends
    resources :users
    resources :comments, only: [:index, :destroy, :show]
    resources :articles do
      member do
        match :train
        match :classify
      end
      collection do
        match :feed_me
      end
    end
    resources :messages
  end

  root to: "articles#index"

  # API
  namespace :api, defaults: { format: 'json' } do
    namespace :v1 do
      resources :articles do
        member do
          post :share
          get :share
        end
      end
      resources :trends,       only: [:index]
      resources :categories,   only: [:index]
      resources :news_sources, only: [:index]
    end
  end

end
