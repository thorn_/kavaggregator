authorization do
  role :guest do
    has_permission_on :articles, to: [:read]
    has_permission_on :messages, to: [:read]
    has_permission_on :comments, to: [:read]
    has_permission_on :api_v1_articles,     to: [:read]
    has_permission_on :api_v1_categories,   to: [:read]
    has_permission_on :api_v1_trends,       to: [:read]
    has_permission_on :api_v1_news_sources, to: [:read]

    has_permission_on :omniauth_callbacks, to: :omniauth
    has_permission_on :devise_registrations, to: :manage
    has_permission_on :devise_sessions, to: :manage
    has_permission_on :feedbacks, to: :send_feedback
    # has_permission_on :conferences, :to => :read do
    #   if_attribute :published => true
    # end
    # has_permission_on :talks, :to => :read do
    #   if_permitted_to :read, :conference
    # end
    # has_permission_on :users, :to => :create
    # has_permission_on :authorization_rules, :to => :read
    # has_permission_on :authorization_usages, :to => :read
  end

  role :user do
    includes :guest
    has_permission_on :comments, to: :create
    has_permission_on :comments, to: :delete do
      if_attribute user: is {user}
    end
    # has_permission_on :conference_attendees, :to => :create, :join_by => :and do
    #   if_attribute :user => is {user}
    #   if_permitted_to :read, :conference
    # end
    # has_permission_on :conference_attendees, :to => :delete do
    #   if_attribute :user => is {user}
    # end
    # has_permission_on :talk_attendees, :to => :create do
    #   if_attribute :talk => { :conference => { :attendees => contains {user} }},
    #       :user => is {user}
    # end
    # has_permission_on :talk_attendees, :to => :delete do
    #   if_attribute :user => is {user}
    # end
  end

  role :administrator do
    includes :user
    has_permission_on [:admin_messages, :messages, :admin_published_articles, :admin_users, :admin_categories, :admin_tags, :events, :admin_keywords, :admin_news_sources, :visited_pages, :comments, :admin_comments, :admin_trends], to: [:manage, :trend]
    has_permission_on [:admin_news_sources], to: [:manage, :clear]
    has_permission_on [:articles, :admin_articles], to: [:manage, :feed_me, :classify, :update_user_state, :train]
    has_permission_on [:from_tracking], to: [:be_free]

    has_permission_on :api_v1_articles,     to: [:manage]
    has_permission_on :api_v1_categories,   to: [:manage]
    has_permission_on :api_v1_trends,       to: [:manage]
    has_permission_on :api_v1_news_sources, to: [:manage]
    # has_permission_on [:conferences, :users, :talks], :to => :manage
    # has_permission_on :authorization_rules, :to => :read
    # has_permission_on :authorization_usages, :to => :read
  end
end

privileges do
  privilege :manage, includes: [:create, :read, :update, :delete, :group, :ungroup, :share]
  privilege :read,   includes: [:index, :show]
  privilege :create, includes: :new
  privilege :update, includes: :edit
  privilege :delete, includes: :destroy
  privilege :omniauth, includes: [:manage, :failure, :twitter, :facebook, :vkontakte, :mailru, :yandex, :google_oauth2]
end
