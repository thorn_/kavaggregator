namespace :environment_setup do
  desc "Setup environment variables for oauth"
  task :set, roles: :app do
    template 'private/environment_variables.rb.erb', "#{current_path}/config/environment_variables.rb"
    template 'private/1.html', "#{current_path}/public/1.html"
    template 'private/BingSiteAuth.xml', "#{current_path}/public/BingSiteAuth.xml"
    template 'private/3ff5fd435bdf.html', "#{current_path}/public/3ff5fd435bdf.html"
    template 'private/c55977a5f10e.html', "#{current_path}/public/c55977a5f10e.html"
    template 'private/channel.html', "#{current_path}/public/channel.html"
    template 'private/yandex_51974979a3603a1f.txt', "#{current_path}/public/yandex_51974979a3603a1f.txt"
    template 'private/google1959137a931c9501.html', "#{current_path}/public/google1959137a931c9501.html"
    template 'private/yandex_7e7b445976e980d2.txt', "#{current_path}/public/yandex_7e7b445976e980d2.txt"
    template 'private/google583c0cf6b16f8a99.html', "#{current_path}/public/google583c0cf6b16f8a99.html"
  end
  after "deploy:cold", "environment_setup:set"
end
