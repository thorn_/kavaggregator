source 'https://rubygems.org'

gem 'rails', '3.2.11'
gem 'pg'
gem 'thin'
gem 'jquery-rails-cdn'
gem 'bcrypt-ruby', '~> 3.0.0'

gem 'high_voltage'
gem 'feedzirra', git: 'git://github.com/pauldix/feedzirra.git'
gem 'simple_form'
gem 'chosen-rails'
gem 'tinymce-rails'
gem 'tinymce-rails-langs'
gem 'bourbon'
gem 'ruby-stemmer'
gem 'activerecord-postgres-hstore'

gem 'devise'
gem 'omniauth'
gem 'omniauth-twitter'
gem 'omniauth-facebook'
gem 'omniauth-google-oauth2'
gem 'omniauth-mailru'
gem 'omniauth-yandex'
gem 'omniauth-vkontakte'

gem 'declarative_authorization'
gem 'backbone-on-rails'
gem 'haml_coffee_assets'
gem 'nokogiri'
gem 'will_paginate'
gem 'ancestry'
gem 'faye'
gem 'russian'
gem 'whenever', require: false

gem 'jbuilder'
gem 'sitemap_generator', require: 'sitemap_generator'
gem 'hirb'
gem 'haml-rails'

group :assets do
  gem 'sass-rails',   '~> 3.2.3'
  gem 'coffee-rails', '~> 3.2.2'
  gem 'bootstrap-sass', '~> 2.1.0.0'

  gem 'therubyracer', '=0.10.2'
  gem 'uglifier', '>= 1.0.3'
end

group :test do
  gem 'guard'
  gem 'guard-spork'
  gem 'guard-cucumber', git: 'git://github.com/guard/guard-cucumber.git'
  gem 'guard-rspec'
  gem 'rspec-rails', git: 'git@github.com:rspec/rspec-rails.git'
  gem 'capybara'
  gem 'spork-rails'
  gem 'libnotify'
  gem 'selenium-webdriver'
  gem 'cucumber-rails', :require => false
  gem 'database_cleaner'
  gem 'capybara-webkit'
  gem 'launchy'
  gem 'factory_girl'
  gem "email_spec"
  gem 'jasminerice'
  gem 'simplecov', :require => false
  gem 'rb-inotify', '~> 0.9'
  gem 'vcr'
  gem 'fakeweb'
  gem 'webmock'
end

group :development do
  gem 'quiet_assets'
  gem 'jasminerice'
  gem 'meta_request', '0.2.0'
end

# deployment
gem 'capistrano'
gem 'rvm-capistrano' #, git: 'https://github.com/wayneeseguin/rvm-capistrano'
gem 'unicorn'

# caching
gem 'dalli'
